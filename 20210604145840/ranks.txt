RANK 1 (29) - 21 novel, 26 surprising, 25 valuable = 15 creative
gen3_1A - novel surprising
gen3_1B - novel surprising valuable CREATIVE
gen1_3B - novel surprising valuable CREATIVE
gen1_2A - novel surprising valuable CREATIVE
gen2_9B - novel surprising
gen1_0A - novel surprising valuable CREATIVE
gen2_6B - novel surprising valuable CREATIVE
gen1_1B - novel surprising valuable CREATIVE
gen3_0A - novel surprising
gen2_1A - novel surprising valuable CREATIVE
gen1_8B -       surprising valuable
gen1_6B - novel surprising valuable CREATIVE
gen3_3B - novel surprising valuable CREATIVE
gen2_1B - novel            valuable
gen1_9A -       surprising valuable
gen3_3A - novel surprising
gen1_1A -       surprising valuable
gen1_8A -       surprising valuable
gen3_0B - novel surprising valuable CREATIVE
gen1_4B - novel surprising valuable CREATIVE
gen3_5A - novel surprising valuable CREATIVE
gen3_9B - novel            valuable
gen1_6A -       surprising valuable
gen2_8B -                  valuable
gen2_3B -       surprising valuable
gen2_7B -       surprising valuable
gen1_4A - novel surprising valuable CREATIVE
gen3_7B - novel surprising valuable CREATIVE
gen3_6B - novel surprising valuable CREATIVE



RANK 2 (16) - 9 novel, 16 surprising, 14 valuable = 7 creative
gen3_9A - novel surprising
gen1_5A - novel surprising valuable CREATIVE
gen1_3A - novel surprising valuable CREATIVE
gen3_4B - novel surprising valuable CREATIVE
gen2_2A - novel surprising valuable CREATIVE
gen2_4A - novel surprising
gen2_6A -       surprising valuable
gen2_4B -       surprising valuable
gen1_7B - novel surprising valuable CREATIVE
gen2_5A - novel surprising valuable CREATIVE
gen2_0B -       surprising valuable
gen3_4A - novel surprising valuable CREATIVE
gen1_9B -       surprising valuable
gen2_9A -       surprising valuable
gen2_3A -       surprising valuable
gen2_5B -       surprising valuable



RANK 3 (15) - 11 novel, 13 surprising, 10 valuable = 6 creative
gen3_7A - novel surprising
gen3_6A - novel surprising
gen1_0B - novel surprising
gen1_7A - novel surprising
gen2_7A - novel surprising
gen3_8A - novel surprising valuable CREATIVE
gen2_0A -       surprising valuable
gen3_2A - novel surprising valuable CREATIVE
gen3_8B - novel surprising valuable CREATIVE
gen2_2B -                  valuable
gen3_2B - novel surprising valuable CREATIVE
gen3_5B - novel surprising valuable CREATIVE
gen1_2B - novel surprising valuable CREATIVE
gen1_5B -       surprising valuable
gen2_8A -                  valuable
