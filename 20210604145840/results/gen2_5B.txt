IMAGE
name=gen2_5B
content=dataset/output_images/gen1_6B.jpg
style=dataset/output_images/gen1_6A.jpg

SCORES
rank=1
ColorDistributionMeasure=0.432593
ComplexityMeasure=0.957825
LivelinessMeasure=0.962216
LuminanceDistributionMeasure=0.78862
LuminanceRedundancyMeasure=0.997874

MUTATIONS

METRICS
valuable=1
novel=0
surprising=1
creative=False
