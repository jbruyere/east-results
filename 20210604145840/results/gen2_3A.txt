IMAGE
name=gen2_3A
content=dataset/output_images/gen1_1A.jpg
style=dataset/output_images/gen1_8B.jpg

SCORES
rank=1
ColorDistributionMeasure=0.390981
ComplexityMeasure=0.93134
LivelinessMeasure=0.970347
LuminanceDistributionMeasure=0.744921
LuminanceRedundancyMeasure=0.992814

MUTATIONS

METRICS
valuable=1
novel=0
surprising=1
creative=False
