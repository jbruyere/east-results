IMAGE
name=gen1_4A
content=dataset/input_images/randegg-in-the-snow-with-ravens.jpg
style=dataset/input_images/george-caleb-bingham-boatmen-on-the-missouri-google-art-project.jpg

SCORES
rank=1
ColorDistributionMeasure=0.352637
ComplexityMeasure=0.932604
LivelinessMeasure=0.975511
LuminanceDistributionMeasure=0.781189
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True
