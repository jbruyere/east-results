IMAGE
name=gen3_5B
content=dataset/output_images/gen2_3B.jpg
style=dataset/output_images/gen2_9A.jpg

SCORES
rank=1
ColorDistributionMeasure=0.488541
ComplexityMeasure=0.931388
LivelinessMeasure=0.967043
LuminanceDistributionMeasure=0.725542
LuminanceRedundancyMeasure=0.991342

MUTATIONS
mutation_1=[lost]

METRICS
valuable=1
novel=1
surprising=1
creative=True
