IMAGE
name=gen3_2B
content=dataset/output_images/gen2_5A.jpg
style=dataset/output_images/gen2_4B.jpg

SCORES
rank=2
ColorDistributionMeasure=0.435678
ComplexityMeasure=0.883742
LivelinessMeasure=0.967461
LuminanceDistributionMeasure=0.710572
LuminanceRedundancyMeasure=0.996443

MUTATIONS
mutation_1=[lost]

METRICS
valuable=1
novel=1
surprising=1
creative=True
