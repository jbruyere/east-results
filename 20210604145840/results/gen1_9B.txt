IMAGE
name=gen1_9B
content=dataset/input_images/orfeu-nos-infernos-detail-1904.jpg
style=dataset/input_images/via-san-leonardo.jpg

SCORES
rank=2
ColorDistributionMeasure=0.426961
ComplexityMeasure=0.963384
LivelinessMeasure=0.961295
LuminanceDistributionMeasure=0.809991
LuminanceRedundancyMeasure=0.993546

MUTATIONS
mutation_1=[content_weight=750,style_weight=0.2]

METRICS
valuable=1
novel=0
surprising=1
creative=False
