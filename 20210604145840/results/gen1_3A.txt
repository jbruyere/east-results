IMAGE
name=gen1_3A
content=dataset/input_images/view-of-amalfi-1844.jpg
style=dataset/input_images/william-henry-hunt-the-winter-dining-room-of-the-earl-of-essex-at-cassiobury-1821-meisterdrucke.jpg

SCORES
rank=2
ColorDistributionMeasure=0.603477
ComplexityMeasure=0.980935
LivelinessMeasure=0.964517
LuminanceDistributionMeasure=0.776397
LuminanceRedundancyMeasure=0.965137

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/4490.jpg]
mutation_2=[content_weight=500,style_weight=0.25]
mutation_3=[color_filter=dark]

METRICS
valuable=1
novel=1
surprising=1
creative=True
