IMAGE
name=gen3_4B
content=dataset/output_images/gen2_2A.jpg
style=dataset/output_images/gen2_1A.jpg

SCORES
rank=1
ColorDistributionMeasure=0.599573
ComplexityMeasure=0.893296
LivelinessMeasure=0.968153
LuminanceDistributionMeasure=0.768879
LuminanceRedundancyMeasure=0.998586

MUTATIONS
mutation_1=[lost]

METRICS
valuable=1
novel=1
surprising=1
creative=True
