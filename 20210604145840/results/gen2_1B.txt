IMAGE
name=gen2_1B
content=dataset/output_images/gen1_1B.jpg
style=dataset/output_images/gen1_8B.jpg

SCORES
rank=1
ColorDistributionMeasure=0.563382
ComplexityMeasure=0.96496
LivelinessMeasure=0.966128
LuminanceDistributionMeasure=0.851929
LuminanceRedundancyMeasure=0.989858

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/75587.jpg]
mutation_2=[color_filter=dark]
mutation_3=[content_weight=750,style_weight=0.15]

METRICS
valuable=1
novel=1
surprising=0
creative=False
