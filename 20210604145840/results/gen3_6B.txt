IMAGE
name=gen3_6B
content=dataset/output_images/gen2_9B.jpg
style=dataset/output_images/gen2_6B.jpg

SCORES
rank=1
ColorDistributionMeasure=0.406877
ComplexityMeasure=0.887724
LivelinessMeasure=0.971663
LuminanceDistributionMeasure=0.803015
LuminanceRedundancyMeasure=0.99716

MUTATIONS
mutation_1=[lost]

METRICS
valuable=1
novel=1
surprising=1
creative=True
