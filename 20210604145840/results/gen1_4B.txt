IMAGE
name=gen1_4B
content=dataset/input_images/george-caleb-bingham-boatmen-on-the-missouri-google-art-project.jpg
style=dataset/input_images/randegg-in-the-snow-with-ravens.jpg

SCORES
rank=1
ColorDistributionMeasure=0.333166
ComplexityMeasure=0.859582
LivelinessMeasure=0.974201
LuminanceDistributionMeasure=0.784141
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True
