IMAGE
name=gen2_9A
content=dataset/output_images/gen1_1A.jpg
style=dataset/output_images/gen1_1B.jpg

SCORES
rank=1
ColorDistributionMeasure=0.447504
ComplexityMeasure=0.931855
LivelinessMeasure=0.970533
LuminanceDistributionMeasure=0.70436
LuminanceRedundancyMeasure=0.991342

MUTATIONS
mutation_1=[color_filter=dark]

METRICS
valuable=1
novel=0
surprising=1
creative=False
