IMAGE
name=gen1_0A
content=dataset/input_images/prisoners-exercising-prisoners-round-1890.jpg
style=dataset/input_images/marilyn-1.jpg

SCORES
rank=1
ColorDistributionMeasure=0.6292
ComplexityMeasure=0.945226
LivelinessMeasure=0.971957
LuminanceDistributionMeasure=0.723412
LuminanceRedundancyMeasure=0.997874

MUTATIONS
mutation_1=[content_weight=500,style_weight=0.2]
mutation_2=[color_filter=bright]

METRICS
valuable=1
novel=1
surprising=1
creative=True
