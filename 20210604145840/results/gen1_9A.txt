IMAGE
name=gen1_9A
content=dataset/input_images/via-san-leonardo.jpg
style=dataset/input_images/orfeu-nos-infernos-detail-1904.jpg

SCORES
rank=1
ColorDistributionMeasure=0.573459
ComplexityMeasure=0.957044
LivelinessMeasure=0.966173
LuminanceDistributionMeasure=0.839843
LuminanceRedundancyMeasure=0.995723

MUTATIONS
mutation_1=[content_weight=750,style_weight=0.005]
mutation_2=[color_filter=warm]

METRICS
valuable=1
novel=0
surprising=1
creative=False
