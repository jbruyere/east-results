IMAGE
name=gen1_7A
content=dataset/input_images/the-starry-night.jpg
style=dataset/input_images/jacob-s-ladder.jpg

SCORES
rank=2
ColorDistributionMeasure=0.240538
ComplexityMeasure=0.899142
LivelinessMeasure=0.965818
LuminanceDistributionMeasure=0.761959
LuminanceRedundancyMeasure=0.981469

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/99340.jpg]

METRICS
valuable=0
novel=1
surprising=1
creative=False
