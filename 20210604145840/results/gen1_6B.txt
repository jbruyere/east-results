IMAGE
name=gen1_6B
content=dataset/input_images/electrician-1915.jpg
style=dataset/input_images/double-twist-1980.jpg

SCORES
rank=1
ColorDistributionMeasure=0.517131
ComplexityMeasure=0.987463
LivelinessMeasure=0.948884
LuminanceDistributionMeasure=0.661609
LuminanceRedundancyMeasure=0.989111

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True
