IMAGE
name=gen2_4A
content=dataset/output_images/gen1_8B.jpg
style=dataset/output_images/gen1_2A.jpg

SCORES
rank=1
ColorDistributionMeasure=0.268394
ComplexityMeasure=0.91227
LivelinessMeasure=0.972483
LuminanceDistributionMeasure=0.767076
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False
