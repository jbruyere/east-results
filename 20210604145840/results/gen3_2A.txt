IMAGE
name=gen3_2A
content=dataset/output_images/gen2_4B.jpg
style=dataset/output_images/gen2_5A.jpg

SCORES
rank=1
ColorDistributionMeasure=0.487532
ComplexityMeasure=0.945951
LivelinessMeasure=0.964069
LuminanceDistributionMeasure=0.697764
LuminanceRedundancyMeasure=0.995723

MUTATIONS
mutation_1=[lost]

METRICS
valuable=1
novel=1
surprising=1
creative=True
