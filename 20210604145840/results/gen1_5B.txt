IMAGE
name=gen1_5B
content=dataset/input_images/double-twist-1980.jpg
style=dataset/input_images/jacob-s-ladder.jpg

SCORES
rank=2
ColorDistributionMeasure=0.431232
ComplexityMeasure=0.94616
LivelinessMeasure=0.962648
LuminanceDistributionMeasure=0.749279
LuminanceRedundancyMeasure=0.99208

MUTATIONS

METRICS
valuable=1
novel=0
surprising=1
creative=False
