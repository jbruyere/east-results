IMAGE
name=gen3_8A
content=dataset/output_images/gen2_9A.jpg
style=dataset/output_images/gen2_5A.jpg

SCORES
rank=2
ColorDistributionMeasure=0.37243
ComplexityMeasure=0.928435
LivelinessMeasure=0.963437
LuminanceDistributionMeasure=0.682603
LuminanceRedundancyMeasure=0.986093

MUTATIONS
mutation_1=[lost]

METRICS
valuable=1
novel=1
surprising=1
creative=True
