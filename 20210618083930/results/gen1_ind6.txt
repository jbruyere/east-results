IMAGE
name=gen1_ind6
content=dataset/input_images/jacob-s-ladder.jpg
style=dataset/input_images/forest.jpg

SCORES
rank=2
ColorDistributionMeasure=0.428666
ComplexityMeasure=0.854053
LivelinessMeasure=0.971711
LuminanceDistributionMeasure=0.796531
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/19988.jpg]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.28336333990091744
novelty_score=0.2974074593868337
surprise_score=0.8
