IMAGE
name=gen1_ind5
content=dataset/input_images/marilyn-1.jpg
style=dataset/input_images/electrician-1915.jpg

SCORES
rank=1
ColorDistributionMeasure=0.578738
ComplexityMeasure=0.986941
LivelinessMeasure=0.965949
LuminanceDistributionMeasure=0.731794
LuminanceRedundancyMeasure=0.99716

MUTATIONS
mutation_1=[content_weight=750,style_weight=0.1]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.40260677668569916
novelty_score=0.1943767512960859
surprise_score=0.8
