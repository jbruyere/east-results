IMAGE
name=gen1_ind9
content=dataset/input_images/plate-77-belted-kingfisher.jpg
style=dataset/input_images/marguerite-1975.jpg

SCORES
rank=1
ColorDistributionMeasure=0.769961
ComplexityMeasure=0.981484
LivelinessMeasure=0.963904
LuminanceDistributionMeasure=0.75929
LuminanceRedundancyMeasure=0.974302

MUTATIONS
mutation_1=[color_filter=cool]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.5388737256185931
novelty_score=0.1817994506107654
surprise_score=0.8
