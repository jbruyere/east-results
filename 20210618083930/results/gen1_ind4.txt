IMAGE
name=gen1_ind4
content=dataset/input_images/rooftops-venice-1983.jpg
style=dataset/input_images/randegg-in-the-snow-with-ravens.jpg

SCORES
rank=1
ColorDistributionMeasure=0.185651
ComplexityMeasure=0.847894
LivelinessMeasure=0.974211
LuminanceDistributionMeasure=0.785225
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.12041650060067804
novelty_score=0.4928789298379515
surprise_score=0.6000000000000001
