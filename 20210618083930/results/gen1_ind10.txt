IMAGE
name=gen1_ind10
content=dataset/input_images/the-magpie-1869.jpg
style=dataset/input_images/via-san-leonardo.jpg

SCORES
rank=2
ColorDistributionMeasure=0.503711
ComplexityMeasure=0.988032
LivelinessMeasure=0.957902
LuminanceDistributionMeasure=0.736478
LuminanceRedundancyMeasure=0.975112

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.3423637737804909
novelty_score=0.22433804355957157
surprise_score=0.4
