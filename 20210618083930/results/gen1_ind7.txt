IMAGE
name=gen1_ind7
content=dataset/input_images/marguerite-1975.jpg
style=dataset/input_images/the-great-wave-off-kanagawa.jpg

SCORES
rank=2
ColorDistributionMeasure=0.474991
ComplexityMeasure=0.856777
LivelinessMeasure=0.971605
LuminanceDistributionMeasure=0.783035
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/69501.jpg]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.30939790997843314
novelty_score=0.25663622013024184
surprise_score=1.0
