IMAGE
name=gen1_ind16
content=dataset/input_images/linhas-de-sombra.jpg
style=dataset/input_images/the-tennis-court-oath-20th-june-1789-1791.jpg

SCORES
rank=1
ColorDistributionMeasure=0.729791
ComplexityMeasure=0.894614
LivelinessMeasure=0.96587
LuminanceDistributionMeasure=0.754259
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[color_filter=cool]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.47529872721747923
novelty_score=0.1770990850555398
surprise_score=0.8
