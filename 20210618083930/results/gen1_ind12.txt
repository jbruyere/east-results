IMAGE
name=gen1_ind12
content=dataset/input_images/william-henry-hunt-the-winter-dining-room-of-the-earl-of-essex-at-cassiobury-1821-meisterdrucke.jpg
style=dataset/input_images/seaweed-gatherers-at-omari.jpg

SCORES
rank=2
ColorDistributionMeasure=0.629289
ComplexityMeasure=0.906341
LivelinessMeasure=0.972763
LuminanceDistributionMeasure=0.775737
LuminanceRedundancyMeasure=0.999294

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.43008727811020836
novelty_score=0.17227780045349847
surprise_score=0.8
