IMAGE
name=gen1_ind15
content=dataset/input_images/cash-register-1917.jpg
style=dataset/input_images/randegg-in-the-snow-with-ravens.jpg

SCORES
rank=1
ColorDistributionMeasure=0.130483
ComplexityMeasure=0.84512
LivelinessMeasure=0.973762
LuminanceDistributionMeasure=0.810317
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.08701218723210571
novelty_score=0.5509773356900282
surprise_score=0.4
