IMAGE
name=gen2_ind14
content=dataset/output_images/gen1_ind10.jpg
style=dataset/output_images/gen1_ind15.jpg

SCORES
rank=1
ColorDistributionMeasure=0.72374
ComplexityMeasure=0.990354
LivelinessMeasure=0.968795
LuminanceDistributionMeasure=0.788744
LuminanceRedundancyMeasure=0.984565

MUTATIONS

METRICS
valuable=1
novel=0
surprising=1
creative=False


METRICS_VALUES
value_score=0.5392440806616913
novelty_score=0.1498999171960527
surprise_score=0.2
