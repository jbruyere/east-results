IMAGE
name=gen7_ind10
content=dataset/output_images/gen6_ind16.jpg
style=dataset/output_images/gen6_ind9.jpg

SCORES
rank=1
ColorDistributionMeasure=0.57864
ComplexityMeasure=0.902667
LivelinessMeasure=0.972748
LuminanceDistributionMeasure=0.855618
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.4347266622427408
novelty_score=0.18166298167188008
surprise_score=0.0
