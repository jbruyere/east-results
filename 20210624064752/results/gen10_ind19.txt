IMAGE
name=gen10_ind19
content=dataset/output_images/gen9_ind3.jpg
style=dataset/output_images/gen9_ind16.jpg

SCORES
rank=1
ColorDistributionMeasure=0.703567
ComplexityMeasure=0.876435
LivelinessMeasure=0.973967
LuminanceDistributionMeasure=0.732466
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[content_weight=750,style_weight=0.1]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.4399029620487459
novelty_score=0.18208298174664891
surprise_score=0.4
