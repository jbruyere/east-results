IMAGE
name=gen3_ind14
content=dataset/output_images/gen2_ind3.jpg
style=dataset/output_images/gen2_ind19.jpg

SCORES
rank=2
ColorDistributionMeasure=0.613376
ComplexityMeasure=0.962217
LivelinessMeasure=0.966187
LuminanceDistributionMeasure=0.759765
LuminanceRedundancyMeasure=0.984565

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.42656446193762576
novelty_score=0.16336925762660362
surprise_score=0.0
