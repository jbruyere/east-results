IMAGE
name=gen9_ind0
content=dataset/output_images/gen8_ind15.jpg
style=dataset/output_images/gen8_ind9.jpg

SCORES
rank=1
ColorDistributionMeasure=0.66363
ComplexityMeasure=0.894286
LivelinessMeasure=0.973295
LuminanceDistributionMeasure=0.811812
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[content_weight=1500,style_weight=0.1]

METRICS
valuable=1
novel=0
surprising=1
creative=False


METRICS_VALUES
value_score=0.4689239357310864
novelty_score=0.15543244801978828
surprise_score=0.2
