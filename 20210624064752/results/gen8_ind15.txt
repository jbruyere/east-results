IMAGE
name=gen8_ind15
content=dataset/output_images/gen7_ind0.jpg
style=dataset/output_images/gen7_ind2.jpg

SCORES
rank=1
ColorDistributionMeasure=0.614917
ComplexityMeasure=0.870052
LivelinessMeasure=0.97151
LuminanceDistributionMeasure=0.812527
LuminanceRedundancyMeasure=0.999294

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.4220268339573322
novelty_score=0.17076511611986445
surprise_score=0.2
