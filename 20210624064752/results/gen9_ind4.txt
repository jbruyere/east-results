IMAGE
name=gen9_ind4
content=dataset/output_images/gen8_ind15.jpg
style=dataset/output_images/gen8_ind2.jpg

SCORES
rank=2
ColorDistributionMeasure=0.655717
ComplexityMeasure=0.871771
LivelinessMeasure=0.972321
LuminanceDistributionMeasure=0.796808
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.4428760678916741
novelty_score=0.1610532365386479
surprise_score=0.2
