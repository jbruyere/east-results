IMAGE
name=gen5_ind9
content=dataset/output_images/gen4_ind6.jpg
style=dataset/output_images/gen4_ind11.jpg

SCORES
rank=1
ColorDistributionMeasure=0.63476
ComplexityMeasure=0.956105
LivelinessMeasure=0.969682
LuminanceDistributionMeasure=0.81682
LuminanceRedundancyMeasure=0.997874

MUTATIONS

METRICS
valuable=1
novel=0
surprising=0
creative=False


METRICS_VALUES
value_score=0.4796744042724324
novelty_score=0.1598724808902834
surprise_score=0.0
