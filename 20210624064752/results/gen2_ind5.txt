IMAGE
name=gen2_ind5
content=dataset/output_images/gen1_ind13.jpg
style=dataset/output_images/gen1_ind2.jpg

SCORES
rank=2
ColorDistributionMeasure=0.512999
ComplexityMeasure=0.864779
LivelinessMeasure=0.968747
LuminanceDistributionMeasure=0.760114
LuminanceRedundancyMeasure=0.998586

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.32620921754908827
novelty_score=0.2432607525898142
surprise_score=0.2
