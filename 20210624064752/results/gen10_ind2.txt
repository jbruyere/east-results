IMAGE
name=gen10_ind2
content=dataset/output_images/gen9_ind19.jpg
style=dataset/output_images/gen9_ind7.jpg

SCORES
rank=1
ColorDistributionMeasure=0.710395
ComplexityMeasure=0.877891
LivelinessMeasure=0.972922
LuminanceDistributionMeasure=0.72614
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/59888.jpg]
mutation_2=[color_filter=warm]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.44059430326615273
novelty_score=0.18659671991033677
surprise_score=0.4
