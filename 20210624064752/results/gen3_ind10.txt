IMAGE
name=gen3_ind10
content=dataset/output_images/gen2_ind19.jpg
style=dataset/output_images/gen2_ind13.jpg

SCORES
rank=2
ColorDistributionMeasure=0.429438
ComplexityMeasure=0.941613
LivelinessMeasure=0.973477
LuminanceDistributionMeasure=0.781183
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.3075044436728154
novelty_score=0.26610452288719744
surprise_score=0.0
