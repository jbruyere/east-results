IMAGE
name=gen2_ind3
content=dataset/output_images/gen1_ind2.jpg
style=dataset/output_images/gen1_ind9.jpg

SCORES
rank=1
ColorDistributionMeasure=0.484012
ComplexityMeasure=0.950884
LivelinessMeasure=0.975643
LuminanceDistributionMeasure=0.780727
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[content_weight=500,style_weight=0.1]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.35056923489921976
novelty_score=0.2350022561251299
surprise_score=0.4
