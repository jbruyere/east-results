IMAGE
name=gen1_ind14
content=dataset/input_images/jacob-s-ladder.jpg
style=dataset/input_images/the-magpie-1869.jpg

SCORES
rank=2
ColorDistributionMeasure=0.59182
ComplexityMeasure=0.964813
LivelinessMeasure=0.972482
LuminanceDistributionMeasure=0.751703
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.417407875843447
novelty_score=0.18116941709821138
surprise_score=0.8
