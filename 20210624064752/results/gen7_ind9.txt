IMAGE
name=gen7_ind9
content=dataset/output_images/gen6_ind16.jpg
style=dataset/output_images/gen6_ind8.jpg

SCORES
rank=1
ColorDistributionMeasure=0.643369
ComplexityMeasure=0.885127
LivelinessMeasure=0.972634
LuminanceDistributionMeasure=0.800557
LuminanceRedundancyMeasure=0.998586

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/26364.jpg]

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.44278499899843726
novelty_score=0.17823141154798644
surprise_score=0.0
