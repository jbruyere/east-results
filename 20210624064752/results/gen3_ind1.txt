IMAGE
name=gen3_ind1
content=dataset/output_images/gen2_ind19.jpg
style=dataset/output_images/gen2_ind2.jpg

SCORES
rank=1
ColorDistributionMeasure=0.72089
ComplexityMeasure=0.996909
LivelinessMeasure=0.970441
LuminanceDistributionMeasure=0.851525
LuminanceRedundancyMeasure=0.99716

MUTATIONS

METRICS
valuable=1
novel=0
surprising=1
creative=False


METRICS_VALUES
value_score=0.5921829600776221
novelty_score=0.15715708868200515
surprise_score=0.4
