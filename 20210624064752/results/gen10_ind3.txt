IMAGE
name=gen10_ind3
content=dataset/output_images/gen9_ind19.jpg
style=dataset/output_images/gen9_ind9.jpg

SCORES
rank=2
ColorDistributionMeasure=0.458762
ComplexityMeasure=0.853336
LivelinessMeasure=0.973346
LuminanceDistributionMeasure=0.718375
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/84501.jpg]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.273732247840046
novelty_score=0.24297266683612814
surprise_score=0.4
