IMAGE
name=gen6_ind17
content=dataset/output_images/gen5_ind2.jpg
style=dataset/output_images/gen5_ind7.jpg

SCORES
rank=1
ColorDistributionMeasure=0.623484
ComplexityMeasure=0.929785
LivelinessMeasure=0.965449
LuminanceDistributionMeasure=0.795449
LuminanceRedundancyMeasure=0.993546

MUTATIONS
mutation_1=[color_filter=cool]

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.44232094521574766
novelty_score=0.1677820024021494
surprise_score=0.0
