IMAGE
name=gen2_ind7
content=dataset/output_images/gen1_ind3.jpg
style=dataset/output_images/gen1_ind19.jpg

SCORES
rank=1
ColorDistributionMeasure=0.779911
ComplexityMeasure=0.981704
LivelinessMeasure=0.970736
LuminanceDistributionMeasure=0.779095
LuminanceRedundancyMeasure=0.996443

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.5769917717890384
novelty_score=0.16468327604172797
surprise_score=0.2
