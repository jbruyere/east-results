IMAGE
name=gen2_ind11
content=dataset/output_images/gen1_ind3.jpg
style=dataset/output_images/gen1_ind1.jpg

SCORES
rank=1
ColorDistributionMeasure=0.769193
ComplexityMeasure=0.964727
LivelinessMeasure=0.969982
LuminanceDistributionMeasure=0.795303
LuminanceRedundancyMeasure=0.988361

MUTATIONS

METRICS
valuable=1
novel=0
surprising=0
creative=False


METRICS_VALUES
value_score=0.5657852908859121
novelty_score=0.15634246949195876
surprise_score=0.0
