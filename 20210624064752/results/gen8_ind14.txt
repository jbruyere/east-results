IMAGE
name=gen8_ind14
content=dataset/output_images/gen7_ind13.jpg
style=dataset/output_images/gen7_ind9.jpg

SCORES
rank=1
ColorDistributionMeasure=0.677184
ComplexityMeasure=0.882929
LivelinessMeasure=0.972658
LuminanceDistributionMeasure=0.772555
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.4492851256036814
novelty_score=0.17693476632171537
surprise_score=0.4
