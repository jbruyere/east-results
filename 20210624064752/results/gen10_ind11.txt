IMAGE
name=gen10_ind11
content=dataset/output_images/gen9_ind7.jpg
style=dataset/output_images/gen9_ind9.jpg

SCORES
rank=1
ColorDistributionMeasure=0.751883
ComplexityMeasure=0.882633
LivelinessMeasure=0.97279
LuminanceDistributionMeasure=0.824058
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[content_weight=1250,style_weight=0.15]

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.5319946978238633
novelty_score=0.18484825573930688
surprise_score=0.0
