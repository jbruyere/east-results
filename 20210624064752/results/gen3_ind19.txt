IMAGE
name=gen3_ind19
content=dataset/output_images/gen2_ind12.jpg
style=dataset/output_images/gen2_ind3.jpg

SCORES
rank=1
ColorDistributionMeasure=0.413824
ComplexityMeasure=0.941468
LivelinessMeasure=0.974275
LuminanceDistributionMeasure=0.864836
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.3282740517493125
novelty_score=0.2863028033560889
surprise_score=0.4
