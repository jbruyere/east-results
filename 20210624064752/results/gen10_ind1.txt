IMAGE
name=gen10_ind1
content=dataset/output_images/gen9_ind11.jpg
style=dataset/output_images/gen9_ind14.jpg

SCORES
rank=1
ColorDistributionMeasure=0.745415
ComplexityMeasure=0.88536
LivelinessMeasure=0.972373
LuminanceDistributionMeasure=0.858281
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[content_weight=1000,style_weight=0.25]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.5507828570708002
novelty_score=0.18692315228774745
surprise_score=0.2
