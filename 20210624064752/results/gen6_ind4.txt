IMAGE
name=gen6_ind4
content=dataset/output_images/gen5_ind9.jpg
style=dataset/output_images/gen5_ind0.jpg

SCORES
rank=1
ColorDistributionMeasure=0.712276
ComplexityMeasure=0.881642
LivelinessMeasure=0.973845
LuminanceDistributionMeasure=0.724028
LuminanceRedundancyMeasure=0.998586

MUTATIONS
mutation_1=[content_weight=500,style_weight=0.25]
mutation_2=[style_transfer=dataset/tmp_images/dataset/tmp_images/78461.jpg]
mutation_3=[color_filter=warm]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.442151655911802
novelty_score=0.20855809373619932
surprise_score=0.2
