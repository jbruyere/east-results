IMAGE
name=napoleoncrossing-the-alps-at-the-st-bernard-pass-20th-may-1800-1801
content=
style=

SCORES
rank=2
ColorDistributionMeasure=0.656716
ComplexityMeasure=0.99255
LivelinessMeasure=0.964448
LuminanceDistributionMeasure=0.743137
LuminanceRedundancyMeasure=0.979111

MUTATIONS

METRICS
valuable=1
novel=0
surprising=1
creative=False


METRICS_VALUES
value_score=0.4574141787710406
novelty_score=0
surprise_score=1.0
