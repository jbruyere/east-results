IMAGE
name=gen5_ind19
content=dataset/output_images/gen4_ind12.jpg
style=dataset/output_images/gen4_ind6.jpg

SCORES
rank=1
ColorDistributionMeasure=0.436979
ComplexityMeasure=0.917101
LivelinessMeasure=0.974733
LuminanceDistributionMeasure=0.791662
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.3092453672061114
novelty_score=0.23727680333335277
surprise_score=0.0
