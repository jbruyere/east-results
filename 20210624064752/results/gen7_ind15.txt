IMAGE
name=gen7_ind15
content=dataset/output_images/gen6_ind13.jpg
style=dataset/output_images/gen6_ind9.jpg

SCORES
rank=1
ColorDistributionMeasure=0.530014
ComplexityMeasure=0.908925
LivelinessMeasure=0.974256
LuminanceDistributionMeasure=0.789009
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.3703142602893322
novelty_score=0.18550974883847002
surprise_score=0.0
