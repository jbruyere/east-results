IMAGE
name=gen2_ind0
content=dataset/output_images/gen1_ind13.jpg
style=dataset/output_images/gen1_ind10.jpg

SCORES
rank=2
ColorDistributionMeasure=0.374991
ComplexityMeasure=0.914001
LivelinessMeasure=0.968841
LuminanceDistributionMeasure=0.703711
LuminanceRedundancyMeasure=0.989858

MUTATIONS
mutation_1=[color_filter=dark]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.23130619355924742
novelty_score=0.3470510572565217
surprise_score=0.4
