IMAGE
name=gen4_ind19
content=dataset/output_images/gen3_ind6.jpg
style=dataset/output_images/gen3_ind0.jpg

SCORES
rank=2
ColorDistributionMeasure=0.526043
ComplexityMeasure=0.903488
LivelinessMeasure=0.971583
LuminanceDistributionMeasure=0.7191
LuminanceRedundancyMeasure=0.987608

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.3279422936243672
novelty_score=0.21395033818273798
surprise_score=0.2
