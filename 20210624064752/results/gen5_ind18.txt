IMAGE
name=gen5_ind18
content=dataset/output_images/gen4_ind17.jpg
style=dataset/output_images/gen4_ind12.jpg

SCORES
rank=1
ColorDistributionMeasure=0.753946
ComplexityMeasure=0.884567
LivelinessMeasure=0.968961
LuminanceDistributionMeasure=0.842924
LuminanceRedundancyMeasure=0.998586

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.5439402099793786
novelty_score=0.1988077462177201
surprise_score=0.0
