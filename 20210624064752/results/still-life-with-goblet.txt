IMAGE
name=still-life-with-goblet
content=
style=

SCORES
rank=1
ColorDistributionMeasure=0.926546
ComplexityMeasure=0.986367
LivelinessMeasure=0.918191
LuminanceDistributionMeasure=0.552842
LuminanceRedundancyMeasure=0.989111

MUTATIONS

METRICS
valuable=0
novel=0
surprising=1
creative=False


METRICS_VALUES
value_score=0.45886466100484036
novelty_score=0
surprise_score=1.0
