IMAGE
name=gen10_ind7
content=dataset/output_images/gen9_ind10.jpg
style=dataset/output_images/gen9_ind19.jpg

SCORES
rank=2
ColorDistributionMeasure=0.676793
ComplexityMeasure=0.869155
LivelinessMeasure=0.971511
LuminanceDistributionMeasure=0.773228
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/45437.jpg]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.4418841108576291
novelty_score=0.16440785853694562
surprise_score=0.4
