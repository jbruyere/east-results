IMAGE
name=gen10_ind6
content=dataset/output_images/gen9_ind3.jpg
style=dataset/output_images/gen9_ind7.jpg

SCORES
rank=1
ColorDistributionMeasure=0.661271
ComplexityMeasure=0.872989
LivelinessMeasure=0.973921
LuminanceDistributionMeasure=0.735211
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.41335574228007566
novelty_score=0.1732343624166187
surprise_score=0.0
