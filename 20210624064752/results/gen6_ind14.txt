IMAGE
name=gen6_ind14
content=dataset/output_images/gen5_ind0.jpg
style=dataset/output_images/gen5_ind18.jpg

SCORES
rank=1
ColorDistributionMeasure=0.678086
ComplexityMeasure=0.884335
LivelinessMeasure=0.970764
LuminanceDistributionMeasure=0.784269
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[content_weight=750,style_weight=0.2]
mutation_2=[style_transfer=dataset/tmp_images/dataset/tmp_images/34661.jpg]

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.4565415437517127
novelty_score=0.1834146116581865
surprise_score=0.0
