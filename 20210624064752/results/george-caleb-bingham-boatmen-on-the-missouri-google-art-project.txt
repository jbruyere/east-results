IMAGE
name=george-caleb-bingham-boatmen-on-the-missouri-google-art-project
content=
style=

SCORES
rank=2
ColorDistributionMeasure=0.717654
ComplexityMeasure=0.962808
LivelinessMeasure=0.965582
LuminanceDistributionMeasure=0.912837
LuminanceRedundancyMeasure=0.989858

MUTATIONS

METRICS
valuable=1
novel=0
surprising=1
creative=False


METRICS_VALUES
value_score=0.6028511498912781
novelty_score=0
surprise_score=1.0
