IMAGE
name=gen10_ind5
content=dataset/output_images/gen9_ind10.jpg
style=dataset/output_images/gen9_ind14.jpg

SCORES
rank=1
ColorDistributionMeasure=0.753729
ComplexityMeasure=0.883739
LivelinessMeasure=0.972646
LuminanceDistributionMeasure=0.824124
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.5339328152831273
novelty_score=0.1855840284123831
surprise_score=0.2
