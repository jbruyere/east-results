IMAGE
name=gen5_ind4
content=dataset/output_images/gen4_ind6.jpg
style=dataset/output_images/gen4_ind18.jpg

SCORES
rank=1
ColorDistributionMeasure=0.645639
ComplexityMeasure=0.884601
LivelinessMeasure=0.972814
LuminanceDistributionMeasure=0.699703
LuminanceRedundancyMeasure=0.996443

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.38737642847546216
novelty_score=0.2074905955569815
surprise_score=0.4
