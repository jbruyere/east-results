IMAGE
name=gen9_ind8
content=dataset/output_images/gen8_ind3.jpg
style=dataset/output_images/gen8_ind13.jpg

SCORES
rank=3
ColorDistributionMeasure=0.624524
ComplexityMeasure=0.863152
LivelinessMeasure=0.967229
LuminanceDistributionMeasure=0.742613
LuminanceRedundancyMeasure=0.991342

MUTATIONS
mutation_1=[color_filter=dark]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.3838413667078695
novelty_score=0.17484640993611716
surprise_score=0.8
