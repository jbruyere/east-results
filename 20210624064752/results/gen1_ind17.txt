IMAGE
name=gen1_ind17
content=dataset/input_images/costume-design-for-salome-1917.jpg
style=dataset/input_images/well-by-the-winding-road-in-the-park-of-chateau-noir.jpg

SCORES
rank=1
ColorDistributionMeasure=0.63638
ComplexityMeasure=0.958976
LivelinessMeasure=0.96846
LuminanceDistributionMeasure=0.778209
LuminanceRedundancyMeasure=0.986852

MUTATIONS

METRICS
valuable=1
novel=0
surprising=1
creative=False


METRICS_VALUES
value_score=0.45389377153631877
novelty_score=0.14968824418575413
surprise_score=0.4
