IMAGE
name=gen10_ind18
content=dataset/output_images/gen9_ind14.jpg
style=dataset/output_images/gen9_ind12.jpg

SCORES
rank=2
ColorDistributionMeasure=0.669258
ComplexityMeasure=0.873789
LivelinessMeasure=0.972016
LuminanceDistributionMeasure=0.813267
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[content_weight=500,style_weight=0.25]
mutation_2=[style_transfer=dataset/tmp_images/dataset/tmp_images/64131.jpg]

METRICS
valuable=1
novel=0
surprising=0
creative=False


METRICS_VALUES
value_score=0.4622817071321418
novelty_score=0.15674334010078933
surprise_score=0.0
