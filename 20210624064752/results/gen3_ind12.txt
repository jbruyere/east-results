IMAGE
name=gen3_ind12
content=dataset/output_images/gen2_ind12.jpg
style=dataset/output_images/gen2_ind19.jpg

SCORES
rank=2
ColorDistributionMeasure=0.631727
ComplexityMeasure=0.96786
LivelinessMeasure=0.963456
LuminanceDistributionMeasure=0.795538
LuminanceRedundancyMeasure=0.98533

MUTATIONS

METRICS
valuable=1
novel=0
surprising=0
creative=False


METRICS_VALUES
value_score=0.46176020398483036
novelty_score=0.15268001741240805
surprise_score=0.0
