IMAGE
name=gen5_ind16
content=dataset/output_images/gen4_ind18.jpg
style=dataset/output_images/gen4_ind7.jpg

SCORES
rank=1
ColorDistributionMeasure=0.342252
ComplexityMeasure=0.932005
LivelinessMeasure=0.973179
LuminanceDistributionMeasure=0.849612
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.2637409726867755
novelty_score=0.310933550305698
surprise_score=0.2
