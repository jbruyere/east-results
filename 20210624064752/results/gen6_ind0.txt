IMAGE
name=gen6_ind0
content=dataset/output_images/gen5_ind0.jpg
style=dataset/output_images/gen5_ind4.jpg

SCORES
rank=1
ColorDistributionMeasure=0.600775
ComplexityMeasure=0.888612
LivelinessMeasure=0.968828
LuminanceDistributionMeasure=0.809714
LuminanceRedundancyMeasure=0.998586

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.4182036597129339
novelty_score=0.18104436771301397
surprise_score=0.0
