IMAGE
name=gen2_ind13
content=dataset/output_images/gen1_ind3.jpg
style=dataset/output_images/gen1_ind9.jpg

SCORES
rank=1
ColorDistributionMeasure=0.521312
ComplexityMeasure=0.965025
LivelinessMeasure=0.974225
LuminanceDistributionMeasure=0.832899
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[content_weight=1000,style_weight=0.2]

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.40821400180297873
novelty_score=0.2071187153497225
surprise_score=0.0
