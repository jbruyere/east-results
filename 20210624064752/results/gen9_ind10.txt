IMAGE
name=gen9_ind10
content=dataset/output_images/gen8_ind9.jpg
style=dataset/output_images/gen8_ind2.jpg

SCORES
rank=2
ColorDistributionMeasure=0.690591
ComplexityMeasure=0.873896
LivelinessMeasure=0.971981
LuminanceDistributionMeasure=0.80552
LuminanceRedundancyMeasure=0.998586

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.4718459612935549
novelty_score=0.1677242077737032
surprise_score=0.0
