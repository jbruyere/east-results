IMAGE
name=double-twist-1980
content=
style=

SCORES
rank=2
ColorDistributionMeasure=0.849086
ComplexityMeasure=0.944945
LivelinessMeasure=0.934038
LuminanceDistributionMeasure=0.705863
LuminanceRedundancyMeasure=0.933451

MUTATIONS

METRICS
valuable=0
novel=0
surprising=1
creative=False


METRICS_VALUES
value_score=0.4937813692776106
novelty_score=0
surprise_score=1.0
