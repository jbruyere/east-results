IMAGE
name=gen2_ind18
content=dataset/output_images/gen1_ind11.jpg
style=dataset/output_images/gen1_ind13.jpg

SCORES
rank=1
ColorDistributionMeasure=0.553373
ComplexityMeasure=0.914296
LivelinessMeasure=0.974839
LuminanceDistributionMeasure=0.749331
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[content_weight=1250,style_weight=0.2]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.36958248432982194
novelty_score=0.20530058167113258
surprise_score=0.8
