IMAGE
name=gen8_ind19
content=dataset/output_images/gen7_ind15.jpg
style=dataset/output_images/gen7_ind0.jpg

SCORES
rank=2
ColorDistributionMeasure=0.656831
ComplexityMeasure=0.874235
LivelinessMeasure=0.972568
LuminanceDistributionMeasure=0.756112
LuminanceRedundancyMeasure=0.998586

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.4216706864325783
novelty_score=0.1803676745189392
surprise_score=0.2
