IMAGE
name=gen10_ind13
content=dataset/output_images/gen9_ind7.jpg
style=dataset/output_images/gen9_ind0.jpg

SCORES
rank=2
ColorDistributionMeasure=0.737746
ComplexityMeasure=0.873116
LivelinessMeasure=0.971479
LuminanceDistributionMeasure=0.806407
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[color_filter=bright]
mutation_2=[content_weight=1250,style_weight=0.15]
mutation_3=[style_transfer=dataset/tmp_images/dataset/tmp_images/61362.jpg]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.5046223902451155
novelty_score=0.18002210519838913
surprise_score=0.4
