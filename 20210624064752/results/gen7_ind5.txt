IMAGE
name=gen7_ind5
content=dataset/output_images/gen6_ind3.jpg
style=dataset/output_images/gen6_ind19.jpg

SCORES
rank=2
ColorDistributionMeasure=0.681834
ComplexityMeasure=0.875075
LivelinessMeasure=0.968385
LuminanceDistributionMeasure=0.776408
LuminanceRedundancyMeasure=0.998586

MUTATIONS
mutation_1=[color_filter=dark]
mutation_2=[style_transfer=dataset/tmp_images/dataset/tmp_images/20539.jpg]

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.44796848166994496
novelty_score=0.1896361633580162
surprise_score=0.0
