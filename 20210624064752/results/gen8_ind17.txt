IMAGE
name=gen8_ind17
content=dataset/output_images/gen7_ind17.jpg
style=dataset/output_images/gen7_ind6.jpg

SCORES
rank=1
ColorDistributionMeasure=0.689049
ComplexityMeasure=0.881032
LivelinessMeasure=0.96925
LuminanceDistributionMeasure=0.796873
LuminanceRedundancyMeasure=0.997874

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.46788855100610866
novelty_score=0.17680216292471826
surprise_score=0.4
