IMAGE
name=gen7_ind13
content=dataset/output_images/gen6_ind5.jpg
style=dataset/output_images/gen6_ind9.jpg

SCORES
rank=1
ColorDistributionMeasure=0.501517
ComplexityMeasure=0.912494
LivelinessMeasure=0.973469
LuminanceDistributionMeasure=0.813011
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.3621881391814861
novelty_score=0.19283871502634195
surprise_score=0.0
