IMAGE
name=gen4_ind15
content=dataset/output_images/gen3_ind9.jpg
style=dataset/output_images/gen3_ind19.jpg

SCORES
rank=1
ColorDistributionMeasure=0.395551
ComplexityMeasure=0.942716
LivelinessMeasure=0.974004
LuminanceDistributionMeasure=0.853222
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.3098889927294856
novelty_score=0.28619504363209325
surprise_score=0.2
