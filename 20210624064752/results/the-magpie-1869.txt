IMAGE
name=the-magpie-1869
content=
style=

SCORES
rank=2
ColorDistributionMeasure=0.642248
ComplexityMeasure=0.990493
LivelinessMeasure=0.956412
LuminanceDistributionMeasure=0.723659
LuminanceRedundancyMeasure=0.994275

MUTATIONS

METRICS
valuable=1
novel=0
surprising=1
creative=False


METRICS_VALUES
value_score=0.43776362810595415
novelty_score=0
surprise_score=1.0
