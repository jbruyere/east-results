IMAGE
name=gen1_ind3
content=dataset/input_images/landscape-under-snow-upper-norwood-1871.jpg
style=dataset/input_images/self-portrait-with-easel.jpg

SCORES
rank=1
ColorDistributionMeasure=0.692697
ComplexityMeasure=0.998987
LivelinessMeasure=0.967854
LuminanceDistributionMeasure=0.812745
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=0
surprising=1
creative=False


METRICS_VALUES
value_score=0.5443363027389929
novelty_score=0.14993467204154295
surprise_score=0.4
