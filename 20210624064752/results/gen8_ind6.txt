IMAGE
name=gen8_ind6
content=dataset/output_images/gen7_ind17.jpg
style=dataset/output_images/gen7_ind0.jpg

SCORES
rank=1
ColorDistributionMeasure=0.617286
ComplexityMeasure=0.869952
LivelinessMeasure=0.969797
LuminanceDistributionMeasure=0.849664
LuminanceRedundancyMeasure=0.998586

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.4418707410830263
novelty_score=0.17540718741001407
surprise_score=0.4
