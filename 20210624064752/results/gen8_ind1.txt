IMAGE
name=gen8_ind1
content=dataset/output_images/gen7_ind2.jpg
style=dataset/output_images/gen7_ind3.jpg

SCORES
rank=1
ColorDistributionMeasure=0.683032
ComplexityMeasure=0.872849
LivelinessMeasure=0.972009
LuminanceDistributionMeasure=0.803951
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/87450.jpg]
mutation_2=[content_weight=750,style_weight=0.1]

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.46588640274382126
novelty_score=0.1779355995249304
surprise_score=0.0
