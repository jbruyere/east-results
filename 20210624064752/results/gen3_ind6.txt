IMAGE
name=gen3_ind6
content=dataset/output_images/gen2_ind13.jpg
style=dataset/output_images/gen2_ind9.jpg

SCORES
rank=1
ColorDistributionMeasure=0.589576
ComplexityMeasure=0.912143
LivelinessMeasure=0.974027
LuminanceDistributionMeasure=0.764866
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.40064440072456997
novelty_score=0.17897252412931958
surprise_score=0.4
