IMAGE
name=gen3_ind13
content=dataset/output_images/gen2_ind2.jpg
style=dataset/output_images/gen2_ind16.jpg

SCORES
rank=1
ColorDistributionMeasure=0.613634
ComplexityMeasure=0.947478
LivelinessMeasure=0.972251
LuminanceDistributionMeasure=0.794345
LuminanceRedundancyMeasure=0.99716

MUTATIONS
mutation_1=[content_weight=1000,style_weight=0.25]

METRICS
valuable=1
novel=0
surprising=0
creative=False


METRICS_VALUES
value_score=0.44774522514272835
novelty_score=0.1585276846277291
surprise_score=0.0
