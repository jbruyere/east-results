IMAGE
name=gen1_ind5
content=dataset/input_images/still-life-with-goblet.jpg
style=dataset/input_images/forest.jpg

SCORES
rank=2
ColorDistributionMeasure=0.282811
ComplexityMeasure=0.934226
LivelinessMeasure=0.962275
LuminanceDistributionMeasure=0.804641
LuminanceRedundancyMeasure=0.999294

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.2044291806318097
novelty_score=0.41485090775400646
surprise_score=0.8
