IMAGE
name=still-life-with-three-skulls
content=
style=

SCORES
rank=2
ColorDistributionMeasure=0.848268
ComplexityMeasure=0.992744
LivelinessMeasure=0.947382
LuminanceDistributionMeasure=0.797912
LuminanceRedundancyMeasure=0.951839

MUTATIONS

METRICS
valuable=1
novel=0
surprising=1
creative=False


METRICS_VALUES
value_score=0.6059181696131408
novelty_score=0
surprise_score=1.0
