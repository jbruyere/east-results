IMAGE
name=gen3_ind7
content=dataset/output_images/gen2_ind2.jpg
style=dataset/output_images/gen2_ind3.jpg

SCORES
rank=1
ColorDistributionMeasure=0.607912
ComplexityMeasure=0.896283
LivelinessMeasure=0.97274
LuminanceDistributionMeasure=0.822738
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/14377.jpg]

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.4357500912675218
novelty_score=0.17548789910443063
surprise_score=0.0
