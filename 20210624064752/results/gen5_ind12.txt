IMAGE
name=gen5_ind12
content=dataset/output_images/gen4_ind6.jpg
style=dataset/output_images/gen4_ind3.jpg

SCORES
rank=1
ColorDistributionMeasure=0.346366
ComplexityMeasure=0.927884
LivelinessMeasure=0.976171
LuminanceDistributionMeasure=0.713492
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.2238432226612302
novelty_score=0.32478411363664417
surprise_score=0.4
