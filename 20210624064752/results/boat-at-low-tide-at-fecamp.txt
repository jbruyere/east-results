IMAGE
name=boat-at-low-tide-at-fecamp
content=
style=

SCORES
rank=1
ColorDistributionMeasure=0.483883
ComplexityMeasure=0.947243
LivelinessMeasure=0.970713
LuminanceDistributionMeasure=0.855369
LuminanceRedundancyMeasure=0.988361

MUTATIONS

METRICS
valuable=1
novel=0
surprising=1
creative=False


METRICS_VALUES
value_score=0.376150567804015
novelty_score=0
surprise_score=1.0
