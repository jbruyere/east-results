IMAGE
name=gen3_ind5
content=dataset/output_images/gen2_ind12.jpg
style=dataset/output_images/gen2_ind9.jpg

SCORES
rank=1
ColorDistributionMeasure=0.594629
ComplexityMeasure=0.909115
LivelinessMeasure=0.9751
LuminanceDistributionMeasure=0.784383
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[content_weight=750,style_weight=0.005]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.41346831900393566
novelty_score=0.17481210287516422
surprise_score=0.2
