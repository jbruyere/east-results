IMAGE
name=gen4_ind13
content=dataset/output_images/gen3_ind15.jpg
style=dataset/output_images/gen3_ind1.jpg

SCORES
rank=1
ColorDistributionMeasure=0.667719
ComplexityMeasure=0.974301
LivelinessMeasure=0.968495
LuminanceDistributionMeasure=0.727829
LuminanceRedundancyMeasure=0.990602

MUTATIONS

METRICS
valuable=1
novel=0
surprising=0
creative=False


METRICS_VALUES
value_score=0.4542687081234672
novelty_score=0.15906516354811348
surprise_score=0.0
