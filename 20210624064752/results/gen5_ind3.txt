IMAGE
name=gen5_ind3
content=dataset/output_images/gen4_ind3.jpg
style=dataset/output_images/gen4_ind15.jpg

SCORES
rank=1
ColorDistributionMeasure=0.319701
ComplexityMeasure=0.93032
LivelinessMeasure=0.974507
LuminanceDistributionMeasure=0.798783
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.2315208609396355
novelty_score=0.3295207533985571
surprise_score=0.0
