Full run (10 gen) with rank selection

METRICS ON ALL
RANK 1 (78) - 51 novel, 67 surprising, 75 valuable = 40 creative
still-life-with-goblet -       surprising          
voix-du-jura-n-3296-2008 -       surprising          
costume-design-for-dance-of-the-seven-veils-1917 -       surprising          
gen1_ind13 - novel surprising valuable CREATIVE
gen5_ind0 - novel surprising valuable CREATIVE
gen1_ind19 - novel surprising valuable CREATIVE
gen5_ind12 - novel surprising valuable CREATIVE
william-henry-hunt-the-winter-dining-room-of-the-earl-of-essex-at-cassiobury-1821-meisterdrucke -       surprising valuable 
gen4_ind6 - novel surprising valuable CREATIVE
via-san-leonardo -       surprising valuable 
gen1_ind1 - novel surprising valuable CREATIVE
gen5_ind15 - novel            valuable 
lot-landscape-1890 -       surprising valuable 
gen4_ind8 - novel surprising valuable CREATIVE
gen6_ind9 - novel surprising valuable CREATIVE
gen4_ind17 - novel surprising valuable CREATIVE
gen6_ind8 - novel surprising valuable CREATIVE
mona-lisa-c-1503-1519 -       surprising valuable 
gen4_ind15 - novel surprising valuable CREATIVE
gen3_ind19 - novel surprising valuable CREATIVE
gen4_ind5 - novel surprising valuable CREATIVE
gen2_ind2 - novel            valuable 
buddha-moon-monk-sunrise-1999 -       surprising valuable 
marilyn-1 -       surprising valuable 
gen1_ind15 - novel surprising valuable CREATIVE
peasant-woman-with-buckets-1913 -       surprising valuable 
gen2_ind7 - novel surprising valuable CREATIVE
plate-77-belted-kingfisher -       surprising valuable 
gen6_ind1 - novel surprising valuable CREATIVE
gen6_ind3 - novel surprising valuable CREATIVE
gen2_ind6 - novel surprising valuable CREATIVE
gen9_ind9 - novel surprising valuable CREATIVE
gen10_ind12 - novel surprising valuable CREATIVE
gen10_ind8 - novel surprising valuable CREATIVE
gen10_ind1 - novel surprising valuable CREATIVE
gen9_ind19 - novel surprising valuable CREATIVE
gen3_ind1 -       surprising valuable 
boat-at-low-tide-at-fecamp -       surprising valuable 
landscape-under-snow-upper-norwood-1871 -       surprising valuable 
self-portrait-with-easel -       surprising valuable 
gen10_ind5 - novel surprising valuable CREATIVE
gen10_ind11 - novel            valuable 
gen9_ind7 - novel surprising valuable CREATIVE
gen2_ind3 - novel surprising valuable CREATIVE
gen2_ind14 -       surprising valuable 
gen3_ind8 -       surprising valuable 
gen7_ind2 - novel            valuable 
gen2_ind9 - novel surprising valuable CREATIVE
bouquet-of-peonies-on-a-musical-score-1876 -       surprising valuable 
gen6_ind16 - novel surprising valuable CREATIVE
the-jetty-at-le-havre-bad-weather-1870 -       surprising valuable 
gen2_ind12 - novel surprising valuable CREATIVE
gen1_ind3 -       surprising valuable 
gen10_ind4 - novel surprising valuable CREATIVE
gen2_ind13 - novel            valuable 
gen8_ind9 - novel            valuable 
gen9_ind11 - novel            valuable 
gen4_ind14 - novel surprising valuable CREATIVE
view-of-amalfi-1844 -       surprising valuable 
jacob-s-ladder -       surprising valuable 
cash-register-1917 -       surprising valuable 
gen2_ind8 - novel            valuable 
gen9_ind14 - novel            valuable 
gen7_ind10 - novel            valuable 
gen3_ind15 - novel surprising valuable CREATIVE
gen10_ind17 - novel            valuable 
gen2_ind10 - novel surprising valuable CREATIVE
gen10_ind9 - novel surprising valuable CREATIVE
gen4_ind16 - novel surprising valuable CREATIVE
gen8_ind14 - novel surprising valuable CREATIVE
gen1_ind9 - novel surprising valuable CREATIVE
gen9_ind0 -       surprising valuable 
gen9_ind16 -       surprising valuable 
gen4_ind4 -       surprising valuable 
randegg-in-the-snow-with-ravens -       surprising valuable 
gen3_ind11 - novel surprising valuable CREATIVE
gen8_ind8 - novel surprising valuable CREATIVE
gen8_ind18 - novel surprising valuable CREATIVE
ColorDistributionMeasure
MEAN    0.645122423076923
STD    0.1463389271670321
MIN    0.346366
Q1    0.5280175
Q2    0.6733885
Q3    0.7535045
MAX    0.926546
ComplexityMeasure
MEAN    0.9345321025641027
STD    0.04092185691440228
MIN    0.872907
Q1    0.8933530000000001
Q2    0.9385954999999999
Q3    0.9755480000000001
MAX    0.999554
LivelinessMeasure
MEAN    0.9686427692307692
STD    0.012977964055415958
MIN    0.885712
Q1    0.96816275
Q2    0.9723440000000001
Q3    0.9739835
MAX    0.976373
LuminanceDistributionMeasure
MEAN    0.8160075897435898
STD    0.06908120598891548
MIN    0.552842
Q1    0.7806819999999999
Q2    0.824453
Q3    0.85821575
MAX    0.958348
LuminanceRedundancyMeasure
MEAN    0.9962662820512821
STD    0.008150710870204992
MIN    0.96342
Q1    0.99716
Q2    1.0
Q3    1.0
MAX    1.0



RANK 2 (90) - 58 novel, 70 surprising, 88 valuable = 43 creative
gen5_ind13 - novel surprising          
gen4_ind3 - novel surprising valuable CREATIVE
gen5_ind14 - novel surprising valuable CREATIVE
gen5_ind16 - novel surprising valuable CREATIVE
gen5_ind8 - novel            valuable 
gen5_ind10 - novel surprising valuable CREATIVE
gen4_ind7 - novel surprising valuable CREATIVE
double-twist-1980 -       surprising          
dark-bridge-2003 -       surprising valuable 
linhas-de-sombra -       surprising valuable 
the-port-of-le-havre-1903 -       surprising valuable 
still-life-with-three-skulls -       surprising valuable 
gen4_ind18 - novel surprising valuable CREATIVE
gen2_ind19 - novel surprising valuable CREATIVE
gen3_ind4 - novel surprising valuable CREATIVE
gen8_ind10 - novel surprising valuable CREATIVE
the-great-wave-off-kanagawa -       surprising valuable 
gen2_ind4 - novel surprising valuable CREATIVE
water-lilies-1917 -       surprising valuable 
rooftops-venice-1983 -       surprising valuable 
forest -       surprising valuable 
seaweed-gatherers-at-omari -       surprising valuable 
george-caleb-bingham-boatmen-on-the-missouri-google-art-project -       surprising valuable 
the-tennis-court-oath-20th-june-1789-1791 -       surprising valuable 
vllandand-sea1948 -       surprising valuable 
gen7_ind13 - novel            valuable 
gen7_ind17 - novel surprising valuable CREATIVE
gen1_ind6 - novel surprising valuable CREATIVE
gen4_ind11 - novel surprising valuable CREATIVE
gen5_ind7 - novel surprising valuable CREATIVE
self-portrait-da-vinci -       surprising valuable 
gen5_ind18 - novel            valuable 
gen2_ind11 -                  valuable 
costume-design-for-salome-1917 -       surprising valuable 
marguerite-1975 -       surprising valuable 
gen2_ind16 - novel surprising valuable CREATIVE
gen3_ind0 -                  valuable 
gen5_ind2 - novel surprising valuable CREATIVE
gen7_ind15 - novel            valuable 
gen10_ind13 - novel surprising valuable CREATIVE
gen5_ind4 - novel surprising valuable CREATIVE
the-magpie-1869 -       surprising valuable 
gen6_ind4 - novel surprising valuable CREATIVE
gen10_ind2 - novel surprising valuable CREATIVE
gen1_ind2 - novel surprising valuable CREATIVE
gen2_ind18 - novel surprising valuable CREATIVE
gen4_ind12 - novel surprising valuable CREATIVE
napoleoncrossing-the-alps-at-the-st-bernard-pass-20th-may-1800-1801 -       surprising valuable 
gen10_ind19 - novel surprising valuable CREATIVE
woman-with-a-flower-1891 -       surprising valuable 
gen1_ind4 -       surprising valuable 
gen1_ind0 -       surprising valuable 
gen8_ind6 - novel surprising valuable CREATIVE
gen1_ind10 - novel surprising valuable CREATIVE
gen9_ind12 - novel surprising valuable CREATIVE
gen10_ind10 - novel            valuable 
gen1_ind16 - novel surprising valuable CREATIVE
gen9_ind3 - novel            valuable 
gen7_ind0 - novel            valuable 
gen1_ind14 - novel surprising valuable CREATIVE
american-500-sunset -       surprising valuable 
gen5_ind17 - novel surprising valuable CREATIVE
gen2_ind17 - novel surprising valuable CREATIVE
gen4_ind9 - novel surprising valuable CREATIVE
gen10_ind6 - novel            valuable 
gen3_ind2 -       surprising valuable 
gen8_ind0 - novel surprising valuable CREATIVE
gen8_ind7 - novel            valuable 
gen3_ind9 -       surprising valuable 
gen3_ind16 -                  valuable 
gen1_ind12 - novel surprising valuable CREATIVE
gen9_ind10 - novel            valuable 
gen3_ind6 - novel surprising valuable CREATIVE
gen8_ind1 - novel            valuable 
gen10_ind0 - novel surprising valuable CREATIVE
gen3_ind7 - novel            valuable 
gen10_ind18 -                  valuable 
gen10_ind16 - novel surprising valuable CREATIVE
gen8_ind19 - novel surprising valuable CREATIVE
gen4_ind2 -       surprising valuable 
gen4_ind1 -       surprising valuable 
gen5_ind9 -                  valuable 
gen3_ind5 - novel surprising valuable CREATIVE
gen9_ind13 - novel surprising valuable CREATIVE
gen7_ind16 - novel surprising valuable CREATIVE
gen3_ind13 -                  valuable 
gen6_ind14 - novel            valuable 
gen7_ind9 - novel            valuable 
gen6_ind6 - novel surprising valuable CREATIVE
gen3_ind18 - novel surprising valuable CREATIVE
ColorDistributionMeasure
MEAN    0.6308132222222222
STD    0.12885062321435778
MIN    0.286823
Q1    0.5808869999999999
Q2    0.6567735
Q3    0.7182585
MAX    0.87148
ComplexityMeasure
MEAN    0.9250738333333333
STD    0.03940827662821043
MIN    0.862874
Q1    0.8845755
Q2    0.9317695
Q3    0.95839575
MAX    0.992744
LivelinessMeasure
MEAN    0.9688418555555555
STD    0.007247788259663327
MIN    0.934038
Q1    0.9664602499999999
Q2    0.971257
Q3    0.97291175
MAX    0.976368
LuminanceDistributionMeasure
MEAN    0.7894354555555558
STD    0.04982182595412958
MIN    0.639112
Q1    0.7558847499999999
Q2    0.793247
Q3    0.818614
MAX    0.923642
LuminanceRedundancyMeasure
MEAN    0.9940716222222221
STD    0.01143760310902184
MIN    0.933451
Q1    0.994275
Q2    0.998586
Q3    1.0
MAX    1.0



RANK 3 (82) - 68 novel, 65 surprising, 77 valuable = 50 creative
well-by-the-winding-road-in-the-park-of-chateau-noir -       surprising          
gen1_ind7 - novel surprising          
prisoners-exercising-prisoners-round-1890 -       surprising          
gen1_ind5 - novel surprising          
coast-near-antibes -       surprising valuable 
gen5_ind3 - novel            valuable 
gen2_ind15 - novel surprising valuable CREATIVE
gen9_ind18 - novel surprising          
gen9_ind6 - novel surprising valuable CREATIVE
gen1_ind11 - novel surprising valuable CREATIVE
gen2_ind0 - novel surprising valuable CREATIVE
gen6_ind10 - novel surprising valuable CREATIVE
gen7_ind12 - novel surprising valuable CREATIVE
gen5_ind6 - novel surprising valuable CREATIVE
gen7_ind1 - novel            valuable 
gen6_ind13 - novel surprising valuable CREATIVE
gen10_ind3 - novel surprising valuable CREATIVE
gen3_ind10 - novel            valuable 
gen6_ind5 - novel            valuable 
gen6_ind12 - novel surprising valuable CREATIVE
gen5_ind19 - novel            valuable 
orfeu-nos-infernos-detail-1904 -       surprising valuable 
the-harvest-1890 -       surprising valuable 
gen7_ind4 - novel            valuable 
gen1_ind8 - novel surprising valuable CREATIVE
gen9_ind5 - novel surprising valuable CREATIVE
gen5_ind1 - novel surprising valuable CREATIVE
gen8_ind4 - novel surprising valuable CREATIVE
gen7_ind8 - novel            valuable 
gen7_ind7 - novel surprising valuable CREATIVE
gen3_ind3 - novel surprising valuable CREATIVE
gen8_ind5 - novel surprising valuable CREATIVE
gen8_ind2 - novel surprising valuable CREATIVE
gen6_ind7 - novel surprising valuable CREATIVE
gen2_ind5 - novel surprising valuable CREATIVE
gen4_ind19 - novel surprising valuable CREATIVE
gen10_ind15 - novel surprising valuable CREATIVE
gen6_ind19 - novel surprising valuable CREATIVE
gen4_ind13 -                  valuable 
gen9_ind2 - novel surprising valuable CREATIVE
view-of-the-tiber-and-castel-st-angelo-1776 -       surprising valuable 
gen10_ind14 - novel surprising valuable CREATIVE
gen8_ind13 - novel surprising valuable CREATIVE
still-life-vase-with-fifteen-sunflowers-1888-1 -       surprising valuable 
the-oath-of-horatii-1784 -       surprising valuable 
gen6_ind11 - novel surprising valuable CREATIVE
gen3_ind17 - novel surprising valuable CREATIVE
gen8_ind16 - novel surprising valuable CREATIVE
the-starry-night -       surprising valuable 
gen7_ind11 - novel surprising valuable CREATIVE
gen8_ind3 - novel surprising valuable CREATIVE
gen7_ind3 - novel surprising valuable CREATIVE
gen8_ind11 - novel surprising valuable CREATIVE
electrician-1915 -       surprising valuable 
gen9_ind8 - novel surprising valuable CREATIVE
gen8_ind17 - novel surprising valuable CREATIVE
gen10_ind7 - novel surprising valuable CREATIVE
gen7_ind5 - novel            valuable 
gen9_ind1 - novel surprising valuable CREATIVE
gen2_ind1 - novel            valuable 
gen9_ind17 - novel surprising valuable CREATIVE
gen9_ind15 - novel surprising valuable CREATIVE
gen3_ind14 - novel            valuable 
gen3_ind12 -                  valuable 
gen5_ind5 - novel surprising valuable CREATIVE
gen8_ind15 - novel surprising valuable CREATIVE
gen9_ind4 - novel surprising valuable CREATIVE
gen5_ind11 - novel surprising valuable CREATIVE
gen7_ind14 - novel            valuable 
gen8_ind12 - novel surprising valuable CREATIVE
gen7_ind19 - novel surprising valuable CREATIVE
gen7_ind18 - novel            valuable 
gen4_ind10 -       surprising valuable 
gen1_ind17 -       surprising valuable 
gen4_ind0 - novel surprising valuable CREATIVE
gen6_ind0 - novel            valuable 
gen6_ind18 - novel surprising valuable CREATIVE
gen7_ind6 - novel surprising valuable CREATIVE
gen1_ind18 - novel surprising valuable CREATIVE
gen6_ind2 - novel surprising valuable CREATIVE
gen6_ind15 - novel            valuable 
gen6_ind17 - novel            valuable 
ColorDistributionMeasure
MEAN    0.5431303414634147
STD    0.11715935599828205
MIN    0.258253
Q1    0.45893675
Q2    0.5757015000000001
Q3    0.6333125000000001
MAX    0.752159
ComplexityMeasure
MEAN    0.9024564878048782
STD    0.03765852129146347
MIN    0.848722
Q1    0.8704850000000001
Q2    0.8943030000000001
Q3    0.93018625
MAX    0.985795
LivelinessMeasure
MEAN    0.9689026585365855
STD    0.006430675474173339
MIN    0.924373
Q1    0.96840375
Q2    0.9702329999999999
Q3    0.97182875
MAX    0.974733
LuminanceDistributionMeasure
MEAN    0.7612765
STD    0.05141230266505452
MIN    0.473297
Q1    0.7380609999999999
Q2    0.7677355
Q3    0.79685675
MAX    0.852126
LuminanceRedundancyMeasure
MEAN    0.993763475609756
STD    0.009624618961384605
MIN    0.949057
Q1    0.9922635
Q2    0.997874
Q3    0.999294
MAX    1.0


METRICS PER GEN

GEN 0
ColorDistributionMeasure
MEAN    0.6707964200000001
STD     0.15344117118753886
MIN     0.277132
Q1      0.5753445
Q2      0.6875355000000001
Q3      0.7925825
MAX     0.926546
ComplexityMeasure
MEAN    0.96317598
STD     0.030642814442860832
MIN     0.85317
Q1      0.94878925
Q2      0.9673050000000001
Q3      0.9856125
MAX     0.999554
LivelinessMeasure
MEAN    0.9593559
STD     0.016502945685240557
MIN     0.885712
Q1      0.95658625
Q2      0.9652655
Q3      0.96820225
MAX     0.973877
LuminanceDistributionMeasure
MEAN    0.7887231
STD     0.08576984630911962
MIN     0.473297
Q1      0.7537645
Q2      0.7989455
Q3      0.8361455
MAX     0.958348
LuminanceRedundancyMeasure
MEAN    0.9830871999999999
STD     0.016228830872247076
MIN     0.933451
Q1      0.9695692499999999
Q2      0.989111
Q3      0.9955422500000001
MAX     1.0



GEN 1
ColorDistributionMeasure
MEAN    0.5924934000000001
STD     0.17030905305837385
MIN     0.258253
Q1      0.52865575
Q2      0.6030835
Q3      0.71921425
MAX     0.876147
ComplexityMeasure
MEAN    0.9473796
STD     0.029419733588868548
MIN     0.888133
Q1      0.9322314999999999
Q2      0.9555880000000001
Q3      0.966937
MAX     0.998987
LivelinessMeasure
MEAN    0.9682692500000002
STD     0.003581779193012885
MIN     0.962275
Q1      0.96552525
Q2      0.968526
Q3      0.97061925
MAX     0.975168
LuminanceDistributionMeasure
MEAN    0.7989864499999999
STD     0.04889729143262948
MIN     0.678388
Q1      0.77833725
Q2      0.8058860000000001
Q3      0.8191550000000001
MAX     0.926337
LuminanceRedundancyMeasure
MEAN    0.99686615
STD     0.003966069455708017
MIN     0.986852
Q1      0.995361
Q2      0.9989399999999999
Q3      0.999294
MAX     1.0



GEN 2
ColorDistributionMeasure
MEAN    0.58979635
STD     0.14249810529171084
MIN     0.32332
Q1      0.48737874999999997
Q2      0.5456235
Q3      0.73003075
MAX     0.795559
ComplexityMeasure
MEAN    0.9400606500000002
STD     0.03485429316924244
MIN     0.864779
Q1      0.91683925
Q2      0.944805
Q3      0.9648015000000001
MAX     0.990354
LivelinessMeasure
MEAN    0.9715895499999998
STD     0.0035529014829432026
MIN     0.96241
Q1      0.96969675
Q2      0.971433
Q3      0.9748239999999999
MAX     0.975818
LuminanceDistributionMeasure
MEAN    0.7732237000000002
STD     0.046395057156016085
MIN     0.639112
Q1      0.75741825
Q2      0.7785455
Q3      0.8025370000000001
MAX     0.849599
LuminanceRedundancyMeasure
MEAN    0.9956573500000003
STD     0.007144930883325592
MIN     0.971848
Q1      0.9947967499999999
Q2      0.999294
Q3      1.0
MAX     1.0



GEN 3
ColorDistributionMeasure
MEAN    0.6086501500000001
STD     0.09276634711266527
MIN     0.413824
Q1      0.58621925
Q2      0.6162245
Q3      0.6818787500000001
MAX     0.761618
ComplexityMeasure
MEAN    0.9396245000000001
STD     0.03316826736128978
MIN     0.892398
Q1      0.9064215
Q2      0.9439555
Q3      0.9654097500000001
MAX     0.996909
LivelinessMeasure
MEAN    0.9709875
STD     0.0036730216920132603
MIN     0.963456
Q1      0.96864925
Q2      0.971916
Q3      0.974089
MAX     0.975271
LuminanceDistributionMeasure
MEAN    0.7906980499999999
STD     0.04228079024270359
MIN     0.677934
Q1      0.7771677499999999
Q2      0.790183
Q3      0.8232117499999999
MAX     0.864836
LuminanceRedundancyMeasure
MEAN    0.9968543000000001
STD     0.004487387459313041
MIN     0.984565
Q1      0.9966200000000001
Q2      0.998584
Q3      1.0
MAX     1.0



GEN 4
ColorDistributionMeasure
MEAN    0.5625517499999999
STD     0.1456274206593233
MIN     0.303257
Q1      0.407503
Q2      0.6219895
Q3      0.67032525
MAX     0.795607
ComplexityMeasure
MEAN    0.93092625
STD     0.03125084714191761
MIN     0.878956
Q1      0.9008765
Q2      0.940005
Q3      0.9506107500000001
MAX     0.986405
LivelinessMeasure
MEAN    0.9711058999999999
STD     0.004617364799320075
MIN     0.960337
Q1      0.968951
Q2      0.972619
Q3      0.974167
MAX     0.976373
LuminanceDistributionMeasure
MEAN    0.7773093999999998
STD     0.05771099340195073
MIN     0.68956
Q1      0.72761825
Q2      0.773544
Q3      0.81804275
MAX     0.891595
LuminanceRedundancyMeasure
MEAN    0.99657335
STD     0.004225986763762998
MIN     0.986852
Q1      0.99481875
Q2      0.998584
Q3      1.0
MAX     1.0



GEN 5
ColorDistributionMeasure
MEAN    0.4894341
STD     0.1602938178314747
MIN     0.286823
Q1      0.3615385
Q2      0.40931850000000003
Q3      0.63747975
MAX     0.753946
ComplexityMeasure
MEAN    0.9214613500000001
STD     0.01951973955327017
MIN     0.884567
Q1      0.9133385
Q2      0.9275425
Q3      0.93259925
MAX     0.956105
LivelinessMeasure
MEAN    0.97168685
STD     0.00377340997341927
MIN     0.964981
Q1      0.9685754999999999
Q2      0.972982
Q3      0.9749002499999999
MAX     0.976368
LuminanceDistributionMeasure
MEAN    0.7953898500000001
STD     0.06113996938523522
MIN     0.699703
Q1      0.741307
Q2      0.8039084999999999
Q3      0.8436145
MAX     0.923397
LuminanceRedundancyMeasure
MEAN    0.99802825
STD     0.002961011953285571
MIN     0.990602
Q1      0.9975162500000001
Q2      1.0
Q3      1.0
MAX     1.0



GEN 6
ColorDistributionMeasure
MEAN    0.5549879499999999
STD     0.09481230691712707
MIN     0.403699
Q1      0.471323
Q2      0.5862134999999999
Q3      0.63614525
MAX     0.712276
ComplexityMeasure
MEAN    0.90670225
STD     0.019982490822905437
MIN     0.871689
Q1      0.8876249999999999
Q2      0.912678
Q3      0.9196422499999999
MAX     0.942829
LivelinessMeasure
MEAN    0.97030025
STD     0.002588659167889814
MIN     0.963802
Q1      0.9692040000000001
Q2      0.9703435
Q3      0.97175775
MAX     0.974148
LuminanceDistributionMeasure
MEAN    0.78462615
STD     0.07662012183707033
MIN     0.673149
Q1      0.72221
Q2      0.775114
Q3      0.83037475
MAX     0.928982
LuminanceRedundancyMeasure
MEAN    0.9956982999999999
STD     0.004699103330423795
MIN     0.98533
Q1      0.9926305
Q2      0.998586
Q3      1.0
MAX     1.0



GEN 7
ColorDistributionMeasure
MEAN    0.5793060999999999
STD     0.09359684187562099
MIN     0.396298
Q1      0.52288975
Q2      0.591537
Q3      0.64374125
MAX     0.707579
ComplexityMeasure
MEAN    0.88661145
STD     0.01716232316871757
MIN     0.851475
Q1      0.8742505
Q2      0.8839079999999999
Q3      0.903404
MAX     0.912494
LivelinessMeasure
MEAN    0.9710692
STD     0.0017358220415699315
MIN     0.968385
Q1      0.96919675
Q2      0.9711615
Q3      0.9726025
MAX     0.974256
LuminanceDistributionMeasure
MEAN    0.8054743
STD     0.0418269425754501
MIN     0.708284
Q1      0.7923052500000001
Q2      0.8017525000000001
Q3      0.8355095
MAX     0.877473
LuminanceRedundancyMeasure
MEAN    0.9983620999999999
STD     0.0019362640548231049
MIN     0.992814
Q1      0.997874
Q2      0.9989399999999999
Q3      1.0
MAX     1.0



GEN 8
ColorDistributionMeasure
MEAN    0.6261515500000001
STD     0.06582959889098142
MIN     0.495467
Q1      0.5981285000000001
Q2      0.6370585
Q3      0.6832865
MAX     0.702685
ComplexityMeasure
MEAN    0.8786811999999997
STD     0.012424839864561636
MIN     0.859984
Q1      0.8703212500000002
Q2      0.874495
Q3      0.88488975
MAX     0.903493
LivelinessMeasure
MEAN    0.97184205
STD     0.0014945591147559353
MIN     0.96925
Q1      0.97029375
Q2      0.9720610000000001
Q3      0.9729362500000001
MAX     0.97426
LuminanceDistributionMeasure
MEAN    0.7879515
STD     0.04573680920932285
MIN     0.712677
Q1      0.7558042500000001
Q2      0.7876110000000001
Q3      0.80624275
MAX     0.88794
LuminanceRedundancyMeasure
MEAN    0.99907905
STD     0.0010070369146659894
MIN     0.996443
Q1      0.998586
Q2      0.999294
Q3      1.0
MAX     1.0



GEN 9
ColorDistributionMeasure
MEAN    0.6232506499999999
STD     0.11014627628307504
MIN     0.338263
Q1      0.6088325
Q2      0.6518485
Q3      0.692183
MAX     0.752831
ComplexityMeasure
MEAN    0.8736511499999999
STD     0.01405122250295327
MIN     0.848722
Q1      0.86412925
Q2      0.8718349999999999
Q3      0.8846010000000001
MAX     0.895548
LivelinessMeasure
MEAN    0.9719534
STD     0.0016867244706827468
MIN     0.967229
Q1      0.97117075
Q2      0.9720625
Q3      0.9731682500000001
MAX     0.974427
LuminanceDistributionMeasure
MEAN    0.78654275
STD     0.04471624649372418
MIN     0.717654
Q1      0.745304
Q2      0.785504
Q3      0.8180005000000001
MAX     0.860888
LuminanceRedundancyMeasure
MEAN    0.9983895
STD     0.0026025392120004737
MIN     0.990602
Q1      0.998408
Q2      0.9989399999999999
Q3      1.0
MAX     1.0



GEN 10
ColorDistributionMeasure
MEAN    0.6778582999999999
STD     0.07715935198995648
MIN     0.458762
Q1      0.66726125
Q2      0.690015
Q3      0.73966325
MAX     0.761218
ComplexityMeasure
MEAN    0.8745407000000001
STD     0.008444668093536887
MIN     0.853336
Q1      0.8720305
Q2      0.876427
Q3      0.8799997500000001
MAX     0.887434
LivelinessMeasure
MEAN    0.9728023499999999
STD     0.0008334698119908185
MIN     0.971479
Q1      0.97224275
Q2      0.972718
Q3      0.97338525
MAX     0.974506
LuminanceDistributionMeasure
MEAN    0.7841129
STD     0.047564487977797054
MIN     0.717005
Q1      0.73452475
Q2      0.782687
Q3      0.8240744999999999
MAX     0.858281
LuminanceRedundancyMeasure
MEAN    0.9999646999999999
STD     0.00015386913270698237
MIN     0.999294
Q1      1.0
Q2      1.0
Q3      1.0
MAX     1.0






RANKED (0 -> best)
RANKS GEN 0
0 - costume-design-for-salome-1917 -   value surprise   
1 - well-by-the-winding-road-in-the-park-of-chateau-noir -     surprise   
2 - the-port-of-le-havre-1903 -   value surprise   
3 - electrician-1915 -   value surprise   
4 - coast-near-antibes -   value surprise   
5 - still-life-vase-with-fifteen-sunflowers-1888-1 -   value surprise   
6 - marguerite-1975 -   value surprise   
7 - forest -   value surprise   
8 - self-portrait-da-vinci -   value surprise   
9 - water-lilies-1917 -   value surprise   
10 - prisoners-exercising-prisoners-round-1890 -     surprise   
11 - napoleoncrossing-the-alps-at-the-st-bernard-pass-20th-may-1800-1801 -   value surprise   
12 - the-starry-night -   value surprise   
13 - vllandand-sea1948 -   value surprise   
14 - double-twist-1980 -     surprise   
15 - the-magpie-1869 -   value surprise   
16 - the-tennis-court-oath-20th-june-1789-1791 -   value surprise   
17 - orfeu-nos-infernos-detail-1904 -   value surprise   
18 - the-harvest-1890 -   value surprise   
19 - george-caleb-bingham-boatmen-on-the-missouri-google-art-project -   value surprise   
20 - seaweed-gatherers-at-omari -   value surprise   
21 - the-oath-of-horatii-1784 -   value surprise   
22 - still-life-with-three-skulls -   value surprise   
23 - view-of-the-tiber-and-castel-st-angelo-1776 -   value surprise   
24 - linhas-de-sombra -   value surprise   
25 - william-henry-hunt-the-winter-dining-room-of-the-earl-of-essex-at-cassiobury-1821-meisterdrucke -   value surprise   
26 - american-500-sunset -   value surprise   
27 - voix-du-jura-n-3296-2008 -     surprise   
28 - buddha-moon-monk-sunrise-1999 -   value surprise   
29 - woman-with-a-flower-1891 -   value surprise   
30 - lot-landscape-1890 -   value surprise   
31 - marilyn-1 -   value surprise   
32 - landscape-under-snow-upper-norwood-1871 -   value surprise   
33 - randegg-in-the-snow-with-ravens -   value surprise   
34 - rooftops-venice-1983 -   value surprise   
35 - still-life-with-goblet -     surprise   
36 - boat-at-low-tide-at-fecamp -   value surprise   
37 - view-of-amalfi-1844 -   value surprise   
38 - costume-design-for-dance-of-the-seven-veils-1917 -     surprise   
39 - the-great-wave-off-kanagawa -   value surprise   
40 - cash-register-1917 -   value surprise   
41 - peasant-woman-with-buckets-1913 -   value surprise   
42 - plate-77-belted-kingfisher -   value surprise   
43 - via-san-leonardo -   value surprise   
44 - jacob-s-ladder -   value surprise   
45 - self-portrait-with-easel -   value surprise   
46 - the-jetty-at-le-havre-bad-weather-1870 -   value surprise   
47 - dark-bridge-2003 -   value surprise   
48 - bouquet-of-peonies-on-a-musical-score-1876 -   value surprise   
49 - mona-lisa-c-1503-1519 -   value surprise   
RANKS GEN 0 BY VALUE SCORE
0 - costume-design-for-dance-of-the-seven-veils-1917 -     surprise   
1 - lot-landscape-1890 -   value surprise   
2 - william-henry-hunt-the-winter-dining-room-of-the-earl-of-essex-at-cassiobury-1821-meisterdrucke -   value surprise   
3 - mona-lisa-c-1503-1519 -   value surprise   
4 - via-san-leonardo -   value surprise   
5 - peasant-woman-with-buckets-1913 -   value surprise   
6 - plate-77-belted-kingfisher -   value surprise   
7 - still-life-with-three-skulls -   value surprise   
8 - george-caleb-bingham-boatmen-on-the-missouri-google-art-project -   value surprise   
9 - marilyn-1 -   value surprise   
10 - dark-bridge-2003 -   value surprise   
11 - buddha-moon-monk-sunrise-1999 -   value surprise   
12 - costume-design-for-salome-1917 -   value surprise   
13 - the-great-wave-off-kanagawa -   value surprise   
14 - marguerite-1975 -   value surprise   
15 - self-portrait-da-vinci -   value surprise   
16 - bouquet-of-peonies-on-a-musical-score-1876 -   value surprise   
17 - rooftops-venice-1983 -   value surprise   
18 - cash-register-1917 -   value surprise   
19 - vllandand-sea1948 -   value surprise   
20 - american-500-sunset -   value surprise   
21 - the-jetty-at-le-havre-bad-weather-1870 -   value surprise   
22 - self-portrait-with-easel -   value surprise   
23 - seaweed-gatherers-at-omari -   value surprise   
24 - double-twist-1980 -     surprise   
25 - woman-with-a-flower-1891 -   value surprise   
26 - the-oath-of-horatii-1784 -   value surprise   
27 - view-of-amalfi-1844 -   value surprise   
28 - jacob-s-ladder -   value surprise   
29 - electrician-1915 -   value surprise   
30 - randegg-in-the-snow-with-ravens -   value surprise   
31 - still-life-with-goblet -     surprise   
32 - napoleoncrossing-the-alps-at-the-st-bernard-pass-20th-may-1800-1801 -   value surprise   
33 - voix-du-jura-n-3296-2008 -     surprise   
34 - the-magpie-1869 -   value surprise   
35 - view-of-the-tiber-and-castel-st-angelo-1776 -   value surprise   
36 - still-life-vase-with-fifteen-sunflowers-1888-1 -   value surprise   
37 - landscape-under-snow-upper-norwood-1871 -   value surprise   
38 - boat-at-low-tide-at-fecamp -   value surprise   
39 - the-starry-night -   value surprise   
40 - forest -   value surprise   
41 - the-tennis-court-oath-20th-june-1789-1791 -   value surprise   
42 - orfeu-nos-infernos-detail-1904 -   value surprise   
43 - water-lilies-1917 -   value surprise   
44 - the-port-of-le-havre-1903 -   value surprise   
45 - the-harvest-1890 -   value surprise   
46 - well-by-the-winding-road-in-the-park-of-chateau-noir -     surprise   
47 - linhas-de-sombra -   value surprise   
48 - coast-near-antibes -   value surprise   
49 - prisoners-exercising-prisoners-round-1890 -     surprise   
RANKS GEN 1
0 - gen1_ind14 - novel value surprise CREATIVE 
1 - gen1_ind18 - novel value surprise CREATIVE 
2 - gen1_ind5 - novel   surprise   
3 - gen1_ind6 - novel value surprise CREATIVE 
4 - gen1_ind12 - novel value surprise CREATIVE 
5 - gen1_ind4 -   value surprise   
6 - gen1_ind7 - novel   surprise   
7 - gen1_ind8 - novel value surprise CREATIVE 
8 - gen1_ind0 -   value surprise   
9 - gen1_ind16 - novel value surprise CREATIVE 
10 - gen1_ind1 - novel value surprise CREATIVE 
11 - gen1_ind17 -   value surprise   
12 - gen1_ind15 - novel value surprise CREATIVE 
13 - gen1_ind13 - novel value surprise CREATIVE 
14 - gen1_ind19 - novel value surprise CREATIVE 
15 - gen1_ind9 - novel value surprise CREATIVE 
16 - gen1_ind10 - novel value surprise CREATIVE 
17 - gen1_ind3 -   value surprise   
18 - gen1_ind11 - novel value surprise CREATIVE 
19 - gen1_ind2 - novel value surprise CREATIVE 
RANKS GEN 1 BY VALUE SCORE
0 - gen1_ind1 - novel value surprise CREATIVE 
1 - gen1_ind19 - novel value surprise CREATIVE 
2 - gen1_ind15 - novel value surprise CREATIVE 
3 - gen1_ind4 -   value surprise   
4 - gen1_ind0 -   value surprise   
5 - gen1_ind3 -   value surprise   
6 - gen1_ind2 - novel value surprise CREATIVE 
7 - gen1_ind17 -   value surprise   
8 - gen1_ind9 - novel value surprise CREATIVE 
9 - gen1_ind12 - novel value surprise CREATIVE 
10 - gen1_ind18 - novel value surprise CREATIVE 
11 - gen1_ind14 - novel value surprise CREATIVE 
12 - gen1_ind16 - novel value surprise CREATIVE 
13 - gen1_ind10 - novel value surprise CREATIVE 
14 - gen1_ind6 - novel value surprise CREATIVE 
15 - gen1_ind8 - novel value surprise CREATIVE 
16 - gen1_ind13 - novel value surprise CREATIVE 
17 - gen1_ind11 - novel value surprise CREATIVE 
18 - gen1_ind5 - novel   surprise   
19 - gen1_ind7 - novel   surprise   
RANKS GEN 2
0 - gen2_ind5 - novel value surprise CREATIVE 
1 - gen2_ind15 - novel value surprise CREATIVE 
2 - gen2_ind4 - novel value surprise CREATIVE 
3 - gen2_ind0 - novel value surprise CREATIVE 
4 - gen2_ind18 - novel value surprise CREATIVE 
5 - gen2_ind8 - novel value     
6 - gen2_ind14 -   value surprise   
7 - gen2_ind6 - novel value surprise CREATIVE 
8 - gen2_ind12 - novel value surprise CREATIVE 
9 - gen2_ind11 -   value     
10 - gen2_ind7 - novel value surprise CREATIVE 
11 - gen2_ind3 - novel value surprise CREATIVE 
12 - gen2_ind1 - novel value     
13 - gen2_ind10 - novel value surprise CREATIVE 
14 - gen2_ind13 - novel value     
15 - gen2_ind9 - novel value surprise CREATIVE 
16 - gen2_ind2 - novel value     
17 - gen2_ind17 - novel value surprise CREATIVE 
18 - gen2_ind16 - novel value surprise CREATIVE 
19 - gen2_ind19 - novel value surprise CREATIVE 
RANKS GEN 2 BY VALUE SCORE
0 - gen2_ind2 - novel value     
1 - gen2_ind7 - novel value surprise CREATIVE 
2 - gen2_ind11 -   value     
3 - gen2_ind14 -   value surprise   
4 - gen2_ind16 - novel value surprise CREATIVE 
5 - gen2_ind17 - novel value surprise CREATIVE 
6 - gen2_ind19 - novel value surprise CREATIVE 
7 - gen2_ind10 - novel value surprise CREATIVE 
8 - gen2_ind12 - novel value surprise CREATIVE 
9 - gen2_ind8 - novel value     
10 - gen2_ind13 - novel value     
11 - gen2_ind1 - novel value     
12 - gen2_ind18 - novel value surprise CREATIVE 
13 - gen2_ind3 - novel value surprise CREATIVE 
14 - gen2_ind9 - novel value surprise CREATIVE 
15 - gen2_ind6 - novel value surprise CREATIVE 
16 - gen2_ind4 - novel value surprise CREATIVE 
17 - gen2_ind5 - novel value surprise CREATIVE 
18 - gen2_ind0 - novel value surprise CREATIVE 
19 - gen2_ind15 - novel value surprise CREATIVE 
RANKS GEN 3
0 - gen3_ind17 - novel value surprise CREATIVE 
1 - gen3_ind10 - novel value     
2 - gen3_ind12 -   value     
3 - gen3_ind14 - novel value     
4 - gen3_ind3 - novel value surprise CREATIVE 
5 - gen3_ind16 -   value     
6 - gen3_ind4 - novel value surprise CREATIVE 
7 - gen3_ind18 - novel value surprise CREATIVE 
8 - gen3_ind7 - novel value     
9 - gen3_ind19 - novel value surprise CREATIVE 
10 - gen3_ind13 -   value     
11 - gen3_ind11 - novel value surprise CREATIVE 
12 - gen3_ind8 -   value surprise   
13 - gen3_ind2 -   value surprise   
14 - gen3_ind9 -   value surprise   
15 - gen3_ind6 - novel value surprise CREATIVE 
16 - gen3_ind5 - novel value surprise CREATIVE 
17 - gen3_ind0 -   value     
18 - gen3_ind1 -   value surprise   
19 - gen3_ind15 - novel value surprise CREATIVE 
RANKS GEN 3 BY VALUE SCORE
0 - gen3_ind1 -   value surprise   
1 - gen3_ind8 -   value surprise   
2 - gen3_ind0 -   value     
3 - gen3_ind9 -   value surprise   
4 - gen3_ind2 -   value surprise   
5 - gen3_ind16 -   value     
6 - gen3_ind12 -   value     
7 - gen3_ind13 -   value     
8 - gen3_ind7 - novel value     
9 - gen3_ind18 - novel value surprise CREATIVE 
10 - gen3_ind14 - novel value     
11 - gen3_ind11 - novel value surprise CREATIVE 
12 - gen3_ind5 - novel value surprise CREATIVE 
13 - gen3_ind6 - novel value surprise CREATIVE 
14 - gen3_ind15 - novel value surprise CREATIVE 
15 - gen3_ind17 - novel value surprise CREATIVE 
16 - gen3_ind3 - novel value surprise CREATIVE 
17 - gen3_ind4 - novel value surprise CREATIVE 
18 - gen3_ind19 - novel value surprise CREATIVE 
19 - gen3_ind10 - novel value     
RANKS GEN 4
0 - gen4_ind19 - novel value surprise CREATIVE 
1 - gen4_ind10 -   value surprise   
2 - gen4_ind14 - novel value surprise CREATIVE 
3 - gen4_ind0 - novel value surprise CREATIVE 
4 - gen4_ind5 - novel value surprise CREATIVE 
5 - gen4_ind9 - novel value surprise CREATIVE 
6 - gen4_ind15 - novel value surprise CREATIVE 
7 - gen4_ind2 -   value surprise   
8 - gen4_ind1 -   value surprise   
9 - gen4_ind17 - novel value surprise CREATIVE 
10 - gen4_ind13 -   value     
11 - gen4_ind4 -   value surprise   
12 - gen4_ind12 - novel value surprise CREATIVE 
13 - gen4_ind16 - novel value surprise CREATIVE 
14 - gen4_ind3 - novel value surprise CREATIVE 
15 - gen4_ind11 - novel value surprise CREATIVE 
16 - gen4_ind18 - novel value surprise CREATIVE 
17 - gen4_ind8 - novel value surprise CREATIVE 
18 - gen4_ind6 - novel value surprise CREATIVE 
19 - gen4_ind7 - novel value surprise CREATIVE 
RANKS GEN 4 BY VALUE SCORE
0 - gen4_ind5 - novel value surprise CREATIVE 
1 - gen4_ind11 - novel value surprise CREATIVE 
2 - gen4_ind2 -   value surprise   
3 - gen4_ind1 -   value surprise   
4 - gen4_ind12 - novel value surprise CREATIVE 
5 - gen4_ind4 -   value surprise   
6 - gen4_ind10 -   value surprise   
7 - gen4_ind13 -   value     
8 - gen4_ind16 - novel value surprise CREATIVE 
9 - gen4_ind9 - novel value surprise CREATIVE 
10 - gen4_ind0 - novel value surprise CREATIVE 
11 - gen4_ind14 - novel value surprise CREATIVE 
12 - gen4_ind17 - novel value surprise CREATIVE 
13 - gen4_ind19 - novel value surprise CREATIVE 
14 - gen4_ind18 - novel value surprise CREATIVE 
15 - gen4_ind15 - novel value surprise CREATIVE 
16 - gen4_ind7 - novel value surprise CREATIVE 
17 - gen4_ind8 - novel value surprise CREATIVE 
18 - gen4_ind6 - novel value surprise CREATIVE 
19 - gen4_ind3 - novel value surprise CREATIVE 
RANKS GEN 5
0 - gen5_ind14 - novel value surprise CREATIVE 
1 - gen5_ind15 - novel value     
2 - gen5_ind1 - novel value surprise CREATIVE 
3 - gen5_ind12 - novel value surprise CREATIVE 
4 - gen5_ind2 - novel value surprise CREATIVE 
5 - gen5_ind4 - novel value surprise CREATIVE 
6 - gen5_ind8 - novel value     
7 - gen5_ind0 - novel value surprise CREATIVE 
8 - gen5_ind9 -   value     
9 - gen5_ind17 - novel value surprise CREATIVE 
10 - gen5_ind18 - novel value     
11 - gen5_ind11 - novel value surprise CREATIVE 
12 - gen5_ind19 - novel value     
13 - gen5_ind7 - novel value surprise CREATIVE 
14 - gen5_ind16 - novel value surprise CREATIVE 
15 - gen5_ind13 - novel   surprise   
16 - gen5_ind6 - novel value surprise CREATIVE 
17 - gen5_ind3 - novel value     
18 - gen5_ind10 - novel value surprise CREATIVE 
19 - gen5_ind5 - novel value surprise CREATIVE 
RANKS GEN 5 BY VALUE SCORE
0 - gen5_ind7 - novel value surprise CREATIVE 
1 - gen5_ind2 - novel value surprise CREATIVE 
2 - gen5_ind18 - novel value     
3 - gen5_ind17 - novel value surprise CREATIVE 
4 - gen5_ind9 -   value     
5 - gen5_ind11 - novel value surprise CREATIVE 
6 - gen5_ind4 - novel value surprise CREATIVE 
7 - gen5_ind5 - novel value surprise CREATIVE 
8 - gen5_ind1 - novel value surprise CREATIVE 
9 - gen5_ind0 - novel value surprise CREATIVE 
10 - gen5_ind19 - novel value     
11 - gen5_ind10 - novel value surprise CREATIVE 
12 - gen5_ind6 - novel value surprise CREATIVE 
13 - gen5_ind16 - novel value surprise CREATIVE 
14 - gen5_ind8 - novel value     
15 - gen5_ind15 - novel value     
16 - gen5_ind3 - novel value     
17 - gen5_ind12 - novel value surprise CREATIVE 
18 - gen5_ind14 - novel value surprise CREATIVE 
19 - gen5_ind13 - novel   surprise   
RANKS GEN 6
0 - gen6_ind10 - novel value surprise CREATIVE 
1 - gen6_ind12 - novel value surprise CREATIVE 
2 - gen6_ind19 - novel value surprise CREATIVE 
3 - gen6_ind7 - novel value surprise CREATIVE 
4 - gen6_ind4 - novel value surprise CREATIVE 
5 - gen6_ind0 - novel value     
6 - gen6_ind6 - novel value surprise CREATIVE 
7 - gen6_ind9 - novel value surprise CREATIVE 
8 - gen6_ind5 - novel value     
9 - gen6_ind18 - novel value surprise CREATIVE 
10 - gen6_ind13 - novel value surprise CREATIVE 
11 - gen6_ind3 - novel value surprise CREATIVE 
12 - gen6_ind17 - novel value     
13 - gen6_ind8 - novel value surprise CREATIVE 
14 - gen6_ind15 - novel value     
15 - gen6_ind2 - novel value surprise CREATIVE 
16 - gen6_ind11 - novel value surprise CREATIVE 
17 - gen6_ind14 - novel value     
18 - gen6_ind1 - novel value surprise CREATIVE 
19 - gen6_ind16 - novel value surprise CREATIVE 
RANKS GEN 6 BY VALUE SCORE
0 - gen6_ind16 - novel value surprise CREATIVE 
1 - gen6_ind14 - novel value     
2 - gen6_ind17 - novel value     
3 - gen6_ind4 - novel value surprise CREATIVE 
4 - gen6_ind2 - novel value surprise CREATIVE 
5 - gen6_ind6 - novel value surprise CREATIVE 
6 - gen6_ind0 - novel value     
7 - gen6_ind1 - novel value surprise CREATIVE 
8 - gen6_ind15 - novel value     
9 - gen6_ind18 - novel value surprise CREATIVE 
10 - gen6_ind3 - novel value surprise CREATIVE 
11 - gen6_ind19 - novel value surprise CREATIVE 
12 - gen6_ind11 - novel value surprise CREATIVE 
13 - gen6_ind9 - novel value surprise CREATIVE 
14 - gen6_ind7 - novel value surprise CREATIVE 
15 - gen6_ind8 - novel value surprise CREATIVE 
16 - gen6_ind5 - novel value     
17 - gen6_ind12 - novel value surprise CREATIVE 
18 - gen6_ind13 - novel value surprise CREATIVE 
19 - gen6_ind10 - novel value surprise CREATIVE 
RANKS GEN 7
0 - gen7_ind1 - novel value     
1 - gen7_ind5 - novel value     
2 - gen7_ind12 - novel value surprise CREATIVE 
3 - gen7_ind8 - novel value     
4 - gen7_ind4 - novel value     
5 - gen7_ind19 - novel value surprise CREATIVE 
6 - gen7_ind14 - novel value     
7 - gen7_ind11 - novel value surprise CREATIVE 
8 - gen7_ind18 - novel value     
9 - gen7_ind9 - novel value     
10 - gen7_ind3 - novel value surprise CREATIVE 
11 - gen7_ind7 - novel value surprise CREATIVE 
12 - gen7_ind2 - novel value     
13 - gen7_ind16 - novel value surprise CREATIVE 
14 - gen7_ind13 - novel value     
15 - gen7_ind6 - novel value surprise CREATIVE 
16 - gen7_ind17 - novel value surprise CREATIVE 
17 - gen7_ind10 - novel value     
18 - gen7_ind15 - novel value     
19 - gen7_ind0 - novel value     
RANKS GEN 7 BY VALUE SCORE
0 - gen7_ind2 - novel value     
1 - gen7_ind0 - novel value     
2 - gen7_ind11 - novel value surprise CREATIVE 
3 - gen7_ind3 - novel value surprise CREATIVE 
4 - gen7_ind5 - novel value     
5 - gen7_ind9 - novel value     
6 - gen7_ind6 - novel value surprise CREATIVE 
7 - gen7_ind10 - novel value     
8 - gen7_ind16 - novel value surprise CREATIVE 
9 - gen7_ind18 - novel value     
10 - gen7_ind17 - novel value surprise CREATIVE 
11 - gen7_ind19 - novel value surprise CREATIVE 
12 - gen7_ind14 - novel value     
13 - gen7_ind15 - novel value     
14 - gen7_ind13 - novel value     
15 - gen7_ind4 - novel value     
16 - gen7_ind7 - novel value surprise CREATIVE 
17 - gen7_ind8 - novel value     
18 - gen7_ind12 - novel value surprise CREATIVE 
19 - gen7_ind1 - novel value     
RANKS GEN 8
0 - gen8_ind12 - novel value surprise CREATIVE 
1 - gen8_ind5 - novel value surprise CREATIVE 
2 - gen8_ind11 - novel value surprise CREATIVE 
3 - gen8_ind19 - novel value surprise CREATIVE 
4 - gen8_ind4 - novel value surprise CREATIVE 
5 - gen8_ind10 - novel value surprise CREATIVE 
6 - gen8_ind0 - novel value surprise CREATIVE 
7 - gen8_ind8 - novel value surprise CREATIVE 
8 - gen8_ind7 - novel value     
9 - gen8_ind18 - novel value surprise CREATIVE 
10 - gen8_ind2 - novel value surprise CREATIVE 
11 - gen8_ind15 - novel value surprise CREATIVE 
12 - gen8_ind17 - novel value surprise CREATIVE 
13 - gen8_ind1 - novel value     
14 - gen8_ind16 - novel value surprise CREATIVE 
15 - gen8_ind6 - novel value surprise CREATIVE 
16 - gen8_ind14 - novel value surprise CREATIVE 
17 - gen8_ind9 - novel value     
18 - gen8_ind3 - novel value surprise CREATIVE 
19 - gen8_ind13 - novel value surprise CREATIVE 
RANKS GEN 8 BY VALUE SCORE
0 - gen8_ind3 - novel value surprise CREATIVE 
1 - gen8_ind9 - novel value     
2 - gen8_ind17 - novel value surprise CREATIVE 
3 - gen8_ind1 - novel value     
4 - gen8_ind13 - novel value surprise CREATIVE 
5 - gen8_ind14 - novel value surprise CREATIVE 
6 - gen8_ind6 - novel value surprise CREATIVE 
7 - gen8_ind16 - novel value surprise CREATIVE 
8 - gen8_ind8 - novel value surprise CREATIVE 
9 - gen8_ind7 - novel value     
10 - gen8_ind2 - novel value surprise CREATIVE 
11 - gen8_ind15 - novel value surprise CREATIVE 
12 - gen8_ind19 - novel value surprise CREATIVE 
13 - gen8_ind18 - novel value surprise CREATIVE 
14 - gen8_ind11 - novel value surprise CREATIVE 
15 - gen8_ind0 - novel value surprise CREATIVE 
16 - gen8_ind12 - novel value surprise CREATIVE 
17 - gen8_ind10 - novel value surprise CREATIVE 
18 - gen8_ind5 - novel value surprise CREATIVE 
19 - gen8_ind4 - novel value surprise CREATIVE 
RANKS GEN 9
0 - gen9_ind2 - novel value surprise CREATIVE 
1 - gen9_ind1 - novel value surprise CREATIVE 
2 - gen9_ind15 - novel value surprise CREATIVE 
3 - gen9_ind8 - novel value surprise CREATIVE 
4 - gen9_ind5 - novel value surprise CREATIVE 
5 - gen9_ind17 - novel value surprise CREATIVE 
6 - gen9_ind18 - novel   surprise   
7 - gen9_ind6 - novel value surprise CREATIVE 
8 - gen9_ind13 - novel value surprise CREATIVE 
9 - gen9_ind3 - novel value     
10 - gen9_ind12 - novel value surprise CREATIVE 
11 - gen9_ind10 - novel value     
12 - gen9_ind4 - novel value surprise CREATIVE 
13 - gen9_ind16 -   value surprise   
14 - gen9_ind7 - novel value surprise CREATIVE 
15 - gen9_ind0 -   value surprise   
16 - gen9_ind14 - novel value     
17 - gen9_ind9 - novel value surprise CREATIVE 
18 - gen9_ind11 - novel value     
19 - gen9_ind19 - novel value surprise CREATIVE 
RANKS GEN 9 BY VALUE SCORE
0 - gen9_ind7 - novel value surprise CREATIVE 
1 - gen9_ind11 - novel value     
2 - gen9_ind12 - novel value surprise CREATIVE 
3 - gen9_ind14 - novel value     
4 - gen9_ind19 - novel value surprise CREATIVE 
5 - gen9_ind10 - novel value     
6 - gen9_ind0 -   value surprise   
7 - gen9_ind9 - novel value surprise CREATIVE 
8 - gen9_ind16 -   value surprise   
9 - gen9_ind4 - novel value surprise CREATIVE 
10 - gen9_ind13 - novel value surprise CREATIVE 
11 - gen9_ind15 - novel value surprise CREATIVE 
12 - gen9_ind1 - novel value surprise CREATIVE 
13 - gen9_ind3 - novel value     
14 - gen9_ind8 - novel value surprise CREATIVE 
15 - gen9_ind17 - novel value surprise CREATIVE 
16 - gen9_ind2 - novel value surprise CREATIVE 
17 - gen9_ind5 - novel value surprise CREATIVE 
18 - gen9_ind18 - novel   surprise   
19 - gen9_ind6 - novel value surprise CREATIVE 
RANKS GEN 10
0 - gen10_ind10 - novel value     
1 - gen10_ind14 - novel value surprise CREATIVE 
2 - gen10_ind15 - novel value surprise CREATIVE 
3 - gen10_ind0 - novel value surprise CREATIVE 
4 - gen10_ind7 - novel value surprise CREATIVE 
5 - gen10_ind3 - novel value surprise CREATIVE 
6 - gen10_ind16 - novel value surprise CREATIVE 
7 - gen10_ind13 - novel value surprise CREATIVE 
8 - gen10_ind18 -   value     
9 - gen10_ind6 - novel value     
10 - gen10_ind19 - novel value surprise CREATIVE 
11 - gen10_ind12 - novel value surprise CREATIVE 
12 - gen10_ind2 - novel value surprise CREATIVE 
13 - gen10_ind9 - novel value surprise CREATIVE 
14 - gen10_ind1 - novel value surprise CREATIVE 
15 - gen10_ind17 - novel value     
16 - gen10_ind5 - novel value surprise CREATIVE 
17 - gen10_ind4 - novel value surprise CREATIVE 
18 - gen10_ind8 - novel value surprise CREATIVE 
19 - gen10_ind11 - novel value     
RANKS GEN 10 BY VALUE SCORE
0 - gen10_ind12 - novel value surprise CREATIVE 
1 - gen10_ind1 - novel value surprise CREATIVE 
2 - gen10_ind8 - novel value surprise CREATIVE 
3 - gen10_ind5 - novel value surprise CREATIVE 
4 - gen10_ind11 - novel value     
5 - gen10_ind13 - novel value surprise CREATIVE 
6 - gen10_ind10 - novel value     
7 - gen10_ind17 - novel value     
8 - gen10_ind9 - novel value surprise CREATIVE 
9 - gen10_ind18 -   value     
10 - gen10_ind7 - novel value surprise CREATIVE 
11 - gen10_ind2 - novel value surprise CREATIVE 
12 - gen10_ind19 - novel value surprise CREATIVE 
13 - gen10_ind4 - novel value surprise CREATIVE 
14 - gen10_ind16 - novel value surprise CREATIVE 
15 - gen10_ind6 - novel value     
16 - gen10_ind0 - novel value surprise CREATIVE 
17 - gen10_ind14 - novel value surprise CREATIVE 
18 - gen10_ind15 - novel value surprise CREATIVE 
19 - gen10_ind3 - novel value surprise CREATIVE 
FULL RANKS
0 - gen6_ind19 - novel value surprise CREATIVE 
1 - gen4_ind10 -   value surprise   
2 - well-by-the-winding-road-in-the-park-of-chateau-noir -     surprise   
3 - gen3_ind17 - novel value surprise CREATIVE 
4 - gen6_ind7 - novel value surprise CREATIVE 
5 - gen5_ind1 - novel value surprise CREATIVE 
6 - gen8_ind12 - novel value surprise CREATIVE 
7 - electrician-1915 -   value surprise   
8 - gen4_ind0 - novel value surprise CREATIVE 
9 - gen10_ind14 - novel value surprise CREATIVE 
10 - gen7_ind1 - novel value     
11 - gen7_ind5 - novel value     
12 - gen1_ind17 -   value surprise   
13 - gen7_ind12 - novel value surprise CREATIVE 
14 - gen8_ind5 - novel value surprise CREATIVE 
15 - gen6_ind10 - novel value surprise CREATIVE 
16 - gen7_ind3 - novel value surprise CREATIVE 
17 - coast-near-antibes -   value surprise   
18 - still-life-vase-with-fifteen-sunflowers-1888-1 -   value surprise   
19 - gen3_ind10 - novel value     
20 - gen6_ind0 - novel value     
21 - gen3_ind12 -   value     
22 - gen9_ind2 - novel value surprise CREATIVE 
23 - gen7_ind8 - novel value     
24 - gen9_ind1 - novel value surprise CREATIVE 
25 - gen9_ind15 - novel value surprise CREATIVE 
26 - gen6_ind5 - novel value     
27 - gen9_ind8 - novel value surprise CREATIVE 
28 - gen7_ind7 - novel value surprise CREATIVE 
29 - gen10_ind15 - novel value surprise CREATIVE 
30 - gen8_ind11 - novel value surprise CREATIVE 
31 - prisoners-exercising-prisoners-round-1890 -     surprise   
32 - gen10_ind7 - novel value surprise CREATIVE 
33 - gen1_ind18 - novel value surprise CREATIVE 
34 - gen9_ind5 - novel value surprise CREATIVE 
35 - gen6_ind18 - novel value surprise CREATIVE 
36 - gen6_ind13 - novel value surprise CREATIVE 
37 - the-starry-night -   value surprise   
38 - gen7_ind4 - novel value     
39 - gen6_ind17 - novel value     
40 - gen3_ind14 - novel value     
41 - gen3_ind3 - novel value surprise CREATIVE 
42 - gen6_ind15 - novel value     
43 - gen4_ind13 -   value     
44 - gen9_ind17 - novel value surprise CREATIVE 
45 - gen7_ind6 - novel value surprise CREATIVE 
46 - gen8_ind2 - novel value surprise CREATIVE 
47 - gen6_ind2 - novel value surprise CREATIVE 
48 - gen10_ind3 - novel value surprise CREATIVE 
49 - gen9_ind18 - novel   surprise   
50 - gen1_ind5 - novel   surprise   
51 - orfeu-nos-infernos-detail-1904 -   value surprise   
52 - gen8_ind15 - novel value surprise CREATIVE 
53 - gen7_ind19 - novel value surprise CREATIVE 
54 - gen5_ind11 - novel value surprise CREATIVE 
55 - gen6_ind11 - novel value surprise CREATIVE 
56 - the-harvest-1890 -   value surprise   
57 - gen2_ind1 - novel value     
58 - gen9_ind6 - novel value surprise CREATIVE 
59 - gen8_ind17 - novel value surprise CREATIVE 
60 - gen1_ind11 - novel value surprise CREATIVE 
61 - gen7_ind14 - novel value     
62 - the-oath-of-horatii-1784 -   value surprise   
63 - gen2_ind5 - novel value surprise CREATIVE 
64 - gen5_ind19 - novel value     
65 - gen2_ind15 - novel value surprise CREATIVE 
66 - gen6_ind12 - novel value surprise CREATIVE 
67 - gen8_ind16 - novel value surprise CREATIVE 
68 - gen8_ind4 - novel value surprise CREATIVE 
69 - gen4_ind19 - novel value surprise CREATIVE 
70 - gen5_ind6 - novel value surprise CREATIVE 
71 - gen1_ind7 - novel   surprise   
72 - gen2_ind0 - novel value surprise CREATIVE 
73 - gen1_ind8 - novel value surprise CREATIVE 
74 - gen7_ind11 - novel value surprise CREATIVE 
75 - gen8_ind3 - novel value surprise CREATIVE 
76 - gen5_ind3 - novel value     
77 - gen5_ind5 - novel value surprise CREATIVE 
78 - gen8_ind13 - novel value surprise CREATIVE 
79 - view-of-the-tiber-and-castel-st-angelo-1776 -   value surprise   
80 - gen7_ind18 - novel value     
81 - gen9_ind4 - novel value surprise CREATIVE 
82 - gen3_ind18 - novel value surprise CREATIVE 
83 - gen6_ind6 - novel value surprise CREATIVE 
84 - gen7_ind9 - novel value     
85 - gen6_ind14 - novel value     
86 - gen3_ind13 -   value     
87 - gen7_ind16 - novel value surprise CREATIVE 
88 - gen9_ind13 - novel value surprise CREATIVE 
89 - gen3_ind5 - novel value surprise CREATIVE 
90 - gen5_ind9 -   value     
91 - gen4_ind1 -   value surprise   
92 - gen4_ind2 -   value surprise   
93 - gen8_ind19 - novel value surprise CREATIVE 
94 - gen10_ind16 - novel value surprise CREATIVE 
95 - gen10_ind18 -   value     
96 - gen3_ind7 - novel value     
97 - gen10_ind0 - novel value surprise CREATIVE 
98 - gen8_ind1 - novel value     
99 - gen3_ind6 - novel value surprise CREATIVE 
100 - gen9_ind10 - novel value     
101 - gen1_ind12 - novel value surprise CREATIVE 
102 - gen3_ind16 -   value     
103 - gen3_ind9 -   value surprise   
104 - gen8_ind7 - novel value     
105 - gen8_ind0 - novel value surprise CREATIVE 
106 - gen3_ind2 -   value surprise   
107 - gen10_ind6 - novel value     
108 - gen4_ind9 - novel value surprise CREATIVE 
109 - gen2_ind17 - novel value surprise CREATIVE 
110 - gen5_ind17 - novel value surprise CREATIVE 
111 - american-500-sunset -   value surprise   
112 - gen1_ind14 - novel value surprise CREATIVE 
113 - gen7_ind0 - novel value     
114 - gen9_ind3 - novel value     
115 - gen1_ind16 - novel value surprise CREATIVE 
116 - gen10_ind10 - novel value     
117 - gen9_ind12 - novel value surprise CREATIVE 
118 - gen1_ind10 - novel value surprise CREATIVE 
119 - gen8_ind6 - novel value surprise CREATIVE 
120 - gen1_ind0 -   value surprise   
121 - gen1_ind4 -   value surprise   
122 - woman-with-a-flower-1891 -   value surprise   
123 - gen10_ind19 - novel value surprise CREATIVE 
124 - napoleoncrossing-the-alps-at-the-st-bernard-pass-20th-may-1800-1801 -   value surprise   
125 - gen4_ind12 - novel value surprise CREATIVE 
126 - gen2_ind18 - novel value surprise CREATIVE 
127 - gen1_ind2 - novel value surprise CREATIVE 
128 - gen10_ind2 - novel value surprise CREATIVE 
129 - gen6_ind4 - novel value surprise CREATIVE 
130 - the-magpie-1869 -   value surprise   
131 - gen5_ind4 - novel value surprise CREATIVE 
132 - gen10_ind13 - novel value surprise CREATIVE 
133 - gen7_ind15 - novel value     
134 - gen5_ind2 - novel value surprise CREATIVE 
135 - gen3_ind0 -   value     
136 - gen2_ind16 - novel value surprise CREATIVE 
137 - marguerite-1975 -   value surprise   
138 - costume-design-for-salome-1917 -   value surprise   
139 - gen2_ind11 -   value     
140 - gen5_ind18 - novel value     
141 - self-portrait-da-vinci -   value surprise   
142 - gen5_ind7 - novel value surprise CREATIVE 
143 - gen4_ind11 - novel value surprise CREATIVE 
144 - gen1_ind6 - novel value surprise CREATIVE 
145 - gen7_ind17 - novel value surprise CREATIVE 
146 - gen7_ind13 - novel value     
147 - vllandand-sea1948 -   value surprise   
148 - the-tennis-court-oath-20th-june-1789-1791 -   value surprise   
149 - george-caleb-bingham-boatmen-on-the-missouri-google-art-project -   value surprise   
150 - seaweed-gatherers-at-omari -   value surprise   
151 - forest -   value surprise   
152 - rooftops-venice-1983 -   value surprise   
153 - water-lilies-1917 -   value surprise   
154 - gen2_ind4 - novel value surprise CREATIVE 
155 - the-great-wave-off-kanagawa -   value surprise   
156 - gen8_ind10 - novel value surprise CREATIVE 
157 - gen3_ind4 - novel value surprise CREATIVE 
158 - gen2_ind19 - novel value surprise CREATIVE 
159 - gen4_ind18 - novel value surprise CREATIVE 
160 - still-life-with-three-skulls -   value surprise   
161 - the-port-of-le-havre-1903 -   value surprise   
162 - linhas-de-sombra -   value surprise   
163 - dark-bridge-2003 -   value surprise   
164 - double-twist-1980 -     surprise   
165 - gen4_ind7 - novel value surprise CREATIVE 
166 - gen5_ind10 - novel value surprise CREATIVE 
167 - gen5_ind8 - novel value     
168 - gen5_ind16 - novel value surprise CREATIVE 
169 - gen5_ind14 - novel value surprise CREATIVE 
170 - gen4_ind3 - novel value surprise CREATIVE 
171 - gen5_ind13 - novel   surprise   
172 - gen1_ind1 - novel value surprise CREATIVE 
173 - william-henry-hunt-the-winter-dining-room-of-the-earl-of-essex-at-cassiobury-1821-meisterdrucke -   value surprise   
174 - gen5_ind15 - novel value     
175 - gen4_ind14 - novel value surprise CREATIVE 
176 - voix-du-jura-n-3296-2008 -     surprise   
177 - gen5_ind12 - novel value surprise CREATIVE 
178 - gen2_ind8 - novel value     
179 - buddha-moon-monk-sunrise-1999 -   value surprise   
180 - gen9_ind16 -   value surprise   
181 - gen2_ind14 -   value surprise   
182 - gen4_ind5 - novel value surprise CREATIVE 
183 - lot-landscape-1890 -   value surprise   
184 - marilyn-1 -   value surprise   
185 - gen1_ind15 - novel value surprise CREATIVE 
186 - gen10_ind12 - novel value surprise CREATIVE 
187 - gen8_ind8 - novel value surprise CREATIVE 
188 - gen1_ind13 - novel value surprise CREATIVE 
189 - landscape-under-snow-upper-norwood-1871 -   value surprise   
190 - gen6_ind9 - novel value surprise CREATIVE 
191 - gen4_ind15 - novel value surprise CREATIVE 
192 - gen3_ind19 - novel value surprise CREATIVE 
193 - gen1_ind19 - novel value surprise CREATIVE 
194 - gen7_ind2 - novel value     
195 - gen2_ind6 - novel value surprise CREATIVE 
196 - gen2_ind12 - novel value surprise CREATIVE 
197 - gen10_ind9 - novel value surprise CREATIVE 
198 - gen5_ind0 - novel value surprise CREATIVE 
199 - gen2_ind7 - novel value surprise CREATIVE 
200 - gen3_ind11 - novel value surprise CREATIVE 
201 - gen9_ind7 - novel value surprise CREATIVE 
202 - gen6_ind3 - novel value surprise CREATIVE 
203 - gen6_ind8 - novel value surprise CREATIVE 
204 - gen4_ind17 - novel value surprise CREATIVE 
205 - gen3_ind8 -   value surprise   
206 - gen1_ind9 - novel value surprise CREATIVE 
207 - randegg-in-the-snow-with-ravens -   value surprise   
208 - gen8_ind18 - novel value surprise CREATIVE 
209 - gen4_ind4 -   value surprise   
210 - gen4_ind16 - novel value surprise CREATIVE 
211 - gen9_ind0 -   value surprise   
212 - gen10_ind1 - novel value surprise CREATIVE 
213 - gen9_ind14 - novel value     
214 - still-life-with-goblet -     surprise   
215 - gen2_ind3 - novel value surprise CREATIVE 
216 - boat-at-low-tide-at-fecamp -   value surprise   
217 - view-of-amalfi-1844 -   value surprise   
218 - gen10_ind17 - novel value     
219 - gen1_ind3 -   value surprise   
220 - gen9_ind9 - novel value surprise CREATIVE 
221 - gen2_ind10 - novel value surprise CREATIVE 
222 - costume-design-for-dance-of-the-seven-veils-1917 -     surprise   
223 - gen2_ind13 - novel value     
224 - cash-register-1917 -   value surprise   
225 - gen4_ind8 - novel value surprise CREATIVE 
226 - gen6_ind1 - novel value surprise CREATIVE 
227 - gen7_ind10 - novel value     
228 - gen2_ind9 - novel value surprise CREATIVE 
229 - gen2_ind2 - novel value     
230 - gen10_ind5 - novel value surprise CREATIVE 
231 - peasant-woman-with-buckets-1913 -   value surprise   
232 - gen10_ind4 - novel value surprise CREATIVE 
233 - plate-77-belted-kingfisher -   value surprise   
234 - gen4_ind6 - novel value surprise CREATIVE 
235 - gen6_ind16 - novel value surprise CREATIVE 
236 - via-san-leonardo -   value surprise   
237 - gen9_ind11 - novel value     
238 - jacob-s-ladder -   value surprise   
239 - gen8_ind14 - novel value surprise CREATIVE 
240 - gen10_ind8 - novel value surprise CREATIVE 
241 - gen10_ind11 - novel value     
242 - self-portrait-with-easel -   value surprise   
243 - gen9_ind19 - novel value surprise CREATIVE 
244 - gen8_ind9 - novel value     
245 - the-jetty-at-le-havre-bad-weather-1870 -   value surprise   
246 - gen3_ind1 -   value surprise   
247 - bouquet-of-peonies-on-a-musical-score-1876 -   value surprise   
248 - gen3_ind15 - novel value surprise CREATIVE 
249 - mona-lisa-c-1503-1519 -   value surprise   
RANKS BY VALUE SCORE
0 - costume-design-for-dance-of-the-seven-veils-1917 -     surprise   
1 - lot-landscape-1890 -   value surprise   
2 - gen1_ind1 - novel value surprise CREATIVE 
3 - gen1_ind19 - novel value surprise CREATIVE 
4 - william-henry-hunt-the-winter-dining-room-of-the-earl-of-essex-at-cassiobury-1821-meisterdrucke -   value surprise   
5 - gen4_ind5 - novel value surprise CREATIVE 
6 - mona-lisa-c-1503-1519 -   value surprise   
7 - via-san-leonardo -   value surprise   
8 - peasant-woman-with-buckets-1913 -   value surprise   
9 - gen2_ind2 - novel value     
10 - gen1_ind15 - novel value surprise CREATIVE 
11 - plate-77-belted-kingfisher -   value surprise   
12 - still-life-with-three-skulls -   value surprise   
13 - george-caleb-bingham-boatmen-on-the-missouri-google-art-project -   value surprise   
14 - marilyn-1 -   value surprise   
15 - dark-bridge-2003 -   value surprise   
16 - gen3_ind1 -   value surprise   
17 - buddha-moon-monk-sunrise-1999 -   value surprise   
18 - costume-design-for-salome-1917 -   value surprise   
19 - gen5_ind7 - novel value surprise CREATIVE 
20 - gen2_ind7 - novel value surprise CREATIVE 
21 - the-great-wave-off-kanagawa -   value surprise   
22 - gen5_ind2 - novel value surprise CREATIVE 
23 - gen2_ind11 -   value     
24 - gen3_ind8 -   value surprise   
25 - gen3_ind0 -   value     
26 - gen10_ind12 - novel value surprise CREATIVE 
27 - marguerite-1975 -   value surprise   
28 - gen10_ind1 - novel value surprise CREATIVE 
29 - gen10_ind8 - novel value surprise CREATIVE 
30 - self-portrait-da-vinci -   value surprise   
31 - gen1_ind4 -   value surprise   
32 - gen1_ind0 -   value surprise   
33 - gen1_ind3 -   value surprise   
34 - gen5_ind18 - novel value     
35 - gen9_ind7 - novel value surprise CREATIVE 
36 - gen2_ind14 -   value surprise   
37 - bouquet-of-peonies-on-a-musical-score-1876 -   value surprise   
38 - gen10_ind5 - novel value surprise CREATIVE 
39 - rooftops-venice-1983 -   value surprise   
40 - gen10_ind11 - novel value     
41 - gen5_ind17 - novel value surprise CREATIVE 
42 - gen3_ind9 -   value surprise   
43 - gen7_ind2 - novel value     
44 - cash-register-1917 -   value surprise   
45 - gen1_ind2 - novel value surprise CREATIVE 
46 - gen9_ind11 - novel value     
47 - vllandand-sea1948 -   value surprise   
48 - gen3_ind2 -   value surprise   
49 - american-500-sunset -   value surprise   
50 - the-jetty-at-le-havre-bad-weather-1870 -   value surprise   
51 - self-portrait-with-easel -   value surprise   
52 - gen4_ind11 - novel value surprise CREATIVE 
53 - gen10_ind13 - novel value surprise CREATIVE 
54 - gen9_ind12 - novel value surprise CREATIVE 
55 - gen3_ind16 -   value     
56 - gen7_ind0 - novel value     
57 - seaweed-gatherers-at-omari -   value surprise   
58 - gen4_ind2 -   value surprise   
59 - double-twist-1980 -     surprise   
60 - gen9_ind14 - novel value     
61 - gen2_ind16 - novel value surprise CREATIVE 
62 - woman-with-a-flower-1891 -   value surprise   
63 - gen10_ind10 - novel value     
64 - gen6_ind16 - novel value surprise CREATIVE 
65 - gen2_ind17 - novel value surprise CREATIVE 
66 - gen5_ind9 -   value     
67 - gen8_ind3 - novel value surprise CREATIVE 
68 - gen9_ind19 - novel value surprise CREATIVE 
69 - gen9_ind10 - novel value     
70 - gen8_ind9 - novel value     
71 - gen10_ind17 - novel value     
72 - gen7_ind11 - novel value surprise CREATIVE 
73 - gen9_ind0 -   value surprise   
74 - gen4_ind1 -   value surprise   
75 - gen8_ind17 - novel value surprise CREATIVE 
76 - the-oath-of-horatii-1784 -   value surprise   
77 - view-of-amalfi-1844 -   value surprise   
78 - gen8_ind1 - novel value     
79 - jacob-s-ladder -   value surprise   
80 - gen9_ind9 - novel value surprise CREATIVE 
81 - gen9_ind16 -   value surprise   
82 - electrician-1915 -   value surprise   
83 - gen10_ind9 - novel value surprise CREATIVE 
84 - gen10_ind18 -   value     
85 - gen3_ind12 -   value     
86 - gen4_ind12 - novel value surprise CREATIVE 
87 - gen7_ind3 - novel value surprise CREATIVE 
88 - randegg-in-the-snow-with-ravens -   value surprise   
89 - gen4_ind4 -   value surprise   
90 - still-life-with-goblet -     surprise   
91 - napoleoncrossing-the-alps-at-the-st-bernard-pass-20th-may-1800-1801 -   value surprise   
92 - gen6_ind14 - novel value     
93 - gen4_ind10 -   value surprise   
94 - gen4_ind13 -   value     
95 - gen1_ind17 -   value surprise   
96 - gen2_ind19 - novel value surprise CREATIVE 
97 - gen8_ind13 - novel value surprise CREATIVE 
98 - gen8_ind14 - novel value surprise CREATIVE 
99 - gen2_ind10 - novel value surprise CREATIVE 
100 - gen4_ind16 - novel value surprise CREATIVE 
101 - gen1_ind9 - novel value surprise CREATIVE 
102 - gen7_ind5 - novel value     
103 - gen3_ind13 -   value     
104 - voix-du-jura-n-3296-2008 -     surprise   
105 - gen9_ind4 - novel value surprise CREATIVE 
106 - gen7_ind9 - novel value     
107 - gen7_ind6 - novel value surprise CREATIVE 
108 - gen6_ind17 - novel value     
109 - gen6_ind4 - novel value surprise CREATIVE 
110 - gen10_ind7 - novel value surprise CREATIVE 
111 - gen8_ind6 - novel value surprise CREATIVE 
112 - gen10_ind2 - novel value surprise CREATIVE 
113 - gen10_ind19 - novel value surprise CREATIVE 
114 - the-magpie-1869 -   value surprise   
115 - gen10_ind4 - novel value surprise CREATIVE 
116 - gen10_ind16 - novel value surprise CREATIVE 
117 - gen3_ind7 - novel value     
118 - gen6_ind2 - novel value surprise CREATIVE 
119 - gen7_ind10 - novel value     
120 - gen7_ind16 - novel value surprise CREATIVE 
121 - gen3_ind18 - novel value surprise CREATIVE 
122 - gen1_ind12 - novel value surprise CREATIVE 
123 - gen6_ind6 - novel value surprise CREATIVE 
124 - gen8_ind16 - novel value surprise CREATIVE 
125 - gen8_ind8 - novel value surprise CREATIVE 
126 - gen3_ind14 - novel value     
127 - gen9_ind13 - novel value surprise CREATIVE 
128 - gen1_ind18 - novel value surprise CREATIVE 
129 - gen7_ind18 - novel value     
130 - gen8_ind7 - novel value     
131 - gen8_ind2 - novel value surprise CREATIVE 
132 - gen8_ind15 - novel value surprise CREATIVE 
133 - gen8_ind19 - novel value surprise CREATIVE 
134 - view-of-the-tiber-and-castel-st-angelo-1776 -   value surprise   
135 - gen7_ind17 - novel value surprise CREATIVE 
136 - gen6_ind0 - novel value     
137 - gen8_ind18 - novel value surprise CREATIVE 
138 - gen1_ind14 - novel value surprise CREATIVE 
139 - gen2_ind12 - novel value surprise CREATIVE 
140 - gen3_ind11 - novel value surprise CREATIVE 
141 - gen3_ind5 - novel value surprise CREATIVE 
142 - gen10_ind6 - novel value     
143 - gen8_ind11 - novel value surprise CREATIVE 
144 - gen10_ind0 - novel value surprise CREATIVE 
145 - gen6_ind1 - novel value surprise CREATIVE 
146 - gen9_ind15 - novel value surprise CREATIVE 
147 - gen4_ind9 - novel value surprise CREATIVE 
148 - gen2_ind8 - novel value     
149 - gen2_ind13 - novel value     
150 - gen6_ind15 - novel value     
151 - gen7_ind19 - novel value surprise CREATIVE 
152 - gen3_ind6 - novel value surprise CREATIVE 
153 - still-life-vase-with-fifteen-sunflowers-1888-1 -   value surprise   
154 - gen1_ind16 - novel value surprise CREATIVE 
155 - landscape-under-snow-upper-norwood-1871 -   value surprise   
156 - gen8_ind0 - novel value surprise CREATIVE 
157 - gen2_ind1 - novel value     
158 - gen5_ind11 - novel value surprise CREATIVE 
159 - gen6_ind18 - novel value surprise CREATIVE 
160 - gen7_ind14 - novel value     
161 - gen1_ind10 - novel value surprise CREATIVE 
162 - gen4_ind0 - novel value surprise CREATIVE 
163 - gen9_ind1 - novel value surprise CREATIVE 
164 - gen3_ind15 - novel value surprise CREATIVE 
165 - gen5_ind4 - novel value surprise CREATIVE 
166 - gen9_ind3 - novel value     
167 - gen9_ind8 - novel value surprise CREATIVE 
168 - gen6_ind3 - novel value surprise CREATIVE 
169 - gen8_ind12 - novel value surprise CREATIVE 
170 - gen9_ind17 - novel value surprise CREATIVE 
171 - gen9_ind2 - novel value surprise CREATIVE 
172 - gen5_ind5 - novel value surprise CREATIVE 
173 - gen6_ind19 - novel value surprise CREATIVE 
174 - gen1_ind6 - novel value surprise CREATIVE 
175 - gen4_ind14 - novel value surprise CREATIVE 
176 - boat-at-low-tide-at-fecamp -   value surprise   
177 - the-starry-night -   value surprise   
178 - gen6_ind11 - novel value surprise CREATIVE 
179 - gen8_ind10 - novel value surprise CREATIVE 
180 - gen7_ind15 - novel value     
181 - gen2_ind18 - novel value surprise CREATIVE 
182 - gen7_ind13 - novel value     
183 - forest -   value surprise   
184 - gen6_ind9 - novel value surprise CREATIVE 
185 - gen3_ind17 - novel value surprise CREATIVE 
186 - gen10_ind14 - novel value surprise CREATIVE 
187 - gen3_ind3 - novel value surprise CREATIVE 
188 - gen2_ind3 - novel value surprise CREATIVE 
189 - the-tennis-court-oath-20th-june-1789-1791 -   value surprise   
190 - orfeu-nos-infernos-detail-1904 -   value surprise   
191 - gen3_ind4 - novel value surprise CREATIVE 
192 - gen2_ind9 - novel value surprise CREATIVE 
193 - gen10_ind15 - novel value surprise CREATIVE 
194 - gen4_ind17 - novel value surprise CREATIVE 
195 - water-lilies-1917 -   value surprise   
196 - gen8_ind5 - novel value surprise CREATIVE 
197 - gen2_ind6 - novel value surprise CREATIVE 
198 - the-port-of-le-havre-1903 -   value surprise   
199 - gen2_ind4 - novel value surprise CREATIVE 
200 - gen3_ind19 - novel value surprise CREATIVE 
201 - gen4_ind19 - novel value surprise CREATIVE 
202 - gen6_ind7 - novel value surprise CREATIVE 
203 - gen2_ind5 - novel value surprise CREATIVE 
204 - gen5_ind1 - novel value surprise CREATIVE 
205 - gen6_ind8 - novel value surprise CREATIVE 
206 - gen7_ind4 - novel value     
207 - gen6_ind5 - novel value     
208 - gen7_ind7 - novel value surprise CREATIVE 
209 - gen7_ind8 - novel value     
210 - gen5_ind0 - novel value surprise CREATIVE 
211 - gen1_ind8 - novel value surprise CREATIVE 
212 - gen4_ind18 - novel value surprise CREATIVE 
213 - the-harvest-1890 -   value surprise   
214 - gen8_ind4 - novel value surprise CREATIVE 
215 - gen1_ind13 - novel value surprise CREATIVE 
216 - gen4_ind15 - novel value surprise CREATIVE 
217 - gen5_ind19 - novel value     
218 - gen3_ind10 - novel value     
219 - well-by-the-winding-road-in-the-park-of-chateau-noir -     surprise   
220 - linhas-de-sombra -   value surprise   
221 - gen9_ind5 - novel value surprise CREATIVE 
222 - gen4_ind7 - novel value surprise CREATIVE 
223 - gen5_ind10 - novel value surprise CREATIVE 
224 - gen6_ind12 - novel value surprise CREATIVE 
225 - gen6_ind13 - novel value surprise CREATIVE 
226 - gen7_ind12 - novel value surprise CREATIVE 
227 - gen5_ind6 - novel value surprise CREATIVE 
228 - gen10_ind3 - novel value surprise CREATIVE 
229 - gen7_ind1 - novel value     
230 - gen5_ind16 - novel value surprise CREATIVE 
231 - gen5_ind8 - novel value     
232 - gen4_ind8 - novel value surprise CREATIVE 
233 - gen1_ind11 - novel value surprise CREATIVE 
234 - gen5_ind15 - novel value     
235 - gen6_ind10 - novel value surprise CREATIVE 
236 - gen4_ind6 - novel value surprise CREATIVE 
237 - gen5_ind3 - novel value     
238 - gen2_ind0 - novel value surprise CREATIVE 
239 - gen9_ind18 - novel   surprise   
240 - gen2_ind15 - novel value surprise CREATIVE 
241 - gen5_ind12 - novel value surprise CREATIVE 
242 - gen9_ind6 - novel value surprise CREATIVE 
243 - gen5_ind14 - novel value surprise CREATIVE 
244 - gen4_ind3 - novel value surprise CREATIVE 
245 - gen1_ind5 - novel   surprise   
246 - coast-near-antibes -   value surprise   
247 - gen5_ind13 - novel   surprise   
248 - prisoners-exercising-prisoners-round-1890 -     surprise   
249 - gen1_ind7 - novel   surprise 


CORRELATIONS
CORRELATION ColorDistributionMeasure X ComplexityMeasure
	PEARSONR 0.15137 0.01661
	SPEARMAM 0.15138 0.0166
	KENDALLT 0.08723 0.03993

CORRELATION ColorDistributionMeasure X LivelinessMeasure
	PEARSONR -0.3937 0.0
	SPEARMAM -0.3733 0.0
	KENDALLT -0.26279 0.0

CORRELATION ColorDistributionMeasure X LuminanceDistributionMeasure
	PEARSONR 0.01105 0.86196
	SPEARMAM 0.09631 0.12882
	KENDALLT 0.06715 0.11377

CORRELATION ColorDistributionMeasure X LuminanceRedundancyMeasure
	PEARSONR -0.35276 0.0
	SPEARMAM -0.295 0.0
	KENDALLT -0.21443 0.0

CORRELATION ColorDistributionMeasure X value_score
	PEARSONR 0.91398 0.0
	SPEARMAM 0.91864 0.0
	KENDALLT 0.77234 0.0

CORRELATION ColorDistributionMeasure X novelty_score
	PEARSONR -0.55554 0.0
	SPEARMAM -0.55385 0.0
	KENDALLT -0.4344 0.0

CORRELATION ColorDistributionMeasure X surprise_score
	PEARSONR 0.07179 0.25809
	SPEARMAM 0.04077 0.52109
	KENDALLT 0.02352 0.61399

CORRELATION ComplexityMeasure X LivelinessMeasure
	PEARSONR -0.34258 0.0
	SPEARMAM -0.3934 0.0
	KENDALLT -0.23773 0.0

CORRELATION ComplexityMeasure X LuminanceDistributionMeasure
	PEARSONR 0.07489 0.23807
	SPEARMAM 0.13327 0.0352
	KENDALLT 0.08919 0.03568

CORRELATION ComplexityMeasure X LuminanceRedundancyMeasure
	PEARSONR -0.39866 0.0
	SPEARMAM -0.41197 0.0
	KENDALLT -0.27743 0.0

CORRELATION ComplexityMeasure X value_score
	PEARSONR 0.31304 0.0
	SPEARMAM 0.28415 0.0
	KENDALLT 0.17815 3e-05

CORRELATION ComplexityMeasure X novelty_score
	PEARSONR -0.40181 0.0
	SPEARMAM -0.3831 0.0
	KENDALLT -0.22487 0.0

CORRELATION ComplexityMeasure X surprise_score
	PEARSONR 0.43955 0.0
	SPEARMAM 0.36445 0.0
	KENDALLT 0.26558 0.0

CORRELATION LivelinessMeasure X LuminanceDistributionMeasure
	PEARSONR 0.19066 0.00247
	SPEARMAM -0.15547 0.01386
	KENDALLT -0.10912 0.01018

CORRELATION LivelinessMeasure X LuminanceRedundancyMeasure
	PEARSONR 0.64943 0.0
	SPEARMAM 0.74266 0.0
	KENDALLT 0.57114 0.0

CORRELATION LivelinessMeasure X value_score
	PEARSONR -0.26714 2e-05
	SPEARMAM -0.3961 0.0
	KENDALLT -0.27808 0.0

CORRELATION LivelinessMeasure X novelty_score
	PEARSONR 0.47342 0.0
	SPEARMAM 0.49471 0.0
	KENDALLT 0.35069 0.0

CORRELATION LivelinessMeasure X surprise_score
	PEARSONR -0.42354 0.0
	SPEARMAM -0.34037 0.0
	KENDALLT -0.24945 0.0

CORRELATION LuminanceDistributionMeasure X LuminanceRedundancyMeasure
	PEARSONR 0.21381 0.00067
	SPEARMAM 0.13134 0.03796
	KENDALLT 0.0965 0.03249

CORRELATION LuminanceDistributionMeasure X value_score
	PEARSONR 0.36505 0.0
	SPEARMAM 0.37122 0.0
	KENDALLT 0.2627 0.0

CORRELATION LuminanceDistributionMeasure X novelty_score
	PEARSONR -0.031 0.62566
	SPEARMAM -0.09502 0.13406
	KENDALLT -0.07097 0.09997

CORRELATION LuminanceDistributionMeasure X surprise_score
	PEARSONR -0.044 0.48857
	SPEARMAM -0.05634 0.37506
	KENDALLT -0.04292 0.35748

CORRELATION LuminanceRedundancyMeasure X value_score
	PEARSONR -0.25354 5e-05
	SPEARMAM -0.25725 4e-05
	KENDALLT -0.18353 5e-05

CORRELATION LuminanceRedundancyMeasure X novelty_score
	PEARSONR 0.52093 0.0
	SPEARMAM 0.45776 0.0
	KENDALLT 0.34108 0.0

CORRELATION LuminanceRedundancyMeasure X surprise_score
	PEARSONR -0.47326 0.0
	SPEARMAM -0.35294 0.0
	KENDALLT -0.27868 0.0

CORRELATION value_score X novelty_score
	PEARSONR -0.55213 0.0
	SPEARMAM -0.60982 0.0
	KENDALLT -0.48062 0.0

CORRELATION value_score X surprise_score
	PEARSONR 0.09796 0.1224
	SPEARMAM 0.02691 0.67198
	KENDALLT 0.00866 0.85264

CORRELATION novelty_score X surprise_score
	PEARSONR -0.60107 0.0
	SPEARMAM -0.35772 0.0
	KENDALLT -0.24167 0.0
