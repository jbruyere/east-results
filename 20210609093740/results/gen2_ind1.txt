IMAGE
name=gen2_ind1
content=dataset/output_images/gen1_ind2.jpg
style=dataset/output_images/gen1_ind11.jpg

SCORES
rank=1
ColorDistributionMeasure=0.421062
ComplexityMeasure=0.942724
LivelinessMeasure=0.967839
LuminanceDistributionMeasure=0.789107
LuminanceRedundancyMeasure=0.988361

MUTATIONS
mutation_1=[content_weight=750,style_weight=0.15]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.29962995360345585
novelty_score=0.262998709683918
surprise_score=0.4
