IMAGE
name=gen3_ind8
content=dataset/output_images/gen2_ind9.jpg
style=dataset/output_images/gen2_ind11.jpg

SCORES
rank=1
ColorDistributionMeasure=0.545342
ComplexityMeasure=0.857486
LivelinessMeasure=0.974658
LuminanceDistributionMeasure=0.800286
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[content_weight=750,style_weight=0.005]
mutation_2=[color_filter=cool]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.36449093844134156
novelty_score=0.19144988236493446
surprise_score=0.2
