IMAGE
name=gen2_ind3
content=dataset/output_images/gen1_ind7.jpg
style=dataset/output_images/gen1_ind2.jpg

SCORES
rank=1
ColorDistributionMeasure=0.452552
ComplexityMeasure=0.863917
LivelinessMeasure=0.973775
LuminanceDistributionMeasure=0.797972
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[color_filter=warm]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.3037993091117319
novelty_score=0.2670720355102111
surprise_score=0.8
