IMAGE
name=gen4_ind9
content=dataset/output_images/gen3_ind6.jpg
style=dataset/output_images/gen3_ind4.jpg

SCORES
rank=1
ColorDistributionMeasure=0.61023
ComplexityMeasure=0.861416
LivelinessMeasure=0.97521
LuminanceDistributionMeasure=0.763291
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/30007.jpg]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.39128642065014896
novelty_score=0.17356850516797664
surprise_score=0.4
