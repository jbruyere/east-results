IMAGE
name=gen3_ind11
content=dataset/output_images/gen2_ind10.jpg
style=dataset/output_images/gen2_ind9.jpg

SCORES
rank=2
ColorDistributionMeasure=0.300366
ComplexityMeasure=0.900927
LivelinessMeasure=0.972495
LuminanceDistributionMeasure=0.760937
LuminanceRedundancyMeasure=0.996443

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.19953951540158765
novelty_score=0.327277177014085
surprise_score=0.2
