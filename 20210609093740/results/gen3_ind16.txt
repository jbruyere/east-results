IMAGE
name=gen3_ind16
content=dataset/output_images/gen2_ind14.jpg
style=dataset/output_images/gen2_ind4.jpg

SCORES
rank=2
ColorDistributionMeasure=0.432092
ComplexityMeasure=0.850014
LivelinessMeasure=0.971388
LuminanceDistributionMeasure=0.784664
LuminanceRedundancyMeasure=0.989858

MUTATIONS
mutation_1=[content_weight=750,style_weight=0.2]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.27710965887044464
novelty_score=0.23862797271391686
surprise_score=0.4
