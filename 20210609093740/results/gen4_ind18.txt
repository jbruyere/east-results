IMAGE
name=gen4_ind18
content=dataset/output_images/gen3_ind8.jpg
style=dataset/output_images/gen3_ind12.jpg

SCORES
rank=1
ColorDistributionMeasure=0.555756
ComplexityMeasure=0.850854
LivelinessMeasure=0.973958
LuminanceDistributionMeasure=0.768801
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/6560.jpg]

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.35382348316955187
novelty_score=0.17598392426317222
surprise_score=0.0
