IMAGE
name=gen4_ind5
content=dataset/output_images/gen3_ind1.jpg
style=dataset/output_images/gen3_ind4.jpg

SCORES
rank=2
ColorDistributionMeasure=0.666238
ComplexityMeasure=0.881825
LivelinessMeasure=0.975455
LuminanceDistributionMeasure=0.676961
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[color_filter=bright]
mutation_2=[style_transfer=dataset/tmp_images/dataset/tmp_images/58307.jpg]
mutation_3=[content_weight=1500,style_weight=0.15]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.38768230178127855
novelty_score=0.21217953571299444
surprise_score=0.2
