IMAGE
name=gen4_ind6
content=dataset/output_images/gen3_ind0.jpg
style=dataset/output_images/gen3_ind4.jpg

SCORES
rank=1
ColorDistributionMeasure=0.718589
ComplexityMeasure=0.879379
LivelinessMeasure=0.974943
LuminanceDistributionMeasure=0.758461
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.46727132962106527
novelty_score=0.1941558680522061
surprise_score=0.2
