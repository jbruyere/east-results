IMAGE
name=gen3_ind14
content=dataset/output_images/gen2_ind9.jpg
style=dataset/output_images/gen2_ind4.jpg

SCORES
rank=2
ColorDistributionMeasure=0.505543
ComplexityMeasure=0.854745
LivelinessMeasure=0.967664
LuminanceDistributionMeasure=0.826552
LuminanceRedundancyMeasure=0.988361

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.34158991151666074
novelty_score=0.20274955802732522
surprise_score=0.2
