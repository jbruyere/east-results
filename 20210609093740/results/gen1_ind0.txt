IMAGE
name=gen1_ind0
content=dataset/input_images/the-tennis-court-oath-20th-june-1789-1791.jpg
style=dataset/input_images/cash-register-1917.jpg

SCORES
rank=1
ColorDistributionMeasure=0.56423
ComplexityMeasure=0.910557
LivelinessMeasure=0.973926
LuminanceDistributionMeasure=0.771587
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.3860772161096533
novelty_score=0.21018491365453762
surprise_score=0.6000000000000001
