IMAGE
name=gen2_ind10
content=dataset/output_images/gen1_ind14.jpg
style=dataset/output_images/gen1_ind2.jpg

SCORES
rank=1
ColorDistributionMeasure=0.277938
ComplexityMeasure=0.851062
LivelinessMeasure=0.967947
LuminanceDistributionMeasure=0.87103
LuminanceRedundancyMeasure=0.998586

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.19914953290274295
novelty_score=0.4202939345427118
surprise_score=0.4
