IMAGE
name=gen4_ind15
content=dataset/output_images/gen3_ind8.jpg
style=dataset/output_images/gen3_ind1.jpg

SCORES
rank=1
ColorDistributionMeasure=0.392204
ComplexityMeasure=0.911098
LivelinessMeasure=0.975786
LuminanceDistributionMeasure=0.792764
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[content_weight=750,style_weight=0.005]
mutation_2=[color_filter=bright]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.2764239159089843
novelty_score=0.24251999982420167
surprise_score=0.4
