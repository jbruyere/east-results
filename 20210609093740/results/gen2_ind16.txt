IMAGE
name=gen2_ind16
content=dataset/output_images/gen1_ind14.jpg
style=dataset/output_images/gen1_ind0.jpg

SCORES
rank=1
ColorDistributionMeasure=0.364072
ComplexityMeasure=0.931875
LivelinessMeasure=0.9744
LuminanceDistributionMeasure=0.762186
LuminanceRedundancyMeasure=0.998586

MUTATIONS
mutation_1=[color_filter=warm]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.25161043928258436
novelty_score=0.3252130780051815
surprise_score=0.6000000000000001
