IMAGE
name=gen2_ind6
content=dataset/output_images/gen1_ind0.jpg
style=dataset/output_images/gen1_ind17.jpg

SCORES
rank=2
ColorDistributionMeasure=0.373831
ComplexityMeasure=0.93058
LivelinessMeasure=0.964846
LuminanceDistributionMeasure=0.74328
LuminanceRedundancyMeasure=0.984565

MUTATIONS
mutation_1=[content_weight=1000,style_weight=0.25]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.24563139111183757
novelty_score=0.32077545085746856
surprise_score=0.4
