IMAGE
name=gen2_ind13
content=dataset/output_images/gen1_ind19.jpg
style=dataset/output_images/gen1_ind17.jpg

SCORES
rank=1
ColorDistributionMeasure=0.457579
ComplexityMeasure=0.948509
LivelinessMeasure=0.96991
LuminanceDistributionMeasure=0.766952
LuminanceRedundancyMeasure=0.975112

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.3148195278808585
novelty_score=0.24908236403093176
surprise_score=0.0
