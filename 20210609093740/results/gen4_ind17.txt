IMAGE
name=gen4_ind17
content=dataset/output_images/gen3_ind7.jpg
style=dataset/output_images/gen3_ind4.jpg

SCORES
rank=1
ColorDistributionMeasure=0.634037
ComplexityMeasure=0.872511
LivelinessMeasure=0.976028
LuminanceDistributionMeasure=0.734732
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[content_weight=1500,style_weight=0.25]

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.3967132859960973
novelty_score=0.17823641133976784
surprise_score=0.0
