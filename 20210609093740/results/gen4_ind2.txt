IMAGE
name=gen4_ind2
content=dataset/output_images/gen3_ind1.jpg
style=dataset/output_images/gen3_ind6.jpg

SCORES
rank=1
ColorDistributionMeasure=0.624902
ComplexityMeasure=0.887669
LivelinessMeasure=0.975766
LuminanceDistributionMeasure=0.698332
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[content_weight=1250,style_weight=0.15]
mutation_2=[color_filter=bright]
mutation_3=[style_transfer=dataset/tmp_images/dataset/tmp_images/7848.jpg]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.377981542174004
novelty_score=0.19258890885629773
surprise_score=0.2
