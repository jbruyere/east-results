IMAGE
name=gen2_ind17
content=dataset/output_images/gen1_ind8.jpg
style=dataset/output_images/gen1_ind0.jpg

SCORES
rank=1
ColorDistributionMeasure=0.61238
ComplexityMeasure=0.864086
LivelinessMeasure=0.97233
LuminanceDistributionMeasure=0.788496
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[content_weight=500,style_weight=0.2]
mutation_2=[style_transfer=dataset/tmp_images/dataset/tmp_images/85166.jpg]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.4054006372593079
novelty_score=0.1983020265945383
surprise_score=0.2
