IMAGE
name=gen3_ind6
content=dataset/output_images/gen2_ind8.jpg
style=dataset/output_images/gen2_ind14.jpg

SCORES
rank=1
ColorDistributionMeasure=0.660069
ComplexityMeasure=0.894026
LivelinessMeasure=0.9744
LuminanceDistributionMeasure=0.769856
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.4426762883737029
novelty_score=0.183368379871539
surprise_score=0.0
