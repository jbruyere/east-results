IMAGE
name=gen1_ind1
content=dataset/input_images/well-by-the-winding-road-in-the-park-of-chateau-noir.jpg
style=dataset/input_images/marguerite-1975.jpg

SCORES
rank=1
ColorDistributionMeasure=0.789014
ComplexityMeasure=0.960153
LivelinessMeasure=0.932088
LuminanceDistributionMeasure=0.575754
LuminanceRedundancyMeasure=0.94528

MUTATIONS
mutation_1=[color_filter=dark]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.38430806838649967
novelty_score=0.28675552515627917
surprise_score=0.4
