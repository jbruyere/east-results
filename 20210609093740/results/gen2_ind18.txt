IMAGE
name=gen2_ind18
content=dataset/output_images/gen1_ind14.jpg
style=dataset/output_images/gen1_ind19.jpg

SCORES
rank=1
ColorDistributionMeasure=0.411346
ComplexityMeasure=0.931013
LivelinessMeasure=0.974732
LuminanceDistributionMeasure=0.774806
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.2892285916595155
novelty_score=0.28384019609983774
surprise_score=0.2
