IMAGE
name=gen1_ind9
content=dataset/input_images/linhas-de-sombra.jpg
style=dataset/input_images/orfeu-nos-infernos-detail-1904.jpg

SCORES
rank=2
ColorDistributionMeasure=0.43867
ComplexityMeasure=0.903267
LivelinessMeasure=0.964375
LuminanceDistributionMeasure=0.736593
LuminanceRedundancyMeasure=0.999294

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.28126836535495026
novelty_score=0.2990049992332524
surprise_score=0.6000000000000001
