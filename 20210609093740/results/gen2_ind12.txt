IMAGE
name=gen2_ind12
content=dataset/output_images/gen1_ind7.jpg
style=dataset/output_images/gen1_ind17.jpg

SCORES
rank=1
ColorDistributionMeasure=0.519071
ComplexityMeasure=0.969364
LivelinessMeasure=0.959158
LuminanceDistributionMeasure=0.821903
LuminanceRedundancyMeasure=0.978319

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.3880653440656921
novelty_score=0.20538212948861184
surprise_score=0.2
