IMAGE
name=gen2_ind11
content=dataset/output_images/gen1_ind14.jpg
style=dataset/output_images/gen1_ind16.jpg

SCORES
rank=1
ColorDistributionMeasure=0.688749
ComplexityMeasure=0.877213
LivelinessMeasure=0.975624
LuminanceDistributionMeasure=0.720445
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[color_filter=warm]
mutation_2=[content_weight=1000,style_weight=0.15]
mutation_3=[style_transfer=dataset/tmp_images/dataset/tmp_images/56436.jpg]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.4246678147114753
novelty_score=0.20900346247812782
surprise_score=0.6000000000000001
