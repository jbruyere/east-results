# RESULTS OF SEVERAL RUNS OF THE EVOLUTIONARY ALGORITHM FOR STYLE TRANSFER

the software: https://gitlab.com/jbruyere/evolutionary-algorithm-for-style-transfer

This repository presents results (classified by date) of different executions of the software, with several parameters, and after different iterations.
