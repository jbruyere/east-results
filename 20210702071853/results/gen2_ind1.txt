IMAGE
name=gen2_ind1
content=dataset/output_images/gen1_ind0.jpg
style=dataset/output_images/gen1_ind7.jpg

SCORES
rank=1
ColorDistributionMeasure=-5.3588
ComplexityMeasure=0.820348
LivelinessMeasure=0.96899
LuminanceDistributionMeasure=0.830622
LuminanceRedundancyMeasure=0.999294

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-3.535751033632449
novelty_score=5.965252925409642
surprise_score=0.4
