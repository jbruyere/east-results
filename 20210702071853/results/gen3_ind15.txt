IMAGE
name=gen3_ind15
content=dataset/output_images/gen2_ind8.jpg
style=dataset/output_images/gen2_ind5.jpg

SCORES
rank=1
ColorDistributionMeasure=0.167032
ComplexityMeasure=0.84005
LivelinessMeasure=0.971482
LuminanceDistributionMeasure=0.67449
LuminanceRedundancyMeasure=0.99716

MUTATIONS
mutation_1=[content_weight=1250,style_weight=0.1]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.09168112626595591
novelty_score=0.6485707167448
surprise_score=0.6000000000000001
