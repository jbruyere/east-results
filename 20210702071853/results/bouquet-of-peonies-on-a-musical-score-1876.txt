IMAGE
name=bouquet-of-peonies-on-a-musical-score-1876
content=
style=

SCORES
rank=1
ColorDistributionMeasure=0.654942
ComplexityMeasure=0.992854
LivelinessMeasure=0.968062
LuminanceDistributionMeasure=0.858789
LuminanceRedundancyMeasure=0.994275

MUTATIONS

METRICS
valuable=1
novel=0
surprising=1
creative=False


METRICS_VALUES
value_score=0.5375073373091193
novelty_score=0
surprise_score=1.0
