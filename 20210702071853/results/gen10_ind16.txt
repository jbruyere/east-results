IMAGE
name=gen10_ind16
content=dataset/output_images/gen9_ind6.jpg
style=dataset/output_images/gen9_ind19.jpg

SCORES
rank=2
ColorDistributionMeasure=-0.238318
ComplexityMeasure=0.812415
LivelinessMeasure=0.963244
LuminanceDistributionMeasure=0.736369
LuminanceRedundancyMeasure=0.993546

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-0.13644403928364282
novelty_score=3.6960421336484877
surprise_score=0.2
