IMAGE
name=gen2_ind0
content=dataset/output_images/gen1_ind2.jpg
style=dataset/output_images/gen1_ind7.jpg

SCORES
rank=2
ColorDistributionMeasure=-0.352516
ComplexityMeasure=0.830694
LivelinessMeasure=0.971154
LuminanceDistributionMeasure=0.724887
LuminanceRedundancyMeasure=0.998586

MUTATIONS
mutation_1=[content_weight=1500,style_weight=0.2]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-0.20585612561489042
novelty_score=0.9700070240276969
surprise_score=0.6000000000000001
