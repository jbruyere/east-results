IMAGE
name=self-portrait-with-easel
content=
style=

SCORES
rank=1
ColorDistributionMeasure=0.731748
ComplexityMeasure=0.978166
LivelinessMeasure=0.970639
LuminanceDistributionMeasure=0.733666
LuminanceRedundancyMeasure=0.999294

MUTATIONS

METRICS
valuable=1
novel=0
surprising=1
creative=False


METRICS_VALUES
value_score=0.5093584524962894
novelty_score=0
surprise_score=1.0
