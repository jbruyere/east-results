IMAGE
name=gen3_ind5
content=dataset/output_images/gen2_ind1.jpg
style=dataset/output_images/gen2_ind8.jpg

SCORES
rank=2
ColorDistributionMeasure=0.381341
ComplexityMeasure=0.837839
LivelinessMeasure=0.964495
LuminanceDistributionMeasure=0.778053
LuminanceRedundancyMeasure=0.994275

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/12289.jpg]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.2383909449454733
novelty_score=0.4835973039811718
surprise_score=0.4
