IMAGE
name=gen5_ind4
content=dataset/output_images/gen4_ind9.jpg
style=dataset/output_images/gen4_ind11.jpg

SCORES
rank=2
ColorDistributionMeasure=-2.291633
ComplexityMeasure=0.818722
LivelinessMeasure=0.971085
LuminanceDistributionMeasure=0.713869
LuminanceRedundancyMeasure=0.997874

MUTATIONS
mutation_1=[content_weight=500,style_weight=0.25]

METRICS
valuable=0
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=-1.2978754091185278
novelty_score=3.936135811610397
surprise_score=0.0
