IMAGE
name=gen3_ind0
content=dataset/output_images/gen2_ind8.jpg
style=dataset/output_images/gen2_ind17.jpg

SCORES
rank=1
ColorDistributionMeasure=0.382857
ComplexityMeasure=0.900671
LivelinessMeasure=0.964765
LuminanceDistributionMeasure=0.733382
LuminanceRedundancyMeasure=0.975112

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.2379080068599245
novelty_score=0.4771076767997855
surprise_score=0.2
