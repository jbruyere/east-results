IMAGE
name=gen10_ind19
content=dataset/output_images/gen9_ind16.jpg
style=dataset/output_images/gen9_ind5.jpg

SCORES
rank=1
ColorDistributionMeasure=0.232968
ComplexityMeasure=0.829383
LivelinessMeasure=0.964361
LuminanceDistributionMeasure=0.767611
LuminanceRedundancyMeasure=0.989858

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.14158104916938513
novelty_score=3.5784920380793137
surprise_score=0.2
