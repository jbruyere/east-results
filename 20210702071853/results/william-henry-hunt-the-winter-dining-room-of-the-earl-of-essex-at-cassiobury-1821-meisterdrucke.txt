IMAGE
name=william-henry-hunt-the-winter-dining-room-of-the-earl-of-essex-at-cassiobury-1821-meisterdrucke
content=
style=

SCORES
rank=1
ColorDistributionMeasure=0.861874
ComplexityMeasure=0.998252
LivelinessMeasure=0.961251
LuminanceDistributionMeasure=0.832301
LuminanceRedundancyMeasure=0.966839

MUTATIONS

METRICS
valuable=1
novel=0
surprising=1
creative=False


METRICS_VALUES
value_score=0.6655111715903295
novelty_score=0
surprise_score=1.0
