IMAGE
name=gen3_ind12
content=dataset/output_images/gen2_ind18.jpg
style=dataset/output_images/gen2_ind8.jpg

SCORES
rank=1
ColorDistributionMeasure=0.39089
ComplexityMeasure=0.894606
LivelinessMeasure=0.970956
LuminanceDistributionMeasure=0.750611
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[color_filter=cool]

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.2546795776458828
novelty_score=0.46952201208844596
surprise_score=0.0
