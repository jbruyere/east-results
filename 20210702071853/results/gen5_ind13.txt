IMAGE
name=gen5_ind13
content=dataset/output_images/gen4_ind7.jpg
style=dataset/output_images/gen4_ind9.jpg

SCORES
rank=1
ColorDistributionMeasure=-0.713812
ComplexityMeasure=0.824784
LivelinessMeasure=0.966601
LuminanceDistributionMeasure=0.837158
LuminanceRedundancyMeasure=0.998586

MUTATIONS

METRICS
valuable=0
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=-0.47573402863118885
novelty_score=2.574587990910936
surprise_score=0.0
