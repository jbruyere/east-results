IMAGE
name=gen9_ind4
content=dataset/output_images/gen8_ind6.jpg
style=dataset/output_images/gen8_ind0.jpg

SCORES
rank=1
ColorDistributionMeasure=-0.37361
ComplexityMeasure=0.782221
LivelinessMeasure=0.967293
LuminanceDistributionMeasure=0.791631
LuminanceRedundancyMeasure=0.99716

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-0.2231483344394714
novelty_score=3.5866663267124674
surprise_score=0.2
