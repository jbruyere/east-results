IMAGE
name=gen4_ind18
content=dataset/output_images/gen3_ind19.jpg
style=dataset/output_images/gen3_ind7.jpg

SCORES
rank=1
ColorDistributionMeasure=0.427504
ComplexityMeasure=0.841066
LivelinessMeasure=0.968906
LuminanceDistributionMeasure=0.726703
LuminanceRedundancyMeasure=0.996443

MUTATIONS
mutation_1=[content_weight=750,style_weight=0.1]
mutation_2=[style_transfer=dataset/tmp_images/dataset/tmp_images/9999.jpg]

METRICS
valuable=0
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.2522675088852399
novelty_score=0.4595211565474948
surprise_score=0.0
