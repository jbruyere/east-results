IMAGE
name=gen8_ind1
content=dataset/output_images/gen7_ind3.jpg
style=dataset/output_images/gen7_ind12.jpg

SCORES
rank=2
ColorDistributionMeasure=-0.379802
ComplexityMeasure=0.784169
LivelinessMeasure=0.968536
LuminanceDistributionMeasure=0.704329
LuminanceRedundancyMeasure=0.99716

MUTATIONS
mutation_1=[color_filter=cool]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-0.20259237893995924
novelty_score=3.278955463241154
surprise_score=0.2
