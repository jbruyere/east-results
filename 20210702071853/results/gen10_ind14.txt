IMAGE
name=gen10_ind14
content=dataset/output_images/gen9_ind11.jpg
style=dataset/output_images/gen9_ind8.jpg

SCORES
rank=2
ColorDistributionMeasure=-3.113636
ComplexityMeasure=0.791529
LivelinessMeasure=0.968721
LuminanceDistributionMeasure=0.743075
LuminanceRedundancyMeasure=0.99716

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-1.7690124307600772
novelty_score=5.230282165220002
surprise_score=0.2
