IMAGE
name=gen7_ind3
content=dataset/output_images/gen6_ind14.jpg
style=dataset/output_images/gen6_ind17.jpg

SCORES
rank=1
ColorDistributionMeasure=0.642875
ComplexityMeasure=0.851781
LivelinessMeasure=0.965661
LuminanceDistributionMeasure=0.701219
LuminanceRedundancyMeasure=0.995

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.36894016148402986
novelty_score=3.091908624321908
surprise_score=0.0
