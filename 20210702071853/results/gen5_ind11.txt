IMAGE
name=gen5_ind11
content=dataset/output_images/gen4_ind9.jpg
style=dataset/output_images/gen4_ind18.jpg

SCORES
rank=2
ColorDistributionMeasure=-0.089184
ComplexityMeasure=0.829126
LivelinessMeasure=0.969092
LuminanceDistributionMeasure=0.701786
LuminanceRedundancyMeasure=0.996443

MUTATIONS

METRICS
valuable=0
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=-0.05011060548391345
novelty_score=2.1654003646784825
surprise_score=0.0
