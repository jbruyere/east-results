IMAGE
name=jacob-s-ladder
content=
style=

SCORES
rank=1
ColorDistributionMeasure=0.595727
ComplexityMeasure=0.999554
LivelinessMeasure=0.968143
LuminanceDistributionMeasure=0.818261
LuminanceRedundancyMeasure=0.987608

MUTATIONS

METRICS
valuable=1
novel=0
surprising=1
creative=False


METRICS_VALUES
value_score=0.4658751082412066
novelty_score=0
surprise_score=1.0
