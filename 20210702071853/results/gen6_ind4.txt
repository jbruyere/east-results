IMAGE
name=gen6_ind4
content=dataset/output_images/gen5_ind6.jpg
style=dataset/output_images/gen5_ind17.jpg

SCORES
rank=2
ColorDistributionMeasure=0.436641
ComplexityMeasure=0.837435
LivelinessMeasure=0.964311
LuminanceDistributionMeasure=0.754723
LuminanceRedundancyMeasure=0.990602

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/59235.jpg]

METRICS
valuable=0
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.2636207112582723
novelty_score=3.187571490105746
surprise_score=0.0
