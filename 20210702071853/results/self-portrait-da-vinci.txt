IMAGE
name=self-portrait-da-vinci
content=
style=

SCORES
rank=2
ColorDistributionMeasure=0.775869
ComplexityMeasure=0.9461
LivelinessMeasure=0.968303
LuminanceDistributionMeasure=0.788595
LuminanceRedundancyMeasure=0.981469

MUTATIONS

METRICS
valuable=1
novel=0
surprising=1
creative=False


METRICS_VALUES
value_score=0.5501325295890473
novelty_score=0
surprise_score=1.0
