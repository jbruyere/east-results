IMAGE
name=gen10_ind2
content=dataset/output_images/gen9_ind11.jpg
style=dataset/output_images/gen9_ind6.jpg

SCORES
rank=2
ColorDistributionMeasure=0.046444
ComplexityMeasure=0.822035
LivelinessMeasure=0.967016
LuminanceDistributionMeasure=0.737985
LuminanceRedundancyMeasure=0.995

MUTATIONS
mutation_1=[color_filter=dark]
mutation_2=[content_weight=1500,style_weight=0.005]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.027109668100674115
novelty_score=3.6164614704994174
surprise_score=0.4
