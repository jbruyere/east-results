IMAGE
name=gen7_ind7
content=dataset/output_images/gen6_ind2.jpg
style=dataset/output_images/gen6_ind7.jpg

SCORES
rank=1
ColorDistributionMeasure=-5.262273
ComplexityMeasure=0.809929
LivelinessMeasure=0.970324
LuminanceDistributionMeasure=0.773172
LuminanceRedundancyMeasure=0.998586

MUTATIONS
mutation_1=[color_filter=bright]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-3.1929983101090187
novelty_score=7.06621111651647
surprise_score=0.2
