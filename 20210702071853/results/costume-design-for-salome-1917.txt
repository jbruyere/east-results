IMAGE
name=costume-design-for-salome-1917
content=
style=

SCORES
rank=2
ColorDistributionMeasure=0.756401
ComplexityMeasure=0.965887
LivelinessMeasure=0.97035
LuminanceDistributionMeasure=0.828522
LuminanceRedundancyMeasure=0.993546

MUTATIONS

METRICS
valuable=1
novel=0
surprising=1
creative=False


METRICS_VALUES
value_score=0.5835779169729374
novelty_score=0
surprise_score=1.0
