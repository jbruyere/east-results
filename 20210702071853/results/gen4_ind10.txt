IMAGE
name=gen4_ind10
content=dataset/output_images/gen3_ind10.jpg
style=dataset/output_images/gen3_ind14.jpg

SCORES
rank=1
ColorDistributionMeasure=-0.231519
ComplexityMeasure=0.830356
LivelinessMeasure=0.971526
LuminanceDistributionMeasure=0.719559
LuminanceRedundancyMeasure=0.99716

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-0.13400982876351378
novelty_score=0.9387939382593302
surprise_score=0.2
