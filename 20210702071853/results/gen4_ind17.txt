IMAGE
name=gen4_ind17
content=dataset/output_images/gen3_ind1.jpg
style=dataset/output_images/gen3_ind11.jpg

SCORES
rank=2
ColorDistributionMeasure=-0.001715
ComplexityMeasure=0.827356
LivelinessMeasure=0.967258
LuminanceDistributionMeasure=0.76657
LuminanceRedundancyMeasure=0.99716

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/1657.jpg]

METRICS
valuable=0
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=-0.0010490967543060488
novelty_score=0.7380316336244213
surprise_score=0.0
