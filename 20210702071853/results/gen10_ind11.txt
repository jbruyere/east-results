IMAGE
name=gen10_ind11
content=dataset/output_images/gen9_ind0.jpg
style=dataset/output_images/gen9_ind5.jpg

SCORES
rank=2
ColorDistributionMeasure=-2.814725
ComplexityMeasure=0.806185
LivelinessMeasure=0.964349
LuminanceDistributionMeasure=0.704706
LuminanceRedundancyMeasure=0.99208

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/36041.jpg]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-1.529887802008162
novelty_score=5.00130181486719
surprise_score=0.4
