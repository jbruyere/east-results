IMAGE
name=gen10_ind10
content=dataset/output_images/gen9_ind17.jpg
style=dataset/output_images/gen9_ind16.jpg

SCORES
rank=1
ColorDistributionMeasure=-0.413608
ComplexityMeasure=0.780543
LivelinessMeasure=0.966366
LuminanceDistributionMeasure=0.767247
LuminanceRedundancyMeasure=0.998586

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-0.2390276144699733
novelty_score=3.7651239051624015
surprise_score=0.2
