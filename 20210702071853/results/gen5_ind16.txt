IMAGE
name=gen5_ind16
content=dataset/output_images/gen4_ind16.jpg
style=dataset/output_images/gen4_ind11.jpg

SCORES
rank=1
ColorDistributionMeasure=0.333613
ComplexityMeasure=0.845112
LivelinessMeasure=0.968655
LuminanceDistributionMeasure=0.81099
LuminanceRedundancyMeasure=0.99716

MUTATIONS
mutation_1=[content_weight=500,style_weight=0.1]
mutation_2=[color_filter=cool]

METRICS
valuable=0
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.22085473087590965
novelty_score=1.8393994724752687
surprise_score=0.0
