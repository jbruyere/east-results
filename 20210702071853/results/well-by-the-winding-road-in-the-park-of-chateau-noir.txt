IMAGE
name=well-by-the-winding-road-in-the-park-of-chateau-noir
content=
style=

SCORES
rank=2
ColorDistributionMeasure=0.752159
ComplexityMeasure=0.966126
LivelinessMeasure=0.924373
LuminanceDistributionMeasure=0.473297
LuminanceRedundancyMeasure=0.949057

MUTATIONS

METRICS
valuable=0
novel=0
surprising=1
creative=False


METRICS_VALUES
value_score=0.30172877282744326
novelty_score=0
surprise_score=1.0
