IMAGE
name=view-of-amalfi-1844
content=
style=

SCORES
rank=1
ColorDistributionMeasure=0.575616
ComplexityMeasure=0.985865
LivelinessMeasure=0.97057
LuminanceDistributionMeasure=0.846637
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=0
surprising=1
creative=False


METRICS_VALUES
value_score=0.4663096611264409
novelty_score=0
surprise_score=1.0
