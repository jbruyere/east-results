IMAGE
name=gen7_ind9
content=dataset/output_images/gen6_ind2.jpg
style=dataset/output_images/gen6_ind17.jpg

SCORES
rank=1
ColorDistributionMeasure=0.468304
ComplexityMeasure=0.839949
LivelinessMeasure=0.963703
LuminanceDistributionMeasure=0.764497
LuminanceRedundancyMeasure=0.993546

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.28793055898413794
novelty_score=3.0593422801186207
surprise_score=0.2
