IMAGE
name=gen10_ind3
content=dataset/output_images/gen9_ind19.jpg
style=dataset/output_images/gen9_ind16.jpg

SCORES
rank=1
ColorDistributionMeasure=0.102664
ComplexityMeasure=0.776344
LivelinessMeasure=0.966244
LuminanceDistributionMeasure=0.791925
LuminanceRedundancyMeasure=0.99716

MUTATIONS
mutation_1=[content_weight=1250,style_weight=0.1]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.06081463359485773
novelty_score=3.6100474861117937
surprise_score=0.2
