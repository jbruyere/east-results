IMAGE
name=gen9_ind12
content=dataset/output_images/gen8_ind11.jpg
style=dataset/output_images/gen8_ind9.jpg

SCORES
rank=1
ColorDistributionMeasure=-0.423337
ComplexityMeasure=0.778755
LivelinessMeasure=0.969406
LuminanceDistributionMeasure=0.748603
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[content_weight=1250,style_weight=0.25]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-0.23907690352375896
novelty_score=3.6110690241522234
surprise_score=0.4
