IMAGE
name=gen3_ind7
content=dataset/output_images/gen2_ind1.jpg
style=dataset/output_images/gen2_ind16.jpg

SCORES
rank=1
ColorDistributionMeasure=0.58865
ComplexityMeasure=0.866727
LivelinessMeasure=0.965502
LuminanceDistributionMeasure=0.771477
LuminanceRedundancyMeasure=0.995

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.3781278937671216
novelty_score=0.40353220926666955
surprise_score=0.6000000000000001
