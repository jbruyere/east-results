IMAGE
name=via-san-leonardo
content=
style=

SCORES
rank=1
ColorDistributionMeasure=0.857001
ComplexityMeasure=0.932492
LivelinessMeasure=0.949094
LuminanceDistributionMeasure=0.85802
LuminanceRedundancyMeasure=0.966839

MUTATIONS

METRICS
valuable=1
novel=0
surprising=1
creative=False


METRICS_VALUES
value_score=0.6291978686484319
novelty_score=0
surprise_score=1.0
