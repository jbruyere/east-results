IMAGE
name=gen5_ind17
content=dataset/output_images/gen4_ind10.jpg
style=dataset/output_images/gen4_ind4.jpg

SCORES
rank=1
ColorDistributionMeasure=0.661262
ComplexityMeasure=0.865397
LivelinessMeasure=0.969101
LuminanceDistributionMeasure=0.700278
LuminanceRedundancyMeasure=0.988361

MUTATIONS
mutation_1=[color_filter=dark]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.38383456061412585
novelty_score=1.8179541080830592
surprise_score=0.8
