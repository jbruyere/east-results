IMAGE
name=gen4_ind8
content=dataset/output_images/gen3_ind19.jpg
style=dataset/output_images/gen3_ind14.jpg

SCORES
rank=2
ColorDistributionMeasure=-0.795227
ComplexityMeasure=0.825153
LivelinessMeasure=0.969994
LuminanceDistributionMeasure=0.741126
LuminanceRedundancyMeasure=0.99716

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-0.47038292263991277
novelty_score=1.4383642111282111
surprise_score=0.4
