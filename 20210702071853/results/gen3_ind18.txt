IMAGE
name=gen3_ind18
content=dataset/output_images/gen2_ind16.jpg
style=dataset/output_images/gen2_ind17.jpg

SCORES
rank=1
ColorDistributionMeasure=0.327877
ComplexityMeasure=0.902775
LivelinessMeasure=0.96115
LuminanceDistributionMeasure=0.795595
LuminanceRedundancyMeasure=0.989111

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.22388176586838038
novelty_score=0.4950625823043303
surprise_score=0.0
