IMAGE
name=marilyn-1
content=
style=

SCORES
rank=1
ColorDistributionMeasure=0.795621
ComplexityMeasure=0.972389
LivelinessMeasure=0.966395
LuminanceDistributionMeasure=0.806524
LuminanceRedundancyMeasure=0.994275

MUTATIONS

METRICS
valuable=1
novel=0
surprising=1
creative=False


METRICS_VALUES
value_score=0.5995491122037194
novelty_score=0
surprise_score=1.0
