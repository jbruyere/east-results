IMAGE
name=gen1_ind13
content=dataset/input_images/costume-design-for-salome-1917.jpg
style=dataset/input_images/jacob-s-ladder.jpg

SCORES
rank=2
ColorDistributionMeasure=0.254793
ComplexityMeasure=0.920841
LivelinessMeasure=0.969074
LuminanceDistributionMeasure=0.759567
LuminanceRedundancyMeasure=0.990602

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.17107808117611983
novelty_score=0.43307081465643876
surprise_score=0.6000000000000001
