IMAGE
name=gen7_ind8
content=dataset/output_images/gen6_ind2.jpg
style=dataset/output_images/gen6_ind1.jpg

SCORES
rank=1
ColorDistributionMeasure=0.26231
ComplexityMeasure=0.83423
LivelinessMeasure=0.967528
LuminanceDistributionMeasure=0.783985
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[content_weight=750,style_weight=0.15]
mutation_2=[style_transfer=dataset/tmp_images/dataset/tmp_images/65090.jpg]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.1658690000415577
novelty_score=3.104168655114717
surprise_score=0.2
