IMAGE
name=gen8_ind8
content=dataset/output_images/gen7_ind11.jpg
style=dataset/output_images/gen7_ind3.jpg

SCORES
rank=1
ColorDistributionMeasure=-0.693146
ComplexityMeasure=0.812046
LivelinessMeasure=0.967503
LuminanceDistributionMeasure=0.762637
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[color_filter=dark]
mutation_2=[style_transfer=dataset/tmp_images/dataset/tmp_images/64066.jpg]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-0.4150198074473969
novelty_score=3.4537615748114017
surprise_score=0.2
