IMAGE
name=gen10_ind6
content=dataset/output_images/gen9_ind0.jpg
style=dataset/output_images/gen9_ind11.jpg

SCORES
rank=1
ColorDistributionMeasure=0.061767
ComplexityMeasure=0.776643
LivelinessMeasure=0.96932
LuminanceDistributionMeasure=0.73902
LuminanceRedundancyMeasure=0.995723

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.03421683573950438
novelty_score=3.6183873125182124
surprise_score=0.2
