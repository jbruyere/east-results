IMAGE
name=gen7_ind11
content=dataset/output_images/gen6_ind5.jpg
style=dataset/output_images/gen6_ind1.jpg

SCORES
rank=1
ColorDistributionMeasure=-2.854133
ComplexityMeasure=0.791735
LivelinessMeasure=0.967901
LuminanceDistributionMeasure=0.756502
LuminanceRedundancyMeasure=0.998586

MUTATIONS
mutation_1=[color_filter=cool]
mutation_2=[content_weight=1500,style_weight=0.005]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-1.6522681953996499
novelty_score=5.171685628547289
surprise_score=0.2
