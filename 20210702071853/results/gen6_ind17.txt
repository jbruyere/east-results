IMAGE
name=gen6_ind17
content=dataset/output_images/gen5_ind0.jpg
style=dataset/output_images/gen5_ind6.jpg

SCORES
rank=1
ColorDistributionMeasure=0.492283
ComplexityMeasure=0.838025
LivelinessMeasure=0.965309
LuminanceDistributionMeasure=0.817384
LuminanceRedundancyMeasure=0.995

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.32388242450329446
novelty_score=3.1784593315450405
surprise_score=0.2
