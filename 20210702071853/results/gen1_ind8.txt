IMAGE
name=gen1_ind8
content=dataset/input_images/coast-near-antibes.jpg
style=dataset/input_images/american-500-sunset.jpg

SCORES
rank=1
ColorDistributionMeasure=0.637476
ComplexityMeasure=0.873127
LivelinessMeasure=0.968794
LuminanceDistributionMeasure=0.733122
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/32823.jpg]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.3950410525209081
novelty_score=0.20164759587851416
surprise_score=0.2
