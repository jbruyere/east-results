IMAGE
name=rooftops-venice-1983
content=
style=

SCORES
rank=1
ColorDistributionMeasure=0.805386
ComplexityMeasure=0.93509
LivelinessMeasure=0.964354
LuminanceDistributionMeasure=0.756112
LuminanceRedundancyMeasure=0.970194

MUTATIONS

METRICS
valuable=1
novel=0
surprising=1
creative=False


METRICS_VALUES
value_score=0.5327686849391476
novelty_score=0
surprise_score=1.0
