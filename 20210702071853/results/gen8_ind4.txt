IMAGE
name=gen8_ind4
content=dataset/output_images/gen7_ind3.jpg
style=dataset/output_images/gen7_ind13.jpg

SCORES
rank=2
ColorDistributionMeasure=0.628102
ComplexityMeasure=0.845642
LivelinessMeasure=0.963002
LuminanceDistributionMeasure=0.70756
LuminanceRedundancyMeasure=0.983023

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.3557712605433199
novelty_score=3.000100392880225
surprise_score=0.2
