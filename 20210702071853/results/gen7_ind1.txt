IMAGE
name=gen7_ind1
content=dataset/output_images/gen6_ind1.jpg
style=dataset/output_images/gen6_ind7.jpg

SCORES
rank=1
ColorDistributionMeasure=-10.068287
ComplexityMeasure=0.800591
LivelinessMeasure=0.96998
LuminanceDistributionMeasure=0.757837
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[content_weight=1250,style_weight=0.2]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-5.921042180100915
novelty_score=11.337348148684406
surprise_score=0.2
