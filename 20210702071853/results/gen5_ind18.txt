IMAGE
name=gen5_ind18
content=dataset/output_images/gen4_ind10.jpg
style=dataset/output_images/gen4_ind9.jpg

SCORES
rank=1
ColorDistributionMeasure=-9.060158
ComplexityMeasure=0.812448
LivelinessMeasure=0.970209
LuminanceDistributionMeasure=0.728926
LuminanceRedundancyMeasure=0.998586

MUTATIONS
mutation_1=[color_filter=warm]
mutation_2=[content_weight=1500,style_weight=0.2]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-5.198350500954577
novelty_score=10.292376139917948
surprise_score=0.2
