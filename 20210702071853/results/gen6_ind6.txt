IMAGE
name=gen6_ind6
content=dataset/output_images/gen5_ind1.jpg
style=dataset/output_images/gen5_ind6.jpg

SCORES
rank=2
ColorDistributionMeasure=-2.96904
ComplexityMeasure=0.792572
LivelinessMeasure=0.968048
LuminanceDistributionMeasure=0.764448
LuminanceRedundancyMeasure=0.997874

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/43870.jpg]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-1.7377020840748085
novelty_score=5.607703720398543
surprise_score=0.2
