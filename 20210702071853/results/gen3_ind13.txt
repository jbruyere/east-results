IMAGE
name=gen3_ind13
content=dataset/output_images/gen2_ind9.jpg
style=dataset/output_images/gen2_ind3.jpg

SCORES
rank=1
ColorDistributionMeasure=0.459284
ComplexityMeasure=0.853689
LivelinessMeasure=0.969661
LuminanceDistributionMeasure=0.733246
LuminanceRedundancyMeasure=0.99716

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/92156.jpg]
mutation_2=[color_filter=cool]
mutation_3=[content_weight=750,style_weight=0.2]

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.27798123602700725
novelty_score=0.4529805916256184
surprise_score=0.0
