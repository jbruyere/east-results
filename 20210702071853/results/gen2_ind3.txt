IMAGE
name=gen2_ind3
content=dataset/output_images/gen1_ind7.jpg
style=dataset/output_images/gen1_ind1.jpg

SCORES
rank=1
ColorDistributionMeasure=0.548617
ComplexityMeasure=0.934281
LivelinessMeasure=0.960407
LuminanceDistributionMeasure=0.815534
LuminanceRedundancyMeasure=0.983796

MUTATIONS
mutation_1=[color_filter=warm]

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.3949564574090501
novelty_score=0.19893719142795507
surprise_score=0.0
