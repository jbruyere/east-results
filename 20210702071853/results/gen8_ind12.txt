IMAGE
name=gen8_ind12
content=dataset/output_images/gen7_ind3.jpg
style=dataset/output_images/gen7_ind17.jpg

SCORES
rank=1
ColorDistributionMeasure=0.103337
ComplexityMeasure=0.820247
LivelinessMeasure=0.964581
LuminanceDistributionMeasure=0.726312
LuminanceRedundancyMeasure=0.995

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/65629.jpg]

METRICS
valuable=0
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.05908612424081674
novelty_score=3.055566059047537
surprise_score=0.0
