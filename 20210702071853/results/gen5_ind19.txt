IMAGE
name=gen5_ind19
content=dataset/output_images/gen4_ind11.jpg
style=dataset/output_images/gen4_ind3.jpg

SCORES
rank=1
ColorDistributionMeasure=-0.28089
ComplexityMeasure=0.826089
LivelinessMeasure=0.967824
LuminanceDistributionMeasure=0.773092
LuminanceRedundancyMeasure=0.998586

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-0.1733708813842528
novelty_score=2.226775949084525
surprise_score=0.2
