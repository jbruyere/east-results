IMAGE
name=gen5_ind1
content=dataset/output_images/gen4_ind5.jpg
style=dataset/output_images/gen4_ind9.jpg

SCORES
rank=1
ColorDistributionMeasure=-64.858856
ComplexityMeasure=0.807315
LivelinessMeasure=0.968696
LuminanceDistributionMeasure=0.782308
LuminanceRedundancyMeasure=0.997874

MUTATIONS
mutation_1=[content_weight=1500,style_weight=0.2]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-39.596180096430416
novelty_score=63.801045642504256
surprise_score=0.4
