IMAGE
name=gen8_ind11
content=dataset/output_images/gen7_ind11.jpg
style=dataset/output_images/gen7_ind1.jpg

SCORES
rank=1
ColorDistributionMeasure=-7.882734
ComplexityMeasure=0.795517
LivelinessMeasure=0.969776
LuminanceDistributionMeasure=0.750791
LuminanceRedundancyMeasure=0.998586

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-4.559343357460039
novelty_score=8.977897313739163
surprise_score=0.2
