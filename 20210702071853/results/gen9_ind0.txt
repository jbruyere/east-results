IMAGE
name=gen9_ind0
content=dataset/output_images/gen8_ind17.jpg
style=dataset/output_images/gen8_ind0.jpg

SCORES
rank=1
ColorDistributionMeasure=0.246096
ComplexityMeasure=0.773012
LivelinessMeasure=0.968192
LuminanceDistributionMeasure=0.714442
LuminanceRedundancyMeasure=0.996443

MUTATIONS
mutation_1=[content_weight=750,style_weight=0.25]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.13112083873859828
novelty_score=3.381416660090844
surprise_score=0.4
