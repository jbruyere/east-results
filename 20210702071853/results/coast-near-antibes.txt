IMAGE
name=coast-near-antibes
content=
style=

SCORES
rank=2
ColorDistributionMeasure=0.324257
ComplexityMeasure=0.856443
LivelinessMeasure=0.966224
LuminanceDistributionMeasure=0.713885
LuminanceRedundancyMeasure=0.993546

MUTATIONS

METRICS
valuable=1
novel=0
surprising=1
creative=False


METRICS_VALUES
value_score=0.19031888342732137
novelty_score=0
surprise_score=1.0
