IMAGE
name=the-starry-night
content=
style=

SCORES
rank=2
ColorDistributionMeasure=0.568867
ComplexityMeasure=0.953615
LivelinessMeasure=0.967138
LuminanceDistributionMeasure=0.743888
LuminanceRedundancyMeasure=0.959935

MUTATIONS

METRICS
valuable=1
novel=0
surprising=1
creative=False


METRICS_VALUES
value_score=0.3746464674769026
novelty_score=0
surprise_score=1.0
