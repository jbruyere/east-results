IMAGE
name=gen1_ind7
content=dataset/input_images/electrician-1915.jpg
style=dataset/input_images/self-portrait-da-vinci.jpg

SCORES
rank=1
ColorDistributionMeasure=0.294315
ComplexityMeasure=0.848362
LivelinessMeasure=0.967454
LuminanceDistributionMeasure=0.890169
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/38805.jpg]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.21487687258872257
novelty_score=0.422653779488999
surprise_score=0.8
