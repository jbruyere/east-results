IMAGE
name=gen9_ind6
content=dataset/output_images/gen8_ind6.jpg
style=dataset/output_images/gen8_ind11.jpg

SCORES
rank=1
ColorDistributionMeasure=-3.689797
ComplexityMeasure=0.791122
LivelinessMeasure=0.969298
LuminanceDistributionMeasure=0.747833
LuminanceRedundancyMeasure=0.998586

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-2.1129700948270633
novelty_score=5.718335166302821
surprise_score=0.4
