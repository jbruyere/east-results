IMAGE
name=gen6_ind12
content=dataset/output_images/gen5_ind1.jpg
style=dataset/output_images/gen5_ind12.jpg

SCORES
rank=2
ColorDistributionMeasure=0.450359
ComplexityMeasure=0.837057
LivelinessMeasure=0.964552
LuminanceDistributionMeasure=0.747552
LuminanceRedundancyMeasure=0.993546

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/74640.jpg]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.27006537784765383
novelty_score=3.1856048068884624
surprise_score=0.4
