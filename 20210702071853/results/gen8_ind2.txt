IMAGE
name=gen8_ind2
content=dataset/output_images/gen7_ind12.jpg
style=dataset/output_images/gen7_ind1.jpg

SCORES
rank=1
ColorDistributionMeasure=-0.538189
ComplexityMeasure=0.787209
LivelinessMeasure=0.970074
LuminanceDistributionMeasure=0.728389
LuminanceRedundancyMeasure=0.99716

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-0.29850936449433524
novelty_score=3.36562240179434
surprise_score=0.2
