IMAGE
name=electrician-1915
content=
style=

SCORES
rank=2
ColorDistributionMeasure=0.657045
ComplexityMeasure=0.966415
LivelinessMeasure=0.949503
LuminanceDistributionMeasure=0.796917
LuminanceRedundancyMeasure=0.96342

MUTATIONS

METRICS
valuable=1
novel=0
surprising=1
creative=False


METRICS_VALUES
value_score=0.4628964682448508
novelty_score=0
surprise_score=1.0
