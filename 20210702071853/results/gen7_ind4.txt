IMAGE
name=gen7_ind4
content=dataset/output_images/gen6_ind3.jpg
style=dataset/output_images/gen6_ind17.jpg

SCORES
rank=2
ColorDistributionMeasure=0.24239
ComplexityMeasure=0.82731
LivelinessMeasure=0.963849
LuminanceDistributionMeasure=0.742034
LuminanceRedundancyMeasure=0.994275

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.14260091048350992
novelty_score=3.1144474062243046
surprise_score=0.2
