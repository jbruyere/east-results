IMAGE
name=gen5_ind14
content=dataset/output_images/gen4_ind4.jpg
style=dataset/output_images/gen4_ind7.jpg

SCORES
rank=1
ColorDistributionMeasure=-0.554446
ComplexityMeasure=0.793365
LivelinessMeasure=0.972111
LuminanceDistributionMeasure=0.704213
LuminanceRedundancyMeasure=0.99716

MUTATIONS
mutation_1=[content_weight=1000,style_weight=0.15]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-0.30027352085845455
novelty_score=2.4462573904454885
surprise_score=0.4
