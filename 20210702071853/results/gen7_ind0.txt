IMAGE
name=gen7_ind0
content=dataset/output_images/gen6_ind8.jpg
style=dataset/output_images/gen6_ind11.jpg

SCORES
rank=1
ColorDistributionMeasure=0.445422
ComplexityMeasure=0.835381
LivelinessMeasure=0.966652
LuminanceDistributionMeasure=0.699565
LuminanceRedundancyMeasure=0.992814

MUTATIONS

METRICS
valuable=0
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.2498172231546545
novelty_score=3.0736939773500445
surprise_score=0.0
