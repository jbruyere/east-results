IMAGE
name=gen5_ind2
content=dataset/output_images/gen4_ind1.jpg
style=dataset/output_images/gen4_ind4.jpg

SCORES
rank=1
ColorDistributionMeasure=-3.788579
ComplexityMeasure=0.815067
LivelinessMeasure=0.970662
LuminanceDistributionMeasure=0.727619
LuminanceRedundancyMeasure=0.999294

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-2.1793902142047004
novelty_score=5.306939362103909
surprise_score=0.4
