IMAGE
name=gen10_ind15
content=dataset/output_images/gen9_ind2.jpg
style=dataset/output_images/gen9_ind16.jpg

SCORES
rank=1
ColorDistributionMeasure=-6.395122
ComplexityMeasure=0.792904
LivelinessMeasure=0.965989
LuminanceDistributionMeasure=0.762728
LuminanceRedundancyMeasure=0.995

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/56249.jpg]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-3.7173580549225504
novelty_score=7.443854311131494
surprise_score=0.4
