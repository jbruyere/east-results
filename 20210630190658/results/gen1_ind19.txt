IMAGE
name=gen1_ind19
content=dataset/input_images/still-life-vase-with-fifteen-sunflowers-1888-1.jpg
style=dataset/input_images/orfeu-nos-infernos-detail-1904.jpg

SCORES
rank=2
ColorDistributionMeasure=0.515278
ComplexityMeasure=0.93706
LivelinessMeasure=0.966405
LuminanceDistributionMeasure=0.657058
LuminanceRedundancyMeasure=0.99716

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.3057290626138436
novelty_score=0.2588157159962779
surprise_score=1.0
