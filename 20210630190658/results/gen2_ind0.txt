IMAGE
name=gen2_ind0
content=dataset/output_images/gen1_ind18.jpg
style=dataset/output_images/gen1_ind17.jpg

SCORES
rank=1
ColorDistributionMeasure=0.305926
ComplexityMeasure=0.928843
LivelinessMeasure=0.972883
LuminanceDistributionMeasure=0.816183
LuminanceRedundancyMeasure=0.999294

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.22547590567595702
novelty_score=0.3778485115355575
surprise_score=0.2
