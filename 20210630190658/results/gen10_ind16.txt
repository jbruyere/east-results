IMAGE
name=gen10_ind16
content=dataset/output_images/gen9_ind6.jpg
style=dataset/output_images/gen9_ind19.jpg

SCORES
rank=1
ColorDistributionMeasure=0.268056
ComplexityMeasure=0.841028
LivelinessMeasure=0.968901
LuminanceDistributionMeasure=0.7816
LuminanceRedundancyMeasure=0.998586

MUTATIONS
mutation_1=[color_filter=bright]

METRICS
valuable=0
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.17048470222075615
novelty_score=0.32050975255385783
surprise_score=0.0
