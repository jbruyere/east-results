IMAGE
name=gen10_ind5
content=dataset/output_images/gen9_ind10.jpg
style=dataset/output_images/gen9_ind15.jpg

SCORES
rank=1
ColorDistributionMeasure=0.410116
ComplexityMeasure=0.847893
LivelinessMeasure=0.969985
LuminanceDistributionMeasure=0.744494
LuminanceRedundancyMeasure=0.998586

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/11929.jpg]

METRICS
valuable=0
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.25076068998088574
novelty_score=0.25415485206780086
surprise_score=0.0
