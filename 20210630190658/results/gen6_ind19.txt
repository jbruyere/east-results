IMAGE
name=gen6_ind19
content=dataset/output_images/gen5_ind2.jpg
style=dataset/output_images/gen5_ind10.jpg

SCORES
rank=1
ColorDistributionMeasure=0.646301
ComplexityMeasure=0.867395
LivelinessMeasure=0.973021
LuminanceDistributionMeasure=0.735014
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/65453.jpg]
mutation_2=[content_weight=1250,style_weight=0.005]
mutation_3=[color_filter=bright]

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.40064787792270107
novelty_score=0.19925949471073262
surprise_score=0.0
