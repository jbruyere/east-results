IMAGE
name=gen7_ind3
content=dataset/output_images/gen6_ind12.jpg
style=dataset/output_images/gen6_ind19.jpg

SCORES
rank=1
ColorDistributionMeasure=0.292209
ComplexityMeasure=0.846569
LivelinessMeasure=0.969827
LuminanceDistributionMeasure=0.823775
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[color_filter=bright]
mutation_2=[content_weight=1000,style_weight=0.1]

METRICS
valuable=0
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.1974931821897816
novelty_score=0.3236169702470794
surprise_score=0.0
