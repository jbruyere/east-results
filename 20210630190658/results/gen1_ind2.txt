IMAGE
name=gen1_ind2
content=dataset/input_images/electrician-1915.jpg
style=dataset/input_images/dark-bridge-2003.jpg

SCORES
rank=1
ColorDistributionMeasure=0.846675
ComplexityMeasure=0.964506
LivelinessMeasure=0.960047
LuminanceDistributionMeasure=0.777916
LuminanceRedundancyMeasure=0.98533

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.6009364883280248
novelty_score=0.21714783634241366
surprise_score=0.6000000000000001
