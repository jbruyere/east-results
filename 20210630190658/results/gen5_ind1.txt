IMAGE
name=gen5_ind1
content=dataset/output_images/gen4_ind2.jpg
style=dataset/output_images/gen4_ind4.jpg

SCORES
rank=1
ColorDistributionMeasure=0.253618
ComplexityMeasure=0.843071
LivelinessMeasure=0.973197
LuminanceDistributionMeasure=0.809429
LuminanceRedundancyMeasure=0.998586

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.16819350414111764
novelty_score=0.3887110607830695
surprise_score=0.4
