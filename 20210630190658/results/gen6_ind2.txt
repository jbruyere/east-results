IMAGE
name=gen6_ind2
content=dataset/output_images/gen5_ind7.jpg
style=dataset/output_images/gen5_ind10.jpg

SCORES
rank=2
ColorDistributionMeasure=-0.036045
ComplexityMeasure=0.836967
LivelinessMeasure=0.971623
LuminanceDistributionMeasure=0.780034
LuminanceRedundancyMeasure=0.999294

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-0.022848514228008772
novelty_score=0.6370283091027062
surprise_score=0.4
