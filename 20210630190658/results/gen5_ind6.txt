IMAGE
name=gen5_ind6
content=dataset/output_images/gen4_ind18.jpg
style=dataset/output_images/gen4_ind2.jpg

SCORES
rank=2
ColorDistributionMeasure=0.117077
ComplexityMeasure=0.837889
LivelinessMeasure=0.973687
LuminanceDistributionMeasure=0.714194
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[color_filter=bright]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.06816900000020867
novelty_score=0.529116979281544
surprise_score=0.4
