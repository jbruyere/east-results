IMAGE
name=gen6_ind15
content=dataset/output_images/gen5_ind0.jpg
style=dataset/output_images/gen5_ind14.jpg

SCORES
rank=2
ColorDistributionMeasure=0.018083
ComplexityMeasure=0.834695
LivelinessMeasure=0.971417
LuminanceDistributionMeasure=0.743184
LuminanceRedundancyMeasure=0.999294

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.010889141083541316
novelty_score=0.5872081738940546
surprise_score=0.6000000000000001
