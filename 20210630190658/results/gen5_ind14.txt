IMAGE
name=gen5_ind14
content=dataset/output_images/gen4_ind2.jpg
style=dataset/output_images/gen4_ind18.jpg

SCORES
rank=1
ColorDistributionMeasure=0.666818
ComplexityMeasure=0.866355
LivelinessMeasure=0.972607
LuminanceDistributionMeasure=0.791074
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/96974.jpg]
mutation_2=[color_filter=cool]
mutation_3=[content_weight=1500,style_weight=0.25]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.4441718002608378
novelty_score=0.1751139728467128
surprise_score=0.2
