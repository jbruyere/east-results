IMAGE
name=gen8_ind5
content=dataset/output_images/gen7_ind2.jpg
style=dataset/output_images/gen7_ind7.jpg

SCORES
rank=1
ColorDistributionMeasure=-0.346525
ComplexityMeasure=0.829055
LivelinessMeasure=0.972099
LuminanceDistributionMeasure=0.742993
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-0.20749762661711224
novelty_score=0.8984003128749116
surprise_score=0.6000000000000001
