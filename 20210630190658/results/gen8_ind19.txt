IMAGE
name=gen8_ind19
content=dataset/output_images/gen7_ind3.jpg
style=dataset/output_images/gen7_ind4.jpg

SCORES
rank=1
ColorDistributionMeasure=0.548764
ComplexityMeasure=0.857917
LivelinessMeasure=0.96868
LuminanceDistributionMeasure=0.795958
LuminanceRedundancyMeasure=0.998586

MUTATIONS
mutation_1=[content_weight=1250,style_weight=0.005]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.3624823334664241
novelty_score=0.20965630323016773
surprise_score=0.2
