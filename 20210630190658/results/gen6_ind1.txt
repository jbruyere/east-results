IMAGE
name=gen6_ind1
content=dataset/output_images/gen5_ind1.jpg
style=dataset/output_images/gen5_ind18.jpg

SCORES
rank=1
ColorDistributionMeasure=0.379504
ComplexityMeasure=0.849788
LivelinessMeasure=0.97289
LuminanceDistributionMeasure=0.793191
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/80022.jpg]
mutation_2=[content_weight=1500,style_weight=0.005]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.24869196214619244
novelty_score=0.263178824295643
surprise_score=0.2
