IMAGE
name=gen3_ind6
content=dataset/output_images/gen2_ind1.jpg
style=dataset/output_images/gen2_ind0.jpg

SCORES
rank=2
ColorDistributionMeasure=0.586032
ComplexityMeasure=0.87155
LivelinessMeasure=0.970228
LuminanceDistributionMeasure=0.775611
LuminanceRedundancyMeasure=0.997874

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/53468.jpg]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.3835368605758243
novelty_score=0.21251069301289335
surprise_score=0.2
