IMAGE
name=gen8_ind1
content=dataset/output_images/gen7_ind10.jpg
style=dataset/output_images/gen7_ind6.jpg

SCORES
rank=1
ColorDistributionMeasure=-3.191889
ComplexityMeasure=0.813234
LivelinessMeasure=0.971832
LuminanceDistributionMeasure=0.764145
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[content_weight=750,style_weight=0.15]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-1.9276593027159168
novelty_score=3.7323277959592605
surprise_score=0.4
