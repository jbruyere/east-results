IMAGE
name=gen6_ind16
content=dataset/output_images/gen5_ind14.jpg
style=dataset/output_images/gen5_ind2.jpg

SCORES
rank=1
ColorDistributionMeasure=0.446016
ComplexityMeasure=0.850666
LivelinessMeasure=0.973173
LuminanceDistributionMeasure=0.780438
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/88677.jpg]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.28795939458484576
novelty_score=0.22667172624466278
surprise_score=0.2
