IMAGE
name=gen4_ind4
content=dataset/output_images/gen3_ind3.jpg
style=dataset/output_images/gen3_ind11.jpg

SCORES
rank=1
ColorDistributionMeasure=0.647369
ComplexityMeasure=0.873583
LivelinessMeasure=0.973893
LuminanceDistributionMeasure=0.771946
LuminanceRedundancyMeasure=0.999294

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.4248616370568219
novelty_score=0.183647129219993
surprise_score=0.0
