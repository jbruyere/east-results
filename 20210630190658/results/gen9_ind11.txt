IMAGE
name=gen9_ind11
content=dataset/output_images/gen8_ind6.jpg
style=dataset/output_images/gen8_ind19.jpg

SCORES
rank=1
ColorDistributionMeasure=0.23781
ComplexityMeasure=0.836969
LivelinessMeasure=0.971035
LuminanceDistributionMeasure=0.767682
LuminanceRedundancyMeasure=0.998586

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.1481634903431291
novelty_score=0.3888264949538921
surprise_score=0.2
