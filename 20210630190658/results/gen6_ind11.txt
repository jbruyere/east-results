IMAGE
name=gen6_ind11
content=dataset/output_images/gen5_ind11.jpg
style=dataset/output_images/gen5_ind7.jpg

SCORES
rank=2
ColorDistributionMeasure=-0.842248
ComplexityMeasure=0.823541
LivelinessMeasure=0.967619
LuminanceDistributionMeasure=0.795678
LuminanceRedundancyMeasure=0.990602

MUTATIONS
mutation_1=[color_filter=dark]
mutation_2=[content_weight=1000,style_weight=0.2]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-0.5290127654809226
novelty_score=1.4381778945838002
surprise_score=0.8
