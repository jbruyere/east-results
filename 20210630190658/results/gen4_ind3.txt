IMAGE
name=gen4_ind3
content=dataset/output_images/gen3_ind14.jpg
style=dataset/output_images/gen3_ind9.jpg

SCORES
rank=1
ColorDistributionMeasure=0.447991
ComplexityMeasure=0.907392
LivelinessMeasure=0.968058
LuminanceDistributionMeasure=0.874059
LuminanceRedundancyMeasure=0.997874

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.3432274941491545
novelty_score=0.25091979976527723
surprise_score=0.0
