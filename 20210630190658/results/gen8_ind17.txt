IMAGE
name=gen8_ind17
content=dataset/output_images/gen7_ind14.jpg
style=dataset/output_images/gen7_ind5.jpg

SCORES
rank=2
ColorDistributionMeasure=0.531888
ComplexityMeasure=0.852374
LivelinessMeasure=0.971133
LuminanceDistributionMeasure=0.764336
LuminanceRedundancyMeasure=0.997874

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.3358065172500378
novelty_score=0.2157645273759156
surprise_score=0.2
