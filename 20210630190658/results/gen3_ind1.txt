IMAGE
name=gen3_ind1
content=dataset/output_images/gen2_ind2.jpg
style=dataset/output_images/gen2_ind3.jpg

SCORES
rank=1
ColorDistributionMeasure=0.625707
ComplexityMeasure=0.959628
LivelinessMeasure=0.973496
LuminanceDistributionMeasure=0.862591
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.5042118158540075
novelty_score=0.19269451432677584
surprise_score=0.2
