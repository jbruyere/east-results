IMAGE
name=gen10_ind7
content=dataset/output_images/gen9_ind13.jpg
style=dataset/output_images/gen9_ind15.jpg

SCORES
rank=1
ColorDistributionMeasure=-0.013561
ComplexityMeasure=0.83387
LivelinessMeasure=0.97001
LuminanceDistributionMeasure=0.764598
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[content_weight=1250,style_weight=0.15]

METRICS
valuable=0
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=-0.00838093967253179
novelty_score=0.5513698423571612
surprise_score=0.0
