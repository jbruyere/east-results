IMAGE
name=gen3_ind18
content=dataset/output_images/gen2_ind8.jpg
style=dataset/output_images/gen2_ind17.jpg

SCORES
rank=1
ColorDistributionMeasure=0.558803
ComplexityMeasure=0.863552
LivelinessMeasure=0.974437
LuminanceDistributionMeasure=0.769115
LuminanceRedundancyMeasure=0.999294

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.36139783843417783
novelty_score=0.22498248940612534
surprise_score=0.0
