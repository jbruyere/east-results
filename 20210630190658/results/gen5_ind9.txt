IMAGE
name=gen5_ind9
content=dataset/output_images/gen4_ind19.jpg
style=dataset/output_images/gen4_ind3.jpg

SCORES
rank=1
ColorDistributionMeasure=0.555628
ComplexityMeasure=0.892515
LivelinessMeasure=0.968483
LuminanceDistributionMeasure=0.855695
LuminanceRedundancyMeasure=0.999294

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.4106803495361022
novelty_score=0.1860552388425348
surprise_score=0.0
