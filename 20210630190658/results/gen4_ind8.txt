IMAGE
name=gen4_ind8
content=dataset/output_images/gen3_ind17.jpg
style=dataset/output_images/gen3_ind9.jpg

SCORES
rank=1
ColorDistributionMeasure=0.502976
ComplexityMeasure=0.901154
LivelinessMeasure=0.96932
LuminanceDistributionMeasure=0.835255
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[content_weight=1500,style_weight=0.15]

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.3667125854587663
novelty_score=0.21437057399886905
surprise_score=0.0
