IMAGE
name=gen10_ind3
content=dataset/output_images/gen9_ind13.jpg
style=dataset/output_images/gen9_ind19.jpg

SCORES
rank=2
ColorDistributionMeasure=0.217152
ComplexityMeasure=0.837757
LivelinessMeasure=0.9683
LuminanceDistributionMeasure=0.753482
LuminanceRedundancyMeasure=0.99716

MUTATIONS

METRICS
valuable=0
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.13235171146414632
novelty_score=0.35968886101181113
surprise_score=0.0
