IMAGE
name=gen7_ind1
content=dataset/output_images/gen6_ind0.jpg
style=dataset/output_images/gen6_ind9.jpg

SCORES
rank=2
ColorDistributionMeasure=-0.350421
ComplexityMeasure=0.822686
LivelinessMeasure=0.969593
LuminanceDistributionMeasure=0.763856
LuminanceRedundancyMeasure=0.998586

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-0.21321152192255277
novelty_score=0.927966412467301
surprise_score=0.8
