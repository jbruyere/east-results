IMAGE
name=gen6_ind18
content=dataset/output_images/gen5_ind1.jpg
style=dataset/output_images/gen5_ind14.jpg

SCORES
rank=1
ColorDistributionMeasure=0.413279
ComplexityMeasure=0.851275
LivelinessMeasure=0.97207
LuminanceDistributionMeasure=0.780241
LuminanceRedundancyMeasure=0.997874

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/33809.jpg]
mutation_2=[color_filter=bright]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.2662657046383194
novelty_score=0.24356280115273823
surprise_score=0.4
