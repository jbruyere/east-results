IMAGE
name=gen7_ind12
content=dataset/output_images/gen6_ind17.jpg
style=dataset/output_images/gen6_ind7.jpg

SCORES
rank=2
ColorDistributionMeasure=0.327382
ComplexityMeasure=0.843819
LivelinessMeasure=0.972142
LuminanceDistributionMeasure=0.731729
LuminanceRedundancyMeasure=0.999294

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.19637099982878733
novelty_score=0.30276620375680313
surprise_score=0.2
