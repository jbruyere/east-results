IMAGE
name=gen1_ind17
content=dataset/input_images/prisoners-exercising-prisoners-round-1890.jpg
style=dataset/input_images/forest.jpg

SCORES
rank=1
ColorDistributionMeasure=0.298541
ComplexityMeasure=0.917347
LivelinessMeasure=0.972391
LuminanceDistributionMeasure=0.798527
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.2126513597204546
novelty_score=0.4001710157969906
surprise_score=0.2
