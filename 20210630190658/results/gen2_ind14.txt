IMAGE
name=gen2_ind14
content=dataset/output_images/gen1_ind14.jpg
style=dataset/output_images/gen1_ind5.jpg

SCORES
rank=2
ColorDistributionMeasure=0.401451
ComplexityMeasure=0.936097
LivelinessMeasure=0.962805
LuminanceDistributionMeasure=0.853628
LuminanceRedundancyMeasure=0.993546

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/75034.jpg]
mutation_2=[color_filter=dark]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.30686571268045937
novelty_score=0.3037965021246414
surprise_score=0.6000000000000001
