IMAGE
name=gen3_ind9
content=dataset/output_images/gen2_ind2.jpg
style=dataset/output_images/gen2_ind13.jpg

SCORES
rank=1
ColorDistributionMeasure=0.399096
ComplexityMeasure=0.904993
LivelinessMeasure=0.968113
LuminanceDistributionMeasure=0.922666
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[content_weight=1250,style_weight=0.25]
mutation_2=[style_transfer=dataset/tmp_images/dataset/tmp_images/35614.jpg]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.3226213946401052
novelty_score=0.3159761902734226
surprise_score=0.4
