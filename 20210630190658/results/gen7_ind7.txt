IMAGE
name=gen7_ind7
content=dataset/output_images/gen6_ind13.jpg
style=dataset/output_images/gen6_ind17.jpg

SCORES
rank=1
ColorDistributionMeasure=-0.120511
ComplexityMeasure=0.834691
LivelinessMeasure=0.972073
LuminanceDistributionMeasure=0.752461
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[content_weight=1000,style_weight=0.25]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-0.07357585149174845
novelty_score=0.7000657294002642
surprise_score=0.4
