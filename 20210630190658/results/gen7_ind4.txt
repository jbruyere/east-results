IMAGE
name=gen7_ind4
content=dataset/output_images/gen6_ind7.jpg
style=dataset/output_images/gen6_ind17.jpg

SCORES
rank=1
ColorDistributionMeasure=0.67152
ComplexityMeasure=0.868305
LivelinessMeasure=0.969242
LuminanceDistributionMeasure=0.785555
LuminanceRedundancyMeasure=0.998586

MUTATIONS
mutation_1=[color_filter=cool]
mutation_2=[style_transfer=dataset/tmp_images/dataset/tmp_images/26401.jpg]
mutation_3=[content_weight=1500,style_weight=0.1]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.443328395483716
novelty_score=0.1994345725956934
surprise_score=0.4
