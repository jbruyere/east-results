IMAGE
name=gen10_ind17
content=dataset/output_images/gen9_ind3.jpg
style=dataset/output_images/gen9_ind6.jpg

SCORES
rank=1
ColorDistributionMeasure=-1.496523
ComplexityMeasure=0.818448
LivelinessMeasure=0.969977
LuminanceDistributionMeasure=0.76897
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-0.9135773442639192
novelty_score=2.012322016813693
surprise_score=0.8
