IMAGE
name=gen2_ind11
content=dataset/output_images/gen1_ind14.jpg
style=dataset/output_images/gen1_ind12.jpg

SCORES
rank=1
ColorDistributionMeasure=0.66098
ComplexityMeasure=0.973393
LivelinessMeasure=0.974807
LuminanceDistributionMeasure=0.801663
LuminanceRedundancyMeasure=0.999294

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.5024354755152288
novelty_score=0.17966423151124516
surprise_score=0.2
