IMAGE
name=gen3_ind3
content=dataset/output_images/gen2_ind12.jpg
style=dataset/output_images/gen2_ind13.jpg

SCORES
rank=1
ColorDistributionMeasure=0.705204
ComplexityMeasure=0.999804
LivelinessMeasure=0.96714
LuminanceDistributionMeasure=0.79785
LuminanceRedundancyMeasure=0.993546

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.5405404653935784
novelty_score=0.18394630533502052
surprise_score=0.2
