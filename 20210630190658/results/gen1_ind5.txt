IMAGE
name=gen1_ind5
content=dataset/input_images/the-magpie-1869.jpg
style=dataset/input_images/still-life-with-goblet.jpg

SCORES
rank=1
ColorDistributionMeasure=0.825836
ComplexityMeasure=0.992415
LivelinessMeasure=0.956555
LuminanceDistributionMeasure=0.757533
LuminanceRedundancyMeasure=0.997874

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.5926173203284713
novelty_score=0.20488437529935763
surprise_score=0.8
