IMAGE
name=gen8_ind9
content=dataset/output_images/gen7_ind6.jpg
style=dataset/output_images/gen7_ind18.jpg

SCORES
rank=2
ColorDistributionMeasure=-0.150567
ComplexityMeasure=0.829626
LivelinessMeasure=0.971065
LuminanceDistributionMeasure=0.737776
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[content_weight=500,style_weight=0.005]

METRICS
valuable=0
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=-0.0894289755744335
novelty_score=0.709769654755114
surprise_score=0.0
