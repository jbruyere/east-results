IMAGE
name=gen4_ind19
content=dataset/output_images/gen3_ind14.jpg
style=dataset/output_images/gen3_ind17.jpg

SCORES
rank=1
ColorDistributionMeasure=0.435837
ComplexityMeasure=0.900027
LivelinessMeasure=0.970802
LuminanceDistributionMeasure=0.836174
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/87923.jpg]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.31820004465376395
novelty_score=0.2529200479958641
surprise_score=0.2
