IMAGE
name=gen10_ind9
content=dataset/output_images/gen9_ind10.jpg
style=dataset/output_images/gen9_ind6.jpg

SCORES
rank=2
ColorDistributionMeasure=-1.193611
ComplexityMeasure=0.817521
LivelinessMeasure=0.970286
LuminanceDistributionMeasure=0.7543
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-0.7141765774039844
novelty_score=1.7102136511956787
surprise_score=0.4
