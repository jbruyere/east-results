IMAGE
name=gen7_ind10
content=dataset/output_images/gen6_ind1.jpg
style=dataset/output_images/gen6_ind19.jpg

SCORES
rank=1
ColorDistributionMeasure=0.349179
ComplexityMeasure=0.845591
LivelinessMeasure=0.972129
LuminanceDistributionMeasure=0.759518
LuminanceRedundancyMeasure=0.998586

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.21769873806256923
novelty_score=0.2811816484951772
surprise_score=0.2
