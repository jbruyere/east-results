IMAGE
name=gen5_ind11
content=dataset/output_images/gen4_ind19.jpg
style=dataset/output_images/gen4_ind14.jpg

SCORES
rank=1
ColorDistributionMeasure=0.426864
ComplexityMeasure=0.858573
LivelinessMeasure=0.970741
LuminanceDistributionMeasure=0.819671
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[color_filter=bright]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.29161489257376044
novelty_score=0.24741338491678402
surprise_score=0.4
