IMAGE
name=gen4_ind1
content=dataset/output_images/gen3_ind8.jpg
style=dataset/output_images/gen3_ind10.jpg

SCORES
rank=1
ColorDistributionMeasure=0.689705
ComplexityMeasure=0.883734
LivelinessMeasure=0.974467
LuminanceDistributionMeasure=0.739398
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/35222.jpg]

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.43885760246478933
novelty_score=0.19048340670579392
surprise_score=0.0
