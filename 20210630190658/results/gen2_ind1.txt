IMAGE
name=gen2_ind1
content=dataset/output_images/gen1_ind6.jpg
style=dataset/output_images/gen1_ind14.jpg

SCORES
rank=1
ColorDistributionMeasure=0.641568
ComplexityMeasure=0.981613
LivelinessMeasure=0.967066
LuminanceDistributionMeasure=0.758466
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.46192899923633535
novelty_score=0.18778038350005366
surprise_score=0.2
