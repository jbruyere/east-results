IMAGE
name=gen10_ind10
content=dataset/output_images/gen9_ind11.jpg
style=dataset/output_images/gen9_ind15.jpg

SCORES
rank=2
ColorDistributionMeasure=0.283918
ComplexityMeasure=0.840294
LivelinessMeasure=0.969873
LuminanceDistributionMeasure=0.738327
LuminanceRedundancyMeasure=0.993546

MUTATIONS
mutation_1=[color_filter=dark]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.16973671336812
novelty_score=0.3185741972049773
surprise_score=0.2
