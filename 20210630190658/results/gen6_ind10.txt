IMAGE
name=gen6_ind10
content=dataset/output_images/gen5_ind11.jpg
style=dataset/output_images/gen5_ind14.jpg

SCORES
rank=2
ColorDistributionMeasure=0.221166
ComplexityMeasure=0.842812
LivelinessMeasure=0.968313
LuminanceDistributionMeasure=0.777058
LuminanceRedundancyMeasure=0.998586

MUTATIONS
mutation_1=[color_filter=dark]
mutation_2=[content_weight=500,style_weight=0.2]
mutation_3=[style_transfer=dataset/tmp_images/dataset/tmp_images/94483.jpg]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.14005665356168717
novelty_score=0.38751531828489016
surprise_score=0.8
