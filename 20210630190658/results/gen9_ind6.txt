IMAGE
name=gen9_ind6
content=dataset/output_images/gen8_ind19.jpg
style=dataset/output_images/gen8_ind4.jpg

SCORES
rank=1
ColorDistributionMeasure=-0.263855
ComplexityMeasure=0.828344
LivelinessMeasure=0.96971
LuminanceDistributionMeasure=0.785484
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-0.16647739691683355
novelty_score=0.8169202737452277
surprise_score=0.4
