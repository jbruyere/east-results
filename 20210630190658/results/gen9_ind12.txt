IMAGE
name=gen9_ind12
content=dataset/output_images/gen8_ind5.jpg
style=dataset/output_images/gen8_ind3.jpg

SCORES
rank=1
ColorDistributionMeasure=0.123953
ComplexityMeasure=0.838363
LivelinessMeasure=0.971658
LuminanceDistributionMeasure=0.740799
LuminanceRedundancyMeasure=0.999294

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.07474742625158706
novelty_score=0.47733367004583377
surprise_score=0.2
