IMAGE
name=gen4_ind2
content=dataset/output_images/gen3_ind13.jpg
style=dataset/output_images/gen3_ind18.jpg

SCORES
rank=1
ColorDistributionMeasure=0.421852
ComplexityMeasure=0.852989
LivelinessMeasure=0.973108
LuminanceDistributionMeasure=0.814199
LuminanceRedundancyMeasure=0.999294

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.2848973636599436
novelty_score=0.2741129983524415
surprise_score=0.2
