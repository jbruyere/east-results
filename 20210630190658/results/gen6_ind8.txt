IMAGE
name=gen6_ind8
content=dataset/output_images/gen5_ind2.jpg
style=dataset/output_images/gen5_ind18.jpg

SCORES
rank=2
ColorDistributionMeasure=0.127812
ComplexityMeasure=0.83895
LivelinessMeasure=0.973584
LuminanceDistributionMeasure=0.707122
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[color_filter=bright]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.07382024570614179
novelty_score=0.4878246548809805
surprise_score=0.6000000000000001
