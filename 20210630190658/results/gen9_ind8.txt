IMAGE
name=gen9_ind8
content=dataset/output_images/gen8_ind16.jpg
style=dataset/output_images/gen8_ind19.jpg

SCORES
rank=1
ColorDistributionMeasure=0.410687
ComplexityMeasure=0.845963
LivelinessMeasure=0.970859
LuminanceDistributionMeasure=0.768637
LuminanceRedundancyMeasure=0.999294

MUTATIONS

METRICS
valuable=0
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.2590795007757152
novelty_score=0.2935052915785262
surprise_score=0.0
