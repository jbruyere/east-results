IMAGE
name=gen8_ind14
content=dataset/output_images/gen7_ind10.jpg
style=dataset/output_images/gen7_ind3.jpg

SCORES
rank=1
ColorDistributionMeasure=0.12018
ComplexityMeasure=0.835577
LivelinessMeasure=0.972834
LuminanceDistributionMeasure=0.746606
LuminanceRedundancyMeasure=0.998586

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.07283403426733186
novelty_score=0.46028592759674464
surprise_score=0.4
