IMAGE
name=gen9_ind18
content=dataset/output_images/gen8_ind4.jpg
style=dataset/output_images/gen8_ind19.jpg

SCORES
rank=1
ColorDistributionMeasure=0.356092
ComplexityMeasure=0.84496
LivelinessMeasure=0.969107
LuminanceDistributionMeasure=0.788294
LuminanceRedundancyMeasure=0.999294

MUTATIONS

METRICS
valuable=0
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.22969503004548653
novelty_score=0.3159646119222628
surprise_score=0.0
