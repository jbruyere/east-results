IMAGE
name=gen7_ind17
content=dataset/output_images/gen6_ind7.jpg
style=dataset/output_images/gen6_ind1.jpg

SCORES
rank=1
ColorDistributionMeasure=0.478366
ComplexityMeasure=0.858302
LivelinessMeasure=0.96751
LuminanceDistributionMeasure=0.808148
LuminanceRedundancyMeasure=0.989111

MUTATIONS
mutation_1=[content_weight=500,style_weight=0.1]
mutation_2=[color_filter=dark]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.3175351635678134
novelty_score=0.21011012027330297
surprise_score=0.4
