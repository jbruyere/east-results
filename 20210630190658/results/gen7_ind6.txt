IMAGE
name=gen7_ind6
content=dataset/output_images/gen6_ind9.jpg
style=dataset/output_images/gen6_ind0.jpg

SCORES
rank=1
ColorDistributionMeasure=-0.583635
ComplexityMeasure=0.828799
LivelinessMeasure=0.972129
LuminanceDistributionMeasure=0.742152
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-0.3489854396294491
novelty_score=1.1601666963453718
surprise_score=0.6000000000000001
