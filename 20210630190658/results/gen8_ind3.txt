IMAGE
name=gen8_ind3
content=dataset/output_images/gen7_ind18.jpg
style=dataset/output_images/gen7_ind4.jpg

SCORES
rank=1
ColorDistributionMeasure=0.556555
ComplexityMeasure=0.863025
LivelinessMeasure=0.971495
LuminanceDistributionMeasure=0.772692
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/90628.jpg]

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.36030619608290443
novelty_score=0.20932356341715536
surprise_score=0.0
