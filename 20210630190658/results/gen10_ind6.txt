IMAGE
name=gen10_ind6
content=dataset/output_images/gen9_ind11.jpg
style=dataset/output_images/gen9_ind13.jpg

SCORES
rank=3
ColorDistributionMeasure=0.369418
ComplexityMeasure=0.838869
LivelinessMeasure=0.967655
LuminanceDistributionMeasure=0.746442
LuminanceRedundancyMeasure=0.994275

MUTATIONS
mutation_1=[content_weight=1000,style_weight=0.1]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.2225539623289728
novelty_score=0.2727366745885506
surprise_score=0.2
