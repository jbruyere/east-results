IMAGE
name=gen4_ind9
content=dataset/output_images/gen3_ind17.jpg
style=dataset/output_images/gen3_ind8.jpg

SCORES
rank=1
ColorDistributionMeasure=0.652863
ComplexityMeasure=0.874711
LivelinessMeasure=0.972581
LuminanceDistributionMeasure=0.817211
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.4538858349039642
novelty_score=0.1805767944352596
surprise_score=0.2
