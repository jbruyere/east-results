IMAGE
name=gen1_ind15
content=dataset/input_images/double-twist-1980.jpg
style=dataset/input_images/coast-near-antibes.jpg

SCORES
rank=2
ColorDistributionMeasure=0.531322
ComplexityMeasure=0.89498
LivelinessMeasure=0.965614
LuminanceDistributionMeasure=0.745314
LuminanceRedundancyMeasure=0.996443

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/25095.jpg]
mutation_2=[color_filter=bright]
mutation_3=[content_weight=500,style_weight=0.1]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.3410094564895764
novelty_score=0.21633969100144562
surprise_score=0.4
