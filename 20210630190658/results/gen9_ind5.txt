IMAGE
name=gen9_ind5
content=dataset/output_images/gen8_ind16.jpg
style=dataset/output_images/gen8_ind1.jpg

SCORES
rank=2
ColorDistributionMeasure=0.302911
ComplexityMeasure=0.838365
LivelinessMeasure=0.96946
LuminanceDistributionMeasure=0.742049
LuminanceRedundancyMeasure=0.998586

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/55470.jpg]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.18242994860720424
novelty_score=0.35249704608381655
surprise_score=0.4
