IMAGE
name=gen5_ind0
content=dataset/output_images/gen4_ind13.jpg
style=dataset/output_images/gen4_ind14.jpg

SCORES
rank=1
ColorDistributionMeasure=0.336159
ComplexityMeasure=0.849708
LivelinessMeasure=0.972131
LuminanceDistributionMeasure=0.775137
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[color_filter=warm]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.2152373867374297
novelty_score=0.31924114724411595
surprise_score=0.4
