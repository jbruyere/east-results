IMAGE
name=gen4_ind6
content=dataset/output_images/gen3_ind11.jpg
style=dataset/output_images/gen3_ind10.jpg

SCORES
rank=1
ColorDistributionMeasure=0.546598
ComplexityMeasure=0.861244
LivelinessMeasure=0.975103
LuminanceDistributionMeasure=0.748086
LuminanceRedundancyMeasure=0.999294

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.34315438055211983
novelty_score=0.2161556451578592
surprise_score=0.0
