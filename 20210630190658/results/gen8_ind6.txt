IMAGE
name=gen8_ind6
content=dataset/output_images/gen7_ind14.jpg
style=dataset/output_images/gen7_ind2.jpg

SCORES
rank=1
ColorDistributionMeasure=-0.036422
ComplexityMeasure=0.830401
LivelinessMeasure=0.971818
LuminanceDistributionMeasure=0.77148
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-0.022675729317898335
novelty_score=0.6004252095009227
surprise_score=0.4
