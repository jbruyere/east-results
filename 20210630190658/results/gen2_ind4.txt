IMAGE
name=gen2_ind4
content=dataset/output_images/gen1_ind13.jpg
style=dataset/output_images/gen1_ind9.jpg

SCORES
rank=2
ColorDistributionMeasure=0.643738
ComplexityMeasure=0.899241
LivelinessMeasure=0.970008
LuminanceDistributionMeasure=0.823402
LuminanceRedundancyMeasure=0.99716

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.46103864355702706
novelty_score=0.19373298997441485
surprise_score=0.0
