IMAGE
name=gen9_ind17
content=dataset/output_images/gen8_ind1.jpg
style=dataset/output_images/gen8_ind16.jpg

SCORES
rank=2
ColorDistributionMeasure=-3.099208
ComplexityMeasure=0.808545
LivelinessMeasure=0.971347
LuminanceDistributionMeasure=0.741869
LuminanceRedundancyMeasure=0.998586

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=-1.803192200984062
novelty_score=3.5779563442062483
surprise_score=0.4
