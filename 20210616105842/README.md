fix tournament selection + multiprocessing (but mistakes with multiprocessing in results & objective computation)
RUN WITH RANK SELECTION


CORRELATION
CORRELATION ColorDistributionMeasure X ComplexityMeasure
	    PEARSONR -0.17448 0.12164
	    	     SPEARMAM -0.29271 0.00842
		     	      KENDALLT -0.11456 0.13258

CORRELATION ColorDistributionMeasure X LivelinessMeasure
	    PEARSONR 0.06983 0.53822
	    	     SPEARMAM 0.20271 0.07134
		     	      KENDALLT 0.14085 0.06448

CORRELATION ColorDistributionMeasure X LuminanceDistributionMeasure
	    PEARSONR 0.23843 0.03318
	    	     SPEARMAM 0.25591 0.02195
		     	      KENDALLT 0.17532 0.02135

CORRELATION ColorDistributionMeasure X LuminanceRedundancyMeasure
	    PEARSONR 0.22584 0.04398
	    	     SPEARMAM 0.44074 4e-05
		     	      KENDALLT 0.3109 9e-05

CORRELATION ColorDistributionMeasure X value_score
	    PEARSONR 0.96222 0.0
	    	     SPEARMAM 0.9609 0.0
		     	      KENDALLT 0.84241 0.0

CORRELATION ColorDistributionMeasure X novelty_score
	    PEARSONR -0.86483 0.0
	    	     SPEARMAM -0.77853 0.0
		     	      KENDALLT -0.60823 0.0

CORRELATION ColorDistributionMeasure X surprise_score
	    PEARSONR -0.00625 0.95611
	    	     SPEARMAM -0.02327 0.83769
		     	      KENDALLT -0.01482 0.85955

CORRELATION ComplexityMeasure X LivelinessMeasure
	    PEARSONR -0.43545 5e-05
	    	     SPEARMAM -0.34215 0.00189
		     	      KENDALLT -0.22883 0.00267

CORRELATION ComplexityMeasure X LuminanceDistributionMeasure
	    PEARSONR -0.31265 0.00475
	    	     SPEARMAM -0.27325 0.01419
		     	      KENDALLT -0.18608 0.01457

CORRELATION ComplexityMeasure X LuminanceRedundancyMeasure
	    PEARSONR -0.56833 0.0
	    	     SPEARMAM -0.50675 0.0
		     	      KENDALLT -0.36483 0.0

CORRELATION ComplexityMeasure X value_score
	    PEARSONR -0.13536 0.23125
	    	     SPEARMAM -0.21263 0.05827
		     	      KENDALLT -0.07342 0.33511

CORRELATION ComplexityMeasure X novelty_score
	    PEARSONR 0.14813 0.18975
	    	     SPEARMAM 0.29885 0.00709
		     	      KENDALLT 0.15823 0.03777

CORRELATION ComplexityMeasure X surprise_score
	    PEARSONR -0.10301 0.36321
	    	     SPEARMAM -0.12437 0.27171
		     	      KENDALLT -0.08785 0.29429

CORRELATION LivelinessMeasure X LuminanceDistributionMeasure
	    PEARSONR 0.33142 0.00267
	    	     SPEARMAM 0.29454 0.008
		     	      KENDALLT 0.19592 0.01012

CORRELATION LivelinessMeasure X LuminanceRedundancyMeasure
	    PEARSONR 0.38252 0.00046
	    	     SPEARMAM 0.6165 0.0
		     	      KENDALLT 0.45444 0.0

CORRELATION LivelinessMeasure X value_score
	    PEARSONR 0.128 0.25784
	    	     SPEARMAM 0.22317 0.0466
		     	      KENDALLT 0.14781 0.05235

CORRELATION LivelinessMeasure X novelty_score
	    PEARSONR -0.0674 0.55248
	    	     SPEARMAM -0.18599 0.09856
		     	      KENDALLT -0.12755 0.09406

CORRELATION LivelinessMeasure X surprise_score
	    PEARSONR 0.22778 0.04215
	    	     SPEARMAM 0.29529 0.00783
		     	      KENDALLT 0.20575 0.01406

CORRELATION LuminanceDistributionMeasure X LuminanceRedundancyMeasure
	    PEARSONR 0.39207 0.00032
	    	     SPEARMAM 0.56288 0.0
		     	      KENDALLT 0.41276 0.0

CORRELATION LuminanceDistributionMeasure X value_score
	    PEARSONR 0.46403 1e-05
	    	     SPEARMAM 0.43638 5e-05
		     	      KENDALLT 0.31013 5e-05

CORRELATION LuminanceDistributionMeasure X novelty_score
	    PEARSONR -0.09677 0.39317
	    	     SPEARMAM -0.15345 0.17417
		     	      KENDALLT -0.1 0.18923

CORRELATION LuminanceDistributionMeasure X surprise_score
	    PEARSONR 0.094 0.4069
	    	     SPEARMAM 0.07216 0.52473
		     	      KENDALLT 0.05315 0.5258

CORRELATION LuminanceRedundancyMeasure X value_score
	    PEARSONR 0.26647 0.01689
	    	     SPEARMAM 0.4902 0.0
		     	      KENDALLT 0.34685 1e-05

CORRELATION LuminanceRedundancyMeasure X novelty_score
	    PEARSONR -0.1658 0.14162
	    	     SPEARMAM -0.35541 0.00122
		     	      KENDALLT -0.23967 0.00253

CORRELATION LuminanceRedundancyMeasure X surprise_score
	    PEARSONR 0.20083 0.07406
	    	     SPEARMAM 0.19501 0.08302
		     	      KENDALLT 0.14756 0.09102

CORRELATION value_score X novelty_score
	    PEARSONR -0.79624 0.0
	    	     SPEARMAM -0.74571 0.0
		     	      KENDALLT -0.57848 0.0

CORRELATION value_score X surprise_score
	    PEARSONR 0.00914 0.93584
	    	     SPEARMAM -0.00027 0.99812
		     	      KENDALLT 0.00253 0.9759

CORRELATION novelty_score X surprise_score
	    PEARSONR 0.13744 0.22408
	    	     SPEARMAM 0.25521 0.02233
		     	      KENDALLT 0.18475 0.02743
			      