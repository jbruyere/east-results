IMAGE
name=gen1_ind8
content=dataset/input_images/the-starry-night.jpg
style=dataset/input_images/linhas-de-sombra.jpg

SCORES
rank=0
ColorDistributionMeasure=0.692655
ComplexityMeasure=0.893485
LivelinessMeasure=0.972921
LuminanceDistributionMeasure=0.729846
LuminanceRedundancyMeasure=0.999294

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.439143
novelty_score=0.192206
surprise_score=1.0
