IMAGE
name=gen4_ind10
content=dataset/output_images/gen3_ind0.jpg
style=dataset/output_images/gen3_ind6.jpg

SCORES
rank=0
ColorDistributionMeasure=0.751829
ComplexityMeasure=0.891902
LivelinessMeasure=0.971328
LuminanceDistributionMeasure=0.846486
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[color_filter=cool]
mutation_2=[content_weight=500,style_weight=0.1]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.550954
novelty_score=0.215512
surprise_score=0.4
