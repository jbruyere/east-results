IMAGE
name=gen2_ind12
content=dataset/output_images/gen1_ind19.jpg
style=dataset/output_images/gen1_ind5.jpg

SCORES
rank=0
ColorDistributionMeasure=0.359272
ComplexityMeasure=0.927344
LivelinessMeasure=0.958086
LuminanceDistributionMeasure=0.686307
LuminanceRedundancyMeasure=0.996443

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.218293
novelty_score=0.331782
surprise_score=0.2
