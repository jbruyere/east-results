IMAGE
name=gen3_ind0
content=dataset/output_images/gen2_ind19.jpg
style=dataset/output_images/gen2_ind8.jpg

SCORES
rank=0
ColorDistributionMeasure=0.458299
ComplexityMeasure=0.937683
LivelinessMeasure=0.963795
LuminanceDistributionMeasure=0.713148
LuminanceRedundancyMeasure=0.995

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/75082.jpg]
mutation_2=[color_filter=dark]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.293895
novelty_score=0.220682
surprise_score=0.2
