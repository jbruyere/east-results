IMAGE
name=gen1_ind3
content=dataset/input_images/seaweed-gatherers-at-omari.jpg
style=dataset/input_images/the-great-wave-off-kanagawa.jpg

SCORES
rank=0
ColorDistributionMeasure=0.48048
ComplexityMeasure=0.873691
LivelinessMeasure=0.96634
LuminanceDistributionMeasure=0.850796
LuminanceRedundancyMeasure=0.999294

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.344891
novelty_score=0.277056
surprise_score=0.6
