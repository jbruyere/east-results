IMAGE
name=gen1_ind17
content=dataset/input_images/the-port-of-le-havre-1903.jpg
style=dataset/input_images/marguerite-1975.jpg

SCORES
rank=0
ColorDistributionMeasure=0.454843
ComplexityMeasure=0.973927
LivelinessMeasure=0.963309
LuminanceDistributionMeasure=0.672245
LuminanceRedundancyMeasure=0.986093

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.282878
novelty_score=0.2973
surprise_score=0.4
