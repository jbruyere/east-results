IMAGE
name=gen4_ind5
content=dataset/output_images/gen3_ind4.jpg
style=dataset/output_images/gen3_ind1.jpg

SCORES
rank=0
ColorDistributionMeasure=0.392517
ComplexityMeasure=0.936879
LivelinessMeasure=0.968791
LuminanceDistributionMeasure=0.721103
LuminanceRedundancyMeasure=0.99716

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.256174
novelty_score=0.247639
surprise_score=0.0
