IMAGE
name=gen4_ind4
content=dataset/output_images/gen3_ind0.jpg
style=dataset/output_images/gen3_ind9.jpg

SCORES
rank=0
ColorDistributionMeasure=0.370461
ComplexityMeasure=0.933084
LivelinessMeasure=0.956033
LuminanceDistributionMeasure=0.686707
LuminanceRedundancyMeasure=0.983796

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.223261
novelty_score=0.276745
surprise_score=0.6
