IMAGE
name=gen2_ind19
content=dataset/output_images/gen1_ind14.jpg
style=dataset/output_images/gen1_ind15.jpg

SCORES
rank=0
ColorDistributionMeasure=0.375442
ComplexityMeasure=0.943276
LivelinessMeasure=0.963381
LuminanceDistributionMeasure=0.77959
LuminanceRedundancyMeasure=0.997874

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/28635.jpg]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.265413
novelty_score=0.29819
surprise_score=0.2
