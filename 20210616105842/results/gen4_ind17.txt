IMAGE
name=gen4_ind17
content=dataset/output_images/gen3_ind6.jpg
style=dataset/output_images/gen3_ind7.jpg

SCORES
rank=0
ColorDistributionMeasure=0.683956
ComplexityMeasure=0.884457
LivelinessMeasure=0.974562
LuminanceDistributionMeasure=0.802342
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.473014
novelty_score=0.178612
surprise_score=0.2
