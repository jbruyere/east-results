IMAGE
name=gen4_ind13
content=dataset/output_images/gen3_ind10.jpg
style=dataset/output_images/gen3_ind7.jpg

SCORES
rank=0
ColorDistributionMeasure=0.754015
ComplexityMeasure=0.892123
LivelinessMeasure=0.974948
LuminanceDistributionMeasure=0.848909
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.556733
novelty_score=0.217536
surprise_score=0.6
