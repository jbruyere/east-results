IMAGE
name=gen2_ind1
content=dataset/output_images/gen1_ind19.jpg
style=dataset/output_images/gen1_ind15.jpg

SCORES
rank=0
ColorDistributionMeasure=0.284624
ComplexityMeasure=0.89615
LivelinessMeasure=0.969929
LuminanceDistributionMeasure=0.69726
LuminanceRedundancyMeasure=0.996443

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/62032.jpg]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.171886
novelty_score=0.396372
surprise_score=0.6
