IMAGE
name=gen1_ind18
content=dataset/input_images/self-portrait-with-easel.jpg
style=dataset/input_images/napoleoncrossing-the-alps-at-the-st-bernard-pass-20th-may-1800-1801.jpg

SCORES
rank=0
ColorDistributionMeasure=0.322498
ComplexityMeasure=0.946029
LivelinessMeasure=0.968789
LuminanceDistributionMeasure=0.74559
LuminanceRedundancyMeasure=0.99208

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.218629
novelty_score=0.380164
surprise_score=0.6
