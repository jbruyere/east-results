IMAGE
name=gen1_ind16
content=dataset/input_images/costume-design-for-dance-of-the-seven-veils-1917.jpg
style=dataset/input_images/still-life-with-goblet.jpg

SCORES
rank=0
ColorDistributionMeasure=0.25727
ComplexityMeasure=0.914015
LivelinessMeasure=0.971989
LuminanceDistributionMeasure=0.77152
LuminanceRedundancyMeasure=0.997874

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.175965
novelty_score=0.428527
surprise_score=0.8
