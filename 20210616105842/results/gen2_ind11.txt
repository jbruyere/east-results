IMAGE
name=gen2_ind11
content=dataset/output_images/gen1_ind5.jpg
style=dataset/output_images/gen1_ind10.jpg

SCORES
rank=0
ColorDistributionMeasure=0.2941
ComplexityMeasure=0.921083
LivelinessMeasure=0.971328
LuminanceDistributionMeasure=0.707695
LuminanceRedundancyMeasure=0.986852

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/1211.jpg]
mutation_2=[color_filter=dark]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.183763
novelty_score=0.382367
surprise_score=0.4
