IMAGE
name=gen1_ind19
content=dataset/input_images/the-harvest-1890.jpg
style=dataset/input_images/cash-register-1917.jpg

SCORES
rank=0
ColorDistributionMeasure=0.588341
ComplexityMeasure=0.913399
LivelinessMeasure=0.969021
LuminanceDistributionMeasure=0.919901
LuminanceRedundancyMeasure=0.999294

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.478693
novelty_score=0.241395
surprise_score=0.6
