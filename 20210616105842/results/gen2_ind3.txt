IMAGE
name=gen2_ind3
content=dataset/output_images/gen1_ind10.jpg
style=dataset/output_images/gen1_ind13.jpg

SCORES
rank=0
ColorDistributionMeasure=0.604455
ComplexityMeasure=0.867607
LivelinessMeasure=0.969506
LuminanceDistributionMeasure=0.796349
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.404894
novelty_score=0.190227
surprise_score=0.0
