IMAGE
name=gen4_ind15
content=dataset/output_images/gen3_ind0.jpg
style=dataset/output_images/gen3_ind10.jpg

SCORES
rank=0
ColorDistributionMeasure=0.528415
ComplexityMeasure=0.856473
LivelinessMeasure=0.972309
LuminanceDistributionMeasure=0.829618
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/18872.jpg]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.364808
novelty_score=0.198251
surprise_score=0.4
