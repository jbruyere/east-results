IMAGE
name=gen4_ind9
content=dataset/output_images/gen3_ind6.jpg
style=dataset/output_images/gen3_ind10.jpg

SCORES
rank=0
ColorDistributionMeasure=0.70036
ComplexityMeasure=0.885373
LivelinessMeasure=0.972874
LuminanceDistributionMeasure=0.854077
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.51523
novelty_score=0.196482
surprise_score=0.2
