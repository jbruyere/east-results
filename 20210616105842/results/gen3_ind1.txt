IMAGE
name=gen3_ind1
content=dataset/output_images/gen2_ind14.jpg
style=dataset/output_images/gen2_ind19.jpg

SCORES
rank=0
ColorDistributionMeasure=0.335528
ComplexityMeasure=0.917438
LivelinessMeasure=0.97155
LuminanceDistributionMeasure=0.720446
LuminanceRedundancyMeasure=0.995

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.214385
novelty_score=0.312757
surprise_score=0.6
