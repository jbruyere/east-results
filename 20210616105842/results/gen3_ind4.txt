IMAGE
name=gen3_ind4
content=dataset/output_images/gen2_ind4.jpg
style=dataset/output_images/gen2_ind8.jpg

SCORES
rank=0
ColorDistributionMeasure=0.604593
ComplexityMeasure=0.974818
LivelinessMeasure=0.965871
LuminanceDistributionMeasure=0.691923
LuminanceRedundancyMeasure=0.975919

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/25879.jpg]
mutation_2=[content_weight=1500,style_weight=0.2]
mutation_3=[color_filter=cool]

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.384395
novelty_score=0.186073
surprise_score=0.0
