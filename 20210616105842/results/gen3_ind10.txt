IMAGE
name=gen3_ind10
content=dataset/output_images/gen2_ind17.jpg
style=dataset/output_images/gen2_ind15.jpg

SCORES
rank=0
ColorDistributionMeasure=0.633334
ComplexityMeasure=0.901495
LivelinessMeasure=0.975951
LuminanceDistributionMeasure=0.777658
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[content_weight=1500,style_weight=0.25]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.433324
novelty_score=0.165925
surprise_score=0.2
