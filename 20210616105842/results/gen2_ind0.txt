IMAGE
name=gen2_ind0
content=dataset/output_images/gen1_ind5.jpg
style=dataset/output_images/gen1_ind0.jpg

SCORES
rank=0
ColorDistributionMeasure=0.580401
ComplexityMeasure=0.884223
LivelinessMeasure=0.965173
LuminanceDistributionMeasure=0.761866
LuminanceRedundancyMeasure=0.99716

MUTATIONS
mutation_1=[color_filter=dark]
mutation_2=[style_transfer=dataset/tmp_images/dataset/tmp_images/65324.jpg]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.376304
novelty_score=0.189598
surprise_score=0.2
