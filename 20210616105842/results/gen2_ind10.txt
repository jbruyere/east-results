IMAGE
name=gen2_ind10
content=dataset/output_images/gen1_ind8.jpg
style=dataset/output_images/gen1_ind5.jpg

SCORES
rank=0
ColorDistributionMeasure=0.56999
ComplexityMeasure=0.955033
LivelinessMeasure=0.963531
LuminanceDistributionMeasure=0.730812
LuminanceRedundancyMeasure=0.991342

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/75842.jpg]
mutation_2=[content_weight=750,style_weight=0.2]
mutation_3=[color_filter=cool]

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.379997
novelty_score=0.182837
surprise_score=0.0
