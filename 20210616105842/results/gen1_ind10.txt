IMAGE
name=gen1_ind10
content=dataset/input_images/mona-lisa-c-1503-1519.jpg
style=dataset/input_images/the-jetty-at-le-havre-bad-weather-1870.jpg

SCORES
rank=0
ColorDistributionMeasure=0.4888
ComplexityMeasure=0.965993
LivelinessMeasure=0.971431
LuminanceDistributionMeasure=0.763654
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.350279
novelty_score=0.242983
surprise_score=0.8
