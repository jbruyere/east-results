IMAGE
name=gen4_ind2
content=dataset/output_images/gen3_ind7.jpg
style=dataset/output_images/gen3_ind2.jpg

SCORES
rank=0
ColorDistributionMeasure=0.474462
ComplexityMeasure=0.850427
LivelinessMeasure=0.973385
LuminanceDistributionMeasure=0.70537
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/4662.jpg]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.276843
novelty_score=0.230861
surprise_score=0.4
