IMAGE
name=gen2_ind12
content=dataset/output_images/gen1_ind14.jpg
style=dataset/output_images/gen1_ind6.jpg

SCORES
rank=2
ColorDistributionMeasure=0.602063
ComplexityMeasure=0.937114
LivelinessMeasure=0.966869
LuminanceDistributionMeasure=0.729258
LuminanceRedundancyMeasure=0.983023

MUTATIONS
mutation_1=[color_filter=warm]

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.3910631387156847
novelty_score=0.2050816215385564
surprise_score=0.0
