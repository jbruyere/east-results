IMAGE
name=gen1_ind1
content=dataset/input_images/napoleoncrossing-the-alps-at-the-st-bernard-pass-20th-may-1800-1801.jpg
style=dataset/input_images/george-caleb-bingham-boatmen-on-the-missouri-google-art-project.jpg

SCORES
rank=1
ColorDistributionMeasure=0.394975
ComplexityMeasure=0.919385
LivelinessMeasure=0.975374
LuminanceDistributionMeasure=0.735987
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[content_weight=1500,style_weight=0.2]
mutation_2=[color_filter=dark]
mutation_3=[style_transfer=dataset/tmp_images/dataset/tmp_images/75753.jpg]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.26068037650519954
novelty_score=0.3289642578437641
surprise_score=1.0
