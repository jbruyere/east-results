IMAGE
name=gen1_ind9
content=dataset/input_images/american-500-sunset.jpg
style=dataset/input_images/seaweed-gatherers-at-omari.jpg

SCORES
rank=1
ColorDistributionMeasure=0.66048
ComplexityMeasure=0.888979
LivelinessMeasure=0.973239
LuminanceDistributionMeasure=0.710543
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/35163.jpg]
mutation_2=[content_weight=1500,style_weight=0.005]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.4057460701190123
novelty_score=0.19751485136460437
surprise_score=0.6000000000000001
