IMAGE
name=gen2_ind9
content=dataset/output_images/gen1_ind6.jpg
style=dataset/output_images/gen1_ind18.jpg

SCORES
rank=1
ColorDistributionMeasure=0.618241
ComplexityMeasure=0.889686
LivelinessMeasure=0.972776
LuminanceDistributionMeasure=0.726575
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[content_weight=500,style_weight=0.15]
mutation_2=[style_transfer=dataset/tmp_images/dataset/tmp_images/20380.jpg]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.38849115655767974
novelty_score=0.2092223751892245
surprise_score=0.2
