IMAGE
name=gen1_ind8
content=dataset/input_images/still-life-vase-with-fifteen-sunflowers-1888-1.jpg
style=dataset/input_images/still-life-with-goblet.jpg

SCORES
rank=2
ColorDistributionMeasure=0.486055
ComplexityMeasure=0.909158
LivelinessMeasure=0.968211
LuminanceDistributionMeasure=0.771702
LuminanceRedundancyMeasure=0.997874

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/35955.jpg]
mutation_2=[content_weight=750,style_weight=0.2]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.32947322345080854
novelty_score=0.2522279510744899
surprise_score=0.8
