IMAGE
name=gen2_ind15
content=dataset/output_images/gen1_ind2.jpg
style=dataset/output_images/gen1_ind6.jpg

SCORES
rank=1
ColorDistributionMeasure=0.685743
ComplexityMeasure=0.984892
LivelinessMeasure=0.970262
LuminanceDistributionMeasure=0.747588
LuminanceRedundancyMeasure=0.983023

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.48157620105934296
novelty_score=0.19541609162719709
surprise_score=0.0
