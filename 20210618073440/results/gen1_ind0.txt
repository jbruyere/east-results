IMAGE
name=gen1_ind0
content=dataset/input_images/costume-design-for-salome-1917.jpg
style=dataset/input_images/woman-with-a-flower-1891.jpg

SCORES
rank=2
ColorDistributionMeasure=0.577044
ComplexityMeasure=0.961391
LivelinessMeasure=0.970945
LuminanceDistributionMeasure=0.763436
LuminanceRedundancyMeasure=0.997874

MUTATIONS
mutation_1=[color_filter=warm]
mutation_2=[style_transfer=dataset/tmp_images/dataset/tmp_images/87435.jpg]
mutation_3=[content_weight=750,style_weight=0.15]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.41034765309314275
novelty_score=0.1944070600152173
surprise_score=0.8
