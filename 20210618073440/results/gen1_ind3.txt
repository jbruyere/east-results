IMAGE
name=gen1_ind3
content=dataset/input_images/plate-77-belted-kingfisher.jpg
style=dataset/input_images/the-tennis-court-oath-20th-june-1789-1791.jpg

SCORES
rank=1
ColorDistributionMeasure=0.572172
ComplexityMeasure=0.869151
LivelinessMeasure=0.970323
LuminanceDistributionMeasure=0.753522
LuminanceRedundancyMeasure=1.0

MUTATIONS
mutation_1=[content_weight=1500,style_weight=0.15]
mutation_2=[style_transfer=dataset/tmp_images/dataset/tmp_images/2678.jpg]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.3636085591814983
novelty_score=0.22009194907587773
surprise_score=0.6000000000000001
