IMAGE
name=gen1_ind6
content=dataset/input_images/boat-at-low-tide-at-fecamp.jpg
style=dataset/input_images/well-by-the-winding-road-in-the-park-of-chateau-noir.jpg

SCORES
rank=1
ColorDistributionMeasure=0.728969
ComplexityMeasure=0.9942
LivelinessMeasure=0.967716
LuminanceDistributionMeasure=0.877602
LuminanceRedundancyMeasure=0.974302

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.5996832779240338
novelty_score=0.1733551857322593
surprise_score=0.4
