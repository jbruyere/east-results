IMAGE
name=gen2_ind19
content=dataset/output_images/gen1_ind6.jpg
style=dataset/output_images/gen1_ind3.jpg

SCORES
rank=2
ColorDistributionMeasure=0.526143
ComplexityMeasure=0.868533
LivelinessMeasure=0.970803
LuminanceDistributionMeasure=0.797998
LuminanceRedundancyMeasure=1.0

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.35401611642780967
novelty_score=0.2320547287830156
surprise_score=0.4
