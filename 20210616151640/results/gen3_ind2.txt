IMAGE
name=gen3_ind2
content=dataset/output_images/gen2_ind10.jpg
style=dataset/output_images/gen2_ind14.jpg

SCORES
rank=0
ColorDistributionMeasure=0
ComplexityMeasure=0
LivelinessMeasure=0
LuminanceDistributionMeasure=0
LuminanceRedundancyMeasure=0

MUTATIONS
mutation_1=[content_weight=750,style_weight=0.005]

METRICS
valuable=0
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.0
novelty_score=1.510430210148795
surprise_score=0.0
