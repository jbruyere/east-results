IMAGE
name=gen1_ind4
content=dataset/input_images/costume-design-for-dance-of-the-seven-veils-1917.jpg
style=dataset/input_images/marguerite-1975.jpg

SCORES
rank=0
ColorDistributionMeasure=0.0
ComplexityMeasure=0.0
LivelinessMeasure=0.0
LuminanceDistributionMeasure=0.0
LuminanceRedundancyMeasure=0.0

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.0
novelty_score=1.9773996294890734
surprise_score=1.0
