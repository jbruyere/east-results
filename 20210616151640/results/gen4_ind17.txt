IMAGE
name=gen4_ind17
content=dataset/output_images/gen3_ind9.jpg
style=dataset/output_images/gen3_ind8.jpg

SCORES
rank=1
ColorDistributionMeasure=0.60693
ComplexityMeasure=0.893457
LivelinessMeasure=0.972949
LuminanceDistributionMeasure=0.732187
LuminanceRedundancyMeasure=0.999294

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.38602695413247406
novelty_score=0.9066460586913592
surprise_score=0.0
