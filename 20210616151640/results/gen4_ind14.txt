IMAGE
name=gen4_ind14
content=dataset/output_images/gen3_ind2.jpg
style=dataset/output_images/gen3_ind0.jpg

SCORES
rank=2
ColorDistributionMeasure=0.640955
ComplexityMeasure=0.875263
LivelinessMeasure=0.970208
LuminanceDistributionMeasure=0.746671
LuminanceRedundancyMeasure=0.996443

MUTATIONS
mutation_1=[color_filter=cool]

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.4049605388392741
novelty_score=0.9095040565662604
surprise_score=0.0
