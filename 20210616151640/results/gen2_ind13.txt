IMAGE
name=gen2_ind13
content=dataset/output_images/gen1_ind7.jpg
style=dataset/output_images/gen1_ind4.jpg

SCORES
rank=1
ColorDistributionMeasure=0.696597
ComplexityMeasure=0.886322
LivelinessMeasure=0.968895
LuminanceDistributionMeasure=0.757504
LuminanceRedundancyMeasure=0.999294

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/88066.jpg]

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.4528225584397871
novelty_score=0.7653618198490993
surprise_score=0.4
