IMAGE
name=gen2_ind5
content=dataset/output_images/gen1_ind3.jpg
style=dataset/output_images/gen1_ind7.jpg

SCORES
rank=1
ColorDistributionMeasure=0.610482
ComplexityMeasure=0.9541
LivelinessMeasure=0.968197
LuminanceDistributionMeasure=0.753433
LuminanceRedundancyMeasure=0.999294

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.42458867861361
novelty_score=0.7580040452525001
surprise_score=0.0
