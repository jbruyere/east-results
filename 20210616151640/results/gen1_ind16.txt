IMAGE
name=gen1_ind16
content=dataset/input_images/via-san-leonardo.jpg
style=dataset/input_images/coast-near-antibes.jpg

SCORES
rank=0
ColorDistributionMeasure=0
ComplexityMeasure=0
LivelinessMeasure=0
LuminanceDistributionMeasure=0
LuminanceRedundancyMeasure=0

MUTATIONS
mutation_1=[content_weight=1250,style_weight=0.2]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.0
novelty_score=1.9839734473658672
surprise_score=1.0
