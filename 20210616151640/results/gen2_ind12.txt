IMAGE
name=gen2_ind12
content=dataset/output_images/gen1_ind5.jpg
style=dataset/output_images/gen1_ind9.jpg

SCORES
rank=2
ColorDistributionMeasure=0.526851
ComplexityMeasure=0.900486
LivelinessMeasure=0.964564
LuminanceDistributionMeasure=0.75236
LuminanceRedundancyMeasure=0.997874

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.34355575474977335
novelty_score=0.7754356472514363
surprise_score=0.0
