IMAGE
name=gen4_ind16
content=dataset/output_images/gen3_ind5.jpg
style=dataset/output_images/gen3_ind4.jpg

SCORES
rank=1
ColorDistributionMeasure=0.725918
ComplexityMeasure=0.886798
LivelinessMeasure=0.970392
LuminanceDistributionMeasure=0.748778
LuminanceRedundancyMeasure=0.997874

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.4667542281558035
novelty_score=0.9052932098953514
surprise_score=0.0
