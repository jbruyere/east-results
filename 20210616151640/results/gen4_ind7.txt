IMAGE
name=gen4_ind7
content=dataset/output_images/gen3_ind7.jpg
style=dataset/output_images/gen3_ind4.jpg

SCORES
rank=1
ColorDistributionMeasure=0.604461
ComplexityMeasure=0.872264
LivelinessMeasure=0.970099
LuminanceDistributionMeasure=0.752412
LuminanceRedundancyMeasure=0.999294

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.3845752084056541
novelty_score=0.9067745303125866
surprise_score=0.0
