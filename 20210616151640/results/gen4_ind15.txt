IMAGE
name=gen4_ind15
content=dataset/output_images/gen3_ind3.jpg
style=dataset/output_images/gen3_ind7.jpg

SCORES
rank=1
ColorDistributionMeasure=0.51335
ComplexityMeasure=0.893943
LivelinessMeasure=0.969573
LuminanceDistributionMeasure=0.769532
LuminanceRedundancyMeasure=0.992814

MUTATIONS
mutation_1=[color_filter=cool]
mutation_2=[content_weight=750,style_weight=0.005]

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.3399370366515295
novelty_score=0.9082158506074969
surprise_score=0.0
