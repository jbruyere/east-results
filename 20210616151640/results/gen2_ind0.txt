IMAGE
name=gen2_ind0
content=dataset/output_images/gen1_ind3.jpg
style=dataset/output_images/gen1_ind6.jpg

SCORES
rank=1
ColorDistributionMeasure=0.739361
ComplexityMeasure=0.969347
LivelinessMeasure=0.970783
LuminanceDistributionMeasure=0.813575
LuminanceRedundancyMeasure=0.986852

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.5586085672995438
novelty_score=0.7671109346225167
surprise_score=0.2
