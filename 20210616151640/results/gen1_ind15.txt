IMAGE
name=gen1_ind15
content=dataset/input_images/the-port-of-le-havre-1903.jpg
style=dataset/input_images/george-caleb-bingham-boatmen-on-the-missouri-google-art-project.jpg

SCORES
rank=0
ColorDistributionMeasure=0
ComplexityMeasure=0
LivelinessMeasure=0
LuminanceDistributionMeasure=0
LuminanceRedundancyMeasure=0

MUTATIONS
mutation_1=[content_weight=1000,style_weight=0.25]
mutation_2=[style_transfer=dataset/tmp_images/dataset/tmp_images/60874.jpg]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.0
novelty_score=1.980477382137033
surprise_score=1.0
