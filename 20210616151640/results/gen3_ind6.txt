IMAGE
name=gen3_ind6
content=dataset/output_images/gen2_ind4.jpg
style=dataset/output_images/gen2_ind2.jpg

SCORES
rank=0
ColorDistributionMeasure=0
ComplexityMeasure=0
LivelinessMeasure=0
LuminanceDistributionMeasure=0
LuminanceRedundancyMeasure=0

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/20637.jpg]

METRICS
valuable=0
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.0
novelty_score=1.510430210148795
surprise_score=0.0
