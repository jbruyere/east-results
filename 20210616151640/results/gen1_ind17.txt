IMAGE
name=gen1_ind17
content=dataset/input_images/electrician-1915.jpg
style=dataset/input_images/the-magpie-1869.jpg

SCORES
rank=0
ColorDistributionMeasure=0
ComplexityMeasure=0
LivelinessMeasure=0
LuminanceDistributionMeasure=0
LuminanceRedundancyMeasure=0

MUTATIONS

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.0
novelty_score=1.982387067178333
surprise_score=1.0
