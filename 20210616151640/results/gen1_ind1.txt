IMAGE
name=gen1_ind1
content=dataset/input_images/boat-at-low-tide-at-fecamp.jpg
style=dataset/input_images/cash-register-1917.jpg

SCORES
rank=0
ColorDistributionMeasure=0.0
ComplexityMeasure=0.0
LivelinessMeasure=0.0
LuminanceDistributionMeasure=0.0
LuminanceRedundancyMeasure=0.0

MUTATIONS
mutation_1=[style_transfer=dataset/tmp_images/dataset/tmp_images/5667.jpg]

METRICS
valuable=0
novel=1
surprising=1
creative=False


METRICS_VALUES
value_score=0.0
novelty_score=1.9813040441221907
surprise_score=1.0
