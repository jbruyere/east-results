IMAGE
name=gen4_ind5
content=dataset/output_images/gen3_ind6.jpg
style=dataset/output_images/gen3_ind1.jpg

SCORES
rank=2
ColorDistributionMeasure=0.642353
ComplexityMeasure=0.966859
LivelinessMeasure=0.966284
LuminanceDistributionMeasure=0.7813
LuminanceRedundancyMeasure=0.975919

MUTATIONS

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.45758658834699867
novelty_score=0.9078637171318868
surprise_score=0.0
