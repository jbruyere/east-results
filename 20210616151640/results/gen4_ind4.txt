IMAGE
name=gen4_ind4
content=dataset/output_images/gen3_ind8.jpg
style=dataset/output_images/gen3_ind4.jpg

SCORES
rank=2
ColorDistributionMeasure=0.669427
ComplexityMeasure=0.875318
LivelinessMeasure=0.96994
LuminanceDistributionMeasure=0.717785
LuminanceRedundancyMeasure=0.998586

MUTATIONS
mutation_1=[color_filter=warm]
mutation_2=[style_transfer=dataset/tmp_images/dataset/tmp_images/63139.jpg]
mutation_3=[content_weight=1250,style_weight=0.1]

METRICS
valuable=1
novel=1
surprising=0
creative=False


METRICS_VALUES
value_score=0.407374467143536
novelty_score=0.916898503817286
surprise_score=0.0
