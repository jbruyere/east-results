IMAGE
name=gen4_ind10
content=dataset/output_images/gen3_ind9.jpg
style=dataset/output_images/gen3_ind4.jpg

SCORES
rank=1
ColorDistributionMeasure=0.551339
ComplexityMeasure=0.86568
LivelinessMeasure=0.970008
LuminanceDistributionMeasure=0.766396
LuminanceRedundancyMeasure=0.999294

MUTATIONS

METRICS
valuable=1
novel=1
surprising=1
creative=True


METRICS_VALUES
value_score=0.3545666821580153
novelty_score=0.907840533287216
surprise_score=0.2
